<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style="text-align: center;">
      <img src="" alt="" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">CMS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
     

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item {{ Route::is('home') ? 'menu-is-opening menu-open' : '' }}">
            <a href="/admin/home" class="nav-link {{ Route::is('home') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
         

          <li class="nav-item {{ Route::is('profile-logs') ? 'menu-is-opening menu-open' : '' }}">
            <a href="profile-logs" class="nav-link {{ Route::is('profile-logs') ? 'active' : '' }}">
              <p>
                Profile Logs
              </p>
            </a>
          </li>

          <li class="nav-item {{ Route::is('watches-profile-logs') ? 'menu-is-opening menu-open' : '' }}">
            <a href="watches-profile-logs" class="nav-link {{ Route::is('watches-profile-logs') ? 'active' : '' }}">
              <p>
                Titan Watches Meet
              </p>
            </a>
          </li>
         

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  