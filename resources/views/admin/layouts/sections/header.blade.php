<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <?php
            $countries = DB::connection('appdb')->table('countries')->whereNotIn('value', ['india'])->where('blocked','=','false')->get();

           
        ?>
        <li class="nav-item d-none d-sm-inline-block">
     
            <select id="admin_country" name="admin_country" class="form-control" style="width:170%">
                @foreach($countries as $each_country)
                <option value="{{$each_country->value}}" @if($each_country->value==session()->get('admin_country')){{"selected"}} @endif>{{$each_country->title}}</option>
                @endforeach
            </select>
        </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <!-- @if (Route::has('login'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
            @endif
            
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif -->
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }}
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                   
                <!-- <a class="dropdown-item" href="">
                       
                    </a> -->

                    <a class="dropdown-item" href="logout-now">
                      Logout
                    </a>
                   
                   
                    <a class="dropdown-item" href=""
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                      
                    </a>


                   

                    <form id="logout-form" action="" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>
  </nav>
  <!-- /.navbar -->

<!-- modal -->
<div class="modal fade" id="expiredPasswordReset" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <form id="update-expiry-password" action="" method="POST" enctype="multipart/form-data">
        @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Your password will expire soon..Reset your password and login again!</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <input type="hidden" name="id" value="{{\Auth::user()->id}}" />
                            <label>New Password</label><span class="form_required">*</span>
                            <input type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" placeholder="New Password" name="password"  maxlength='20' required>
                            <span style="color: #888282;"><b>Password must contains at least 8 but not more than 20 characters, contain uppercase and lower case letters, numbers and special characters (e.g.,%,$)</b></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Confirm Password</label><span class="form_required">*</span>
                            <input type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" placeholder="Retype password" name="password_confirmation"  maxlength='20' required>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong class="error-text">{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </form>
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script src="{{ asset('js/jquery-3.6.0.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready( function() {
        //password expiry model
        var password_expired_on = '<?php echo \Auth::user()->password_expired_on; ?>';
        var current_date        = '<?php echo date('Y-m-d'); ?>';
        if(password_expired_on <= current_date)
        {
            $("#expiredPasswordReset").modal("show");
        }

        //country selection dropdown
        var is_clicked  = 0;
        var admin_country = $("#admin_country").children("option:selected").val();
        setCountry(admin_country,is_clicked);
    });

    $('#admin_country').on('change', function() {
        var admin_country   = this.value;
        var is_clicked  = 1;
        setCountry(admin_country,is_clicked);
    });
    function setCountry(admin_country,is_clicked)
    {
        var data  = {'admin_country': admin_country}
        $.ajax({
            url: "{{ route('set-country-to-session') }}",
            data: data,
            success: function(resultData) {
                if(is_clicked == 1)  
                {
                    location.reload();
                }
            }
        });
    }
</script>