<!DOCTYPE html>
<html lang="en">
<head>
<title>Titan Encircle | Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
  <link id="icon57" rel="apple-touch-icon" href="./media/logos/icon57.png">
  <link id="icon72" rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/icon72.png')}}">
  <link id="icon76" rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/icon76.png')}}">
  <link id="icon114" rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/icon114.png')}}">
  <link id="icon120" rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/icon120.png')}}">
  <link id="icon152" rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/icon152.png')}}">
  <link id="icon167" rel="apple-touch-icon" sizes="167x167" href="{{ asset('img/icon167.png')}}">
  <link id="icon180" rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/icon180.png')}}">
  <link id="icon144" rel="icon" type="image/png" sizes="144x144" href="{{ asset('img/icon144.png')}}"/>
  <link id="icon192" rel="icon" type="image/png" sizes="192x192" href="{{ asset('img/icon192.png')}}"/>
  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{$_ENV['APP_URL']}}/assets/css/style.css">
  <link rel="stylesheet" href="{{$_ENV['APP_URL']}}/assets/css/toastr.css" rel="stylesheet" type="text/css">
  

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
  <style>
      .loyalty-logo {
        position: absolute;
    width: 70px;
    right: 10px;
    top: 10px;
}

.button__loader{

  display:none;
}
.banner-head{
  text-align:center;
}
.toast-info {
  background-color: #2f96b4;
}

.owl-nav{
  display: none;
}
.owl-dots {
  text-align: center;
  padding-top: 15px;
  top: 92%;
  position: absolute;
  left: 0;
  right: 0;
}
.owl-dots button.owl-dot {
  width: 15px;
  height: 15px;
  border-radius: 50%;
  display: inline-block;
  background: #ccc;
  margin: 0 3px;
}
.owl-dots button.owl-dot.active {
  background-color: #000;
}
.owl-dots button.owl-dot:focus {
  outline: none;
}
.owl-nav button {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    background: rgba(255, 255, 255, 0.38) !important;
}
.owl-dots .owl-dot span{
  font-size: 70px;
  position: relative;
  top: -5px;
}
.owl-theme .owl-dots .owl-dot span{
  width: 0px;
  height: 0px;
  margin: 0px;
}

.owl-carousel{
  height: 100%;
}
.item{
  height: 100vh;
}
.item-1{
  background-image: url("assets/usa-banner1.png");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  height: 100vh;
}
.item-2{
  background-image: url("assets/usa-banner2.png");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  height: 100vh;
}
.item-3{
  background-image: url("assets/image/login-banner4.jpg");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  height: 100vh;
}
.item-4{
  background-image: url("assets/image/login-banner-vday-1500-teal.jpg");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  height: 100vh;
}
.item-5{
  background-image: url("assets/image/login-banner-vday-1500-pink.jpg");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  height: 100vh;
}
.banner-head{
    max-width: 100%;
    width: 85%;
    margin: 0 auto;
    color: #fff;
}
.banner-head-btm{
    max-width: 100%;
    width: 60%;
    margin: 0 auto;
    color: #fff;
}
@media (max-width: 1440px){
  .item-5{
    background-image: url("assets/image/login-banner-vday-1440-pink.jpg");
  }
  .item-4{
    background-image: url("assets/image/login-banner-vday-1440-teal.jpg");
  }
}
@media (max-width: 1024px){
  .item-5{
    background-image: url("assets/image/login-banner-vday-1024-pink.jpg");
  }
  .item-4{
    background-image: url("assets/image/login-banner-vday-1024-teal.jpg");
  }
}
@media (max-width: 768px){
  .item .banner-head{
    width: 98%;
  }
  .item .banner-head-btm{
    width: 85%;
  }
  .item-5{
    background-image: url("assets/image/login-banner-vday-764-pink.jpg");
  }
  .item-4{
    background-image: url("assets/image/login-banner-vday-764-teal.jpg");
  }
}
</style>
</head>
<body>
<input name="APP_URL" value="{{$_ENV['APP_URL']}}" type="hidden"/>
  <section class="vh-100">
    <input type="hidden" value="{{iplocation()}}"/>
    <div class="container-fluid h-100">
      <div class="row d-flex align-items-center justify-content-center h-100 ">
        
      
        <div class="col-md-6 col-lg-6 col-xl-6 p-0 h-100 mob-hide">

          <!-- <div class="slide1 h-100 d-flex flex-column justify-content-center align-items-center">
            <div class="banner-txt-sec d-flex flex-column justify-content-center align-items-center">
              <h1 class="banner-head">Welcome To Encircle</h1>
            </div>
          </div> -->

          <div class="owl-carousel-login owl-carousel owl-theme-login owl-theme">

              
          
          <!-- <div class="item">
                <a href="{{$_ENV['APP_URL']}}/usa">
                  <div class="item-2 d-flex justify-content-center align-items-center">
                  <div class="container">
                      <h1 class="banner-head mb-3">A Truly Rewarding Loyalty Program</h1>
                      <p class="banner-head-btm">become a member and enjoy rewards, benefits and experiences starting now!</p>
                    </div>
                  </div>
                </a>
              </div> -->
          
            <div class="item">
                <a href="{{$_ENV['APP_URL']}}/usa">
                  <div class="item-1"></div>
                </a>
              </div>

              <div class="item">
                <a href="{{$_ENV['APP_URL']}}/usa">
                  <div class="item-2 d-flex justify-content-center align-items-center">
                  <div class="container">
                  
                    </div>
                  </div>
                </a>
              </div> 

            

          </div> 
        </div>



        <div class="col-md-6 col-lg-6 col-xl-6 clearfix">
        <img class="loyalty-logo" src="{{$_ENV['APP_URL']}}/assets/Titan_Encircle_Logo_highres_Highres.png">
          <div class="d-flex justify-content-center ">
            <img src="{{$_ENV['APP_URL']}}/assets/image/logo-tanishq-usa.png" class="img-fluid mt-2 titan-brand">
          </div>
         <!--  <p style="
    font-size: 18px;
    text-align: center;
    color: #727272;
">Sharjah Expo Center, Booth number  <b style="
    color: #000;
">5-2740</b></p> -->
          <div class="sign-txt d-flex justify-content-center" style="text-align: center;">Register & Get 50 USD<br> worth points</div>
          <div class="d-flex justify-content-center">
            <form class="login-form w-90" id="loginSection">
            <input value="<?=session('SESSION_BACKLINK')?>" name="back_link" id="prelogin_redeem" class="name-input" type="hidden">
            @csrf
            <input value="{{Session::get('logged_in_country')}}" type="hidden"  id="country" name="country">
            @if(Session::get('logged_in_country',null)==='india')
            <input value="{{Session::get('logged_in_country')}}" value="+91" type="hidden"  id="country_code_input_field" name="country_code_input_field">
            @endif
              <!--Mob field-->
              <div class="align-items-center justify-content-center form-text send-alert-black">
                  </div>
              <div class="form-top d-flex justify-content-center">
              @if(Session::get('logged_in_country',null)==='uae' || Session::get('logged_in_country',null)==='usa')
                <select class="form-control country_code_input_field" style="width:85px;" id="country_code_input_field" name="country_code_input_field">
                 <!--  <option disabled selected value="default">---</option> -->
                  @if(Session::get('logged_in_country',null)==='uae')
                  <option title='+971(UAE)' value="+971">+971</option>
                  @endif
                  @if(Session::get('logged_in_country',null)==='usa')
                  <option title='+1(USA)' value='+1'>+1</option>
                  @endif
                  <option title='+91(INDIA)' value="+91">+91</option>
                </select>
                @endif
                <input type="number" class="form-control mobile" id="mobile" placeholder="Enter Mobile No" name="mobile" style="width:90%">
                <input autocomplete="off" type="password" class="form-control login-otp w-90" id="otpdigits" name="otpdigits" placeholder="Enter OTP Here">
              </div>
             @if(Session::get('logged_in_country',null)==='india')
              <div class="d-flex align-items-center form-text px-4" id="whatsapp-text">
                I would like to Opt-in for WhatsApp / SMS / Email
              </div>
              <div class="form-check login-check px-5">
                <div class="form-check-inline">
                  <input checked type="checkbox" class="form-check-input consent-check" id="check1" name="Whatsapp" value="Yes">
                  <label class="form-check-label" for="check1">Yes</label>
                </div>
                <div class="form-check-inline">
                  <input  type="checkbox" class="form-check-input consent-check" id="check2" name="ustom-radio" value="No">
                  <label class="form-check-label" for="check2">No</label>
                </div>
              </div>
              @else 
              <input  style="display:none;" checked type="checkbox" class="form-check-input consent-check" id="check1" name="Whatsapp" value="Yes">
              <input  style="display:none;" type="checkbox" class="form-check-input consent-check" id="check2" name="ustom-radio" value="No">
              @endif
             
              <div class="form-check form-check-privacy px-5">
                <input checked type="checkbox" class="form-check-input" id="terms" name="terms">
                <label class="form-check-label" for="check1">I agree to the <span class="bold"><a href="{{$_ENV['APP_URL']}}/privacy-policy" target="_blank">Privacy Policy</a></span> and
                  <span class="bold"><a target="_blank" href="{{$_ENV['APP_URL']}}/terms-condition">T&C's</a></span></label>
              </div>

              <div class="d-flex justify-content-center mt-3" style="gap: 20px;">
                <button href="javascript:void(0)" class="btn btn-otp d-flex justify-content-center align-items-center send-otp">Send OTP</button>
              <!-- loader button -->

              <a href="" class="button__loader">
                <span class="button__text">Loading...</span>
              </a>

              <a href="javascript:void(0)" class="btn btn-otp justify-content-center align-items-center confirm-otp" >Confirm OTP</a>
              <a href="javascript:void(0)" class="btn btn-resend-otp  justify-content-center align-items-center">Resend OTP</a>
              </div>

              <div class=" d-flex justify-content-center mt-4 bck-home">
                <a href="{{$_ENV['APP_URL']}}/usa">Back to Home</a>   
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<script src="{{$_ENV['APP_URL']}}/assets/js/toastr.min.js"></script>
<!-- <script src="{{$_ENV['APP_URL']}}/assets/js/script.js"></script> -->
<script src="{{$_ENV['APP_URL']}}/assets/js/script-usa.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
</body>
<script>

  $(function() {
    var owlLogin = $(".owl-carousel-login");
    owlLogin.owlCarousel({
      items: 1,
      //margin: 10,
      loop: true,
      nav: true,
      autoplay:true,
      autoplayTimeout:5000,
      autoplayHoverPause:true,
      autoHeight:true
      });
  });




  $(document).ready(function(){
    $('.consent-check').click(function(){
      $('.consent-check').not(this).prop('checked', false);
    });
    let back_link = $("input[name=back_link]","#loginSection").val();
    var href = back_link.match(/([^\/]*)\/*$/)[1];
  if(href=='games') {
    toastr.info('You need to login to access Game Zone');
  }

  if(href=='referral') {
    toastr.info('You will be required to login to go to Referral');
  }
  if(href=='MyProfile') {
    toastr.info('You will be required to Login/Enroll to Become a member');
  }
  

/* 
 alert(href); */

  });
</script>
</html>