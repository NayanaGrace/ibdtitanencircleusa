@extends('layouts.app')
@section('title', 'Enroll')
@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link href="{{$_ENV['APP_URL']}}/home/css/pages/home_page.css?<?php echo date('Y-m-d_H:i:s'); ?>" rel="stylesheet" integrity="aLTBTeGDitgZY3aUpdqjdiqcSUpEfm66ncTtLb/nx1yNGjQlrWja+Y87ffa47IWx" type="text/css"> 

<style>
.personal-info, .personal-info-min, .life-style, .back-to-profile {
 display:none;
}
</style>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<script src="{{$_ENV['APP_URL']}}/assets/js/enroll.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
$(document).ready(function(){

    $(".button__loader11").hide();
    $('#OneTrustokey').modal({backdrop: 'static', keyboard: false}, 'show');
    $('#OneTrustokey').modal('show'); 


$(document).on('click', '.OneTrustoky', function(){

$(".button__loader11").show();
$(".OneTrustoky").hide();
$(".okycancel").hide();

var APP_URL = $("input[name=APP_URL]").val();

var mobile ={{session('SESS_MOBILE')}};


$.ajax({
    type: 'POST',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "mobile": mobile
     },
    url: APP_URL + '/onetrustsignup',
    success: function(data){
      $('#OneTrustokey').modal('hide');
      $(".button__loader11").hide();

}       
});

});

$(document).on('click', '.okycancel', function(){

var APP_URL = $("input[name=APP_URL]").val();


window.location = APP_URL + '/logout';

});

$(document).on('click', '.ThankYouCancel', function(){
    $('#Thankyoumodal').modal('hide');
    $('.back-to-profile').css('display', 'flex');
    $(".update-popup").hide();
    $.scrollTo($('.updat-hd'), 0);
  });

});
</script>
@endsection
@section('content')
<?php
$marital_status = 1;
if($marital_status==1) {
    $marital_status="No";
} else {
    $marital_status="Yes";
}

$maritalstatus_enroll = [

    array(
      "title" => "Single", 
      "value" => "No"
    ),
    array(
      "title" => "Married", 
      "value" => "Yes"
    )
    ];

?>
<section class="update-profile" style="min-height: calc(100vh - 111px);">
    <div class="container">
        <div class="row">
            <div class="updat-hd top-hds">Hi! We would like to know you better to offer you a great experience. Please help us
                with your details below</div>

            <div class="steps clearfix my-5rem my-md-3rem">
                <ul role="tablist">
                    <li role="tab" aria-disabled="false" class="first current d-flex flex-column justify-content-center align-items-center" aria-selected="true">
                        <a href="#form-total-h-0" id="form-total-t-0" aria-controls="form-total-p-0">
                            <span class="current-info audible"></span>
                            <span class="title active b-info"></span>
                        </a>
                        <span class="mt-2 head active b-info">Basic Information</span>
                    </li>

                    <li role="tab" aria-disabled="false" aria-selected="true" class="d-flex flex-column justify-content-center align-items-center">
                        <a href="#form-total-h-0" id="form-total-t-0" aria-controls="form-total-p-0">
                            <span class="current-info audible"></span>
                            <span class="title p-info"></span>
                        </a>
                        <span class="mt-2 head p-info">Personal Information</span>
                    </li>

                    <li role="tab" aria-disabled="false" class="last d-flex flex-column justify-content-center align-items-center" aria-selected="true">
                        <a href="#form-total-h-2" id="form-total-t-2" aria-controls="form-total-p-2">
                            <span class="title l-info"></span>
                        </a>
                        <span class="mt-2 head l-info">Address / Location</span>
                    </li>
                </ul>
            </div>
        </div>
            <div class="update-profile-div">
                <form id="updateprofile" class="updateprofile w-80 w-md-90">
            @csrf
                <div class="row updat-form basic-info">
                    <div class="updat-form-hd">BASIC INFORMATION</div>

            <input value="<?=session('SESSION_BACKLINK')?>" name="back_link" id="prelogin_redeem" class="name-input" type="hidden">
            <input value="<?=session('logged_in_country')?>" name="logged_country" id="logged_country" class="name-input" type="hidden">

          
                    <div class="form-fields row">
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label ps-3">Title</label>
                            <select name="title" class="form-select updat-form-placeh">
                                <option value="">Select Title</option>
                                @foreach(getDoList('salutation') as $row)
                                <option  value="{{$row->title}}">{{$row->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label ps-3">First Name</label>
                            <input value="" type="text" class="form-control updat-form-placeh" name="first_name" id="first_name" placeholder="Enter First Name">
                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label ps-3">Last Name</label>
                            <input value="" type="text" class="form-control updat-form-placeh" id="last_name" name="last_name" placeholder="Enter Last Name">
                        </div>

                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="gender" class="form-label ps-3">Gender</label>
                            <select id="gender" name="gender" class="form-select">
                                <option value="">Select Gender</option>
                               
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                              
                            </select>
                        </div>
                       
                        <div class="col-md-4 col-sm-12 form-label-tp input-daterange">
                            <label for="" class="form-label ps-3">Date of Birth</label>
                            <input value="" type="date" class="form-control updat-form-placeh datepicker" id="dob" name="dob" placeholder="DD / MM / YYYY">
                          <!--   <span class="fa fa-calendar calen" id="fa-1"></span> -->
                        </div>



                        


                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label updat-form-placeh ps-3">Contact Number</label>
                            <input value="{{session('SESS_MOBILE')}}" type="text" class="form-control updat-form-placeh" id="mobile" name="mobile" placeholder="Ente Contact Number">
                        </div>
                        <div class="col-md-12">
                            <div class="row d-flex justify-content-center">
                                <div class="col-md-8 col-sm-12 form-label-tp email-padding">
                                    <label for="" class="form-label ps-3">Email ID</label>
                                    <input value="" type="email" class="form-control updat-form-placeh" id="email" name="email"
                                        placeholder="Enter your email id">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12  text-center">
                        <button type="button" class="btn profile-update-btn basic-info-btn mt-lg-5 mt-md-5 mt-2" >Next</button>
                    </div>
                </div>
            
                <!--personal information-->
                <div class="row updat-form personal-info">
                    <div class="updat-form-hd">Personal Information</div>
                    <div class="col-md-12">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-8 col-sm-12 form-label-tp ">
                                <label for="" class="form-label">Marital Status</label>
                                <select id="marital_status" name="marital_status" class="form-select updat-form-placeh marital_status">
                                    <option value="">Select Marital Status</option>
                                    @foreach($maritalstatus_enroll as $row)
                                    <option @if($marital_status==$row['value']) selected @endif value="{{$row['value']}}">{{$row['title']}}</option>
                            @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  text-center">
                       <button type="button" class="btn profile-update-btn go-back-basic-info mt-lg-5 mt-md-5 mt-2">Previous</button>
                        <button type="button" class="btn profile-update-btn personal-info-btn mt-lg-5 mt-md-5 mt-2">Next</button>
                    </div>
                </div>
                <!--personal information end-->
                <!--personal information2-->
                <div class="row updat-form personal-info-min">
                    <div class="updat-form-hd">Personal Information</div>
                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Martial Status</label>
                        <select id="" class="form-select marital_status_min">
                        <option value="">Select Martial Status</option>
                        @foreach($maritalstatus_enroll as $row)
                                    <option @if($marital_status==$row['value']) selected @endif value="{{$row['value']}}">{{$row['title']}}</option>
                            @endforeach
                        </select>
                    </div> 
                    <div class="col-md-4 col-sm-12 form-label-tp input-daterange">
                        <label for="" class="form-label">Birthday of Spouse</label>
                        <input value="" type="date" class="form-control updat-form-placeh datepicker" id="spouse_dob" name="spouse_dob" placeholder="DD / MM / YYYY">
                      <!--   <span class="fa fa-calendar calen" id="fa-1"></span> -->
                    </div>
                    <div class="col-md-4 col-sm-12 form-label-tp input-daterange">
                        <label for="" class="form-label updat-form-placeh">Anniversary</label>
                        <input value="" type="date" class="form-control updat-form-placeh datepicker" id="anniversary_date" name="anniversary_date" placeholder="DD / MM / YYYY">
                     <!--    <span class="fa fa-calendar calen" id="fa-1"></span> -->
                    </div>
                    <div class="col-md-12  text-center">
                       <button type="button" class="btn profile-update-btn go-back-basic-info mt-lg-5 mt-md-5 mt-2">Previous</button>
                        <button type="button" class="btn profile-update-btn personal-info-min-btn mt-lg-5 mt-md-5 mt-2">Next</button>
                    </div>
                </div>
                <!--personal information2 end-->
                <!--Lifestyle Preferences-->
                <div class="row updat-form life-style">
                    <div class="updat-form-hd">Address / Location</div>

                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Country</label>
                        <select id="country" name="country" class="form-select select_country">
                        <option value="">Select Country</option>
                         @foreach($country as $row)
                         <option  value="{{$row->title}}">{{$row->title}}</option>
                        @endforeach 
                    
                        </select>
                    </div>


                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Emirates / States</label>

                      <select id="state" name="state" class="form-select updat-form-placeh select_state">
                                <option value="">Select state</option>
                               
                            </select>
                    </div>


                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">City</label>
                    

                      <select id="city" name="city" class="form-select updat-form-placeh">
                                <option value="">Select city</option>
                              
                            </select>
                    </div>


                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">City Other</label>
                        <input value="" type="text" class="form-control updat-form-placeh" id="city_others" name="city_others" placeholder="Ente city Others">
                    </div>



                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Colony</label>
                        <input value="" type="text" class="form-control updat-form-placeh" id="colony" name="colony" placeholder="Ente Colony">
                    </div>


                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Street</label>
                        <input value="" type="text" class="form-control updat-form-placeh" id="state" name="street_name" placeholder="Ente street name">
                    </div>


                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Building Name</label>
                        <input value="" type="text" class="form-control updat-form-placeh" id="building_name" name="building_name" placeholder="Enter Building Name">
                    </div>




                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">House No</label>
                        <input value="" type="text" class="form-control updat-form-placeh" id="house_no" name="house_no" placeholder="Ente House No">
                    </div>  



                    <div class="col-md-12  text-center">
                        <button type="button" class="btn profile-update-btn go-back-life-style mt-lg-5 mt-md-5 mt-2">Previous</button>
                      <!--   <button type="submit" class="btn profile-update-btn life-style-btn mt-lg-5 mt-md-5 mt-2">Submit</button> -->
                      <a id="btnLoader" type="submit" class="button__loader btn reffer-btn reffer_btn mt-lg-5 mt-md-5 mt-2" onclick="btnFunction()">
                    <span class="button__text">Submit</span>
</a >
                    </div>
                </div>
                <!--Lifestyle Preferences end-->
            </div>
            </section>
        </form> 
    </div> 
    <section class="update-profile back-to-profile" style="min-height: calc(100vh - 111px);align-items: center;">
    <div class="container">
        <div class="row">
            <div class="updat-hd text-center">That’s all for now. Thanks for filling all the details. We’ll customize offers
                according to the details you have provided. </div>
                <div class="col-md-12  text-center">
                <a href="{{$_ENV['APP_URL']}}/myprofile/personal-information/myprofile-info" class="btn profile-submit-btn back-btn mt-lg-5 mt-md-5 mt-2">Back to profile</a>

                </div>
        </div>
    </div>

</section>


<div class="modal fade modal-light show" id="OneTrustokey" style="padding-right: 8px;" aria-modal="true" role="dialog">
      <div class="modal-dialog modal-lg modal-dialog-centered align-items-center d-flex justify-content-center">
        <div class="modal-content bg-yellow">
  
    <div class="modal-body">
          <p style="
    text-align: center;
    color: #000;
    font-size: 18px;
    line-height: 2rem;
    overflow: hidden;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    margin: 16px 17px 25px 17px;">
            I Agree to enrol into Loyalty program &amp;  the Account related communication </p>
          <form id="OneTrustoky" class="row">
           @csrf
             

                <div class="btn-grp d-flex align-items-center  pl-3 mt-1" style="
    gap: 15px;
    justify-content: center;
">
                   <a href="javascript:void(0)" class="btn btn-member OneTrustoky" style="box-shadow: none"><span>OK</span></a>
             
                <a href="javascript:void(0)" class="btn btn-member okycancel" style="box-shadow: none;background: #FFf;color: #000;border: 0.5px solid #e1e1e1;">
                    <span>Cancel</span>
                </a>

                <a class="button__loader button__loader11">
                <span class="button__text">Loading...</span>
              </a>

                </div>
            </form>
          </div>
        
        </div>
      </div>
    </div>





    <div class="modal-wrapper" >
    <div class="modal fade modal-light show" id="Thankyoumodal" style="padding-right: 8px;" aria-modal="true" role="dialog">
      <div class="modal-dialog modal-md modal-dialog-centered align-items-center d-flex justify-content-center">
        <div class="modal-content bg-yellow" style=" padding: 15px;text-align: center; background: url({{$_ENV['APP_URL']}}/assets/background_ibd.png);">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div> -->
          <!-- Modal body -->
      
          <div class="modal-body">

         
            <img src="{{$_ENV['APP_URL']}}/assets/Titan_Encircle_Logo_highres_Highres.png" style="width:100px;">
            <img src="{{$_ENV['APP_URL']}}/assets/thankyou-m.png" style="width:100%;margin-bottom: 10px;">
           <!--  <h4class="page-info-heading-1">
           
            </h4> -->
       
            <p style="
    color: #ffffff;
"> Your account will be credited with <b style="
    font-size: 18px;
    color: #f59301;
">200 AED</b> worth Points within the next 24 hours.  </p>             
<div class="btn-grp d-flex align-items-center p-0 mt-5" style="
    justify-content: center;
">
                <a href="javascript:void(0)" class="profile-update-btn ThankYouCancel" style="box-shadow: none;background: #FFf;color: #000;border: 0.5px solid #e1e1e1;">
                    <span>OK</span>
                </a>
                </div>
          </div>
        
        </div>
      </div>
    </div>
</div>
<!--  Thnk you pop up ends here-->


<script>

$(document).ready(function() {
       /*  $('#multiple-selected').multiselect(); */
       $('.js-example-basic-multiple').select2();
       $('.select2-container').css("width","100%")
    });
</script>
@endsection