@extends('PreLogin.layouts.app')
@section('title', 'My Profile')
@section('content')
<div class="myaccount-section d-flex justify-content-center align-items-center">
  <div class="myaccount-wrapper col-11 col-md-11 col-lg-9">
    <section class="titan-carousel account-carousel d-flex justify-content-center align-items-center">
      
      <!-- <div id="demo" class="carousel slide" data-bs-ride="carousel">
        
        <div class="carousel-indicators">
          <?php $i = 0 ?>
          @foreach($web_gallery as $each_web_gallery_image)
          @if(isset($each_web_gallery_image->file_name))
          <button type="button" data-bs-target="#demo" data-bs-slide-to="{{$i}}" @if($i==0) class="active" @endif></button>
          @endif
          <?php $i++; ?>
          @endforeach
        </div>

    
        <div class="carousel-inner">

      <?php $i = 0 ?>
      @foreach($web_gallery as $each_web_gallery_image)
      @if(isset($each_web_gallery_image->file_name))
        <div class="carousel-item @if($i==0) active @endif">
          <img src="/admin/images/banners/{{$each_web_gallery_image->file_name}}" class="d-block" style="width:100%">
        </div>
      @endif
      <?php $i++; ?>
      @endforeach
        </div>
      </div> -->
      <div class="account-image">
        <!-- <div class="account-image-icon"><img src="/assets/image/img-chng.png" alt="" class="img-fluid"></div> -->
      </div>
    </section>

    <section class="d-flex justify-content-center align-items-center">
    <div class="wrapper account-btm-bx mb-md-5 md-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-md-3 col-xl-2 col-sm-12 p-0 account-list-web">
            <div class="d-flex flex-column">
              <ul class="nav nav-pills flex-column mb-sm-auto mb-0 account-bx-ul" id="menu">
                <li class="nav-item ">
                  <a href="#" class="nav-link align-middle px-0 active">
                    Overview
                  </a>
                </li>
                @if(Session::get('log_country',null)==='usa')

                <!-- <li class="nav-item">
                  <a href="/myprofile/personal-information" class="nav-link px-0 align-middle">
                    Complete Profile</a>
                </li> -->
                <li class="nav-item">
                  <a href="/myprofile/personal-information/edit" class="nav-link px-0 align-middle">
                    Update Profile</a>
                </li class="nav-item">
                @else
                
                <li class="nav-item">
                  <a href="/myprofile/personal-information/edit" class="nav-link px-0 align-middle">
                    Update Profile</a>
                </li class="nav-item">
                @endif

                <li class="nav-item">
                  <a href="/myprofile/account-statement" class="nav-link px-0 align-middle">
                  Purchase Statement</a>
                </li>

                <li class="nav-item">
                  <a href="/myprofile/referral" class="nav-link px-0 align-middle btm-none">
                    <span class="">Refer Now</span></a>
                </li>
              </ul>
            </div>
          </div>

          <div class="col-12 p-0 account-list-mob">
            <div class="account-inline-list d-flex">
              <div class="col d-flex align-items-center justify-content-center down-arrow active"> Overview</div>
              @if(Session::get('log_country',null)==='usa')
              <!-- <div class="col d-flex align-items-center justify-content-center down-arrow "><a href="/myprofile/personal-information">Complete Profile</a></div> -->
              <div class="col d-flex align-items-center justify-content-center down-arrow "><a href="/myprofile/personal-information/edit"">Update Profile</a></div>
              @else
              <div class="col d-flex align-items-center justify-content-center down-arrow "><a href="/myprofile/personal-information/edit">Update Profile</a></div>
              @endif
              <div class="col d-flex align-items-center justify-content-center down-arrow "><a href="/myprofile/account-statement">Purchase Statement</a></div>
              <div class="col d-flex align-items-center justify-content-center down-arrow "><a href="/myprofile/referral">Referral</a></div>

            </div>
          </div>

          <div class="col-md-12 col-xl-10 col-xs-12 col-sm-12 account-bx-rgt account-bx-h">
            <div class="row">
              <div class="d-flex justify-content-between align-items-center">
                <div class="account-bx-wlcm"><p style="margin: 0;">Hello {{ $Data['first_name']." ".$Data['last_name'] }}!</p></div>
                <div class="account-bx-mem d-flex justify-content-end"><p style="margin: 0;">Member Since: {{ $Data['member_since'] }}</p></div>
              </div>
              
              <div class="col-md-6 col-xs-12 account-bx-tp mt-3">
                
                <div class="account-bx-1 d-flex flex-column justify-content-between px-4 py-3">
                  <div class="account-coin-bal">Your current point balance is </div>
                  <div class="d-flex justify-content-between align-items-center w-100 mb-6">
                    <div class="account-coin float-start">{{ empty($Data['balance'])?'0':$Data['balance'] }}</div>
                    <div class="account-coin-end d-flex align-items-center justify-content-between"><p>Redeem now</p>
                    <a href="/shop">
                    <img class="redeem-img img-fluid" src="/assets/image/account-nxt-arrow.png" style="width: 80%;">
                    </a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-xs-12 account-bx-tp mt-3">
                <clear></clear>

                <div class="account-bx-2 d-flex flex-column justify-content-between px-4 py-3 @if($Cards['1']=='Silver') silver @elseif($Cards['1'] == 'Gold') gold @elseif($Cards['1'] == 'Platinum') platinum @else platinum @endif">
                  <div class="account-coin-bal">Your current tier is </div>
                  <div class="account-coin d-flex">{{ $Cards['1'] }}</div>
                </div>
              </div>
              @if($Cards['1'] == 'Silver')

              <div class="col-md-6 col-xs-12 account-bx-tp mt-5">
                <div class="account-bx-wlcm-btm">Next Tier </div>
                <div class="account-bx-3 d-flex flex-column justify-content-between px-4 py-3 gold">
                  <div class="account-tier">Gold</div>
                  <div class="account-tier-bal">56 points required to upgrade</div>
                </div>
              </div>

              @elseif($Cards['1'] == 'Gold')
              <div class="col-md-6 col-xs-12 account-bx-tp mt-5">
                <div class="account-bx-wlcm-btm">Next Tier </div>
                <div class="account-bx-3 d-flex flex-column justify-content-between px-4 py-3 platinum">
                  <div class="account-tier">Platinum</div>
                  <div class="account-tier-bal">56 points required to upgrade</div>
                </div>
              </div>
            @else 

              <div class="col-md-6 col-xs-12 account-bx-tp mt-5">
                <div class="account-bx-wlcm-btm">Next Tier </div>
                <div class="account-bx-3 d-flex flex-column justify-content-between px-4 py-3 platinum">
                  <div class="account-tier">Platinum</div>
                  <div class="account-tier-bal"> You can now earn  <span>4x more points</span> by shopping <span> Titan Products!</span></div>
                </div>
              </div>
            @endif
              <div class="col-md-6 col-xs-12 account-bx-tp mt-5">
                <div class="account-bx-wlcm-btm">Validity</div>
                <div class="account-bx-4 d-flex flex-column justify-content-between px-4 py-3">
                  <div class="account-coin-bal">Points will expire on </div>
                  <?php
                  $time_zone = 'Asia/Kolkata';
                  ?>
                    <div class="account-coin d-flex">{{ \Carbon\Carbon::parse($Data['expiry_date'])->setTimezone($time_zone)->format('d-M-Y') }}</div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  </div>
</div>
@endsection