@extends('layouts.app')
@section('title', 'Terms & Condition')
@section('bodyclass', 'terms-condition')
@section('content')

@php
$country_selected = false;
$country = null;
$api_url = session('API_URL', null);
$title1 = "Everything you need to know about our terms and conditions";
$title2 = "Titan Encircle Dubai Loyalty Program Terms & Conditions";
$margin_top = 30;
if($api_url) {
  $country_selected = true;
  $margin_top = 0;
  $country = (session('API_URL') == $_ENV['API_URL_UAE'])?'uae':'india';
}
if($country_selected) {
  $title2 = $title1;
}

@endphp

<section class="terms-and-condition bg-yellow-white first-section-pt">
  <div class="container">
    @if(!$country_selected || ($country_selected && $country == 'india'))
    <div class="row">
      <div class="col-lg-12">
        <h2 class="heading-light-1">
          {{ $title1 }}
        </h2>
        <div class="terms-and-condition-content">
          <ol>
            <li>
              Membership is voluntary and open to Indian citizens based in India only and shall be recognized only through concerned Indian mobile numbers of members/family members enrolled and as registered with www.titanencircle.com or registered with Titan Company Limited (“Company”).
            </li>

            <li>
              No physical membership cards will be issued to the customers/members/family members.
            </li>

            <li>
                While making purchases at Tanishq, Zoya, MIA, World of Titan, Fastrack, Helios, Titan Eye plus & Taneira outlets across India, mention encircle membership number or registered mobile number to the cashier before billing. Points will be credited only when membership number is provided while billing. Please ensure to check mobile number matching with your name and update us with change of mobile number through our website www.titanencircle.com or customer care help desk at 18001084826 only.
              <ul>
                <li class="bullet-points-circle">
                  Member can also earn encircle points while making purchases at Company's online website https://www.titan.co.in (E-commerce website) & also at www.caratlane.com. Customer required to link his/her encircle membership number with his/her account profile in website to earn encircle points. Points will credited only after successful delivery of order. Please ensure to check mobile number & email id is matching with your name and update us for any change through our website www.titanencircle.com or customer care help desk at 18001084826 only.
                </li>
              </ul>
            </li>

            <li>
              Please note that the purchase of gold coins/bars & silver articles are excluded from the Titan Encircle program.
            </li>

            <li>
              Reward points can be redeemed at Tanishq, Zoya, MIA, World of Titan, Fastrack, Helios, Titan Eye plus & Taneira outlets across India & also at E-commerce websites(titan.co.in, Caratlane.com). The value of one point is equal to Rupee 1. However, reward points cannot be redeemed for cash. Members will be required to present the membership number/mobile number at the time of redemption of the reward points for offline purchase. Customer required to link his/her encircle membership number with his/her account profile in E-commerce website to redeem encircle points.
            </li>

            <li>
              By enrolling into the Titan encircle program at any Titan store through enrollment form, Tabs or enrolling through E-commerce websites you agree that you have read the terms & conditions that govern the Titan encircle program and that you accept them. As an encircle member you will also start receiving promotional communication from all Titan brands.
            </li>

            <li>
              The encircle reward Points issued by Company to the members/family members are free and is without any consideration to be payable by member/family members who is/are enrolled. The allotment & redemption of reward points will be solely at the discretion of Company and no right created to the Customer/member/family member and is a privilege given to a customer/member/family member.
            </li>

            <li>
              Titan encircle program benefit cannot be clubbed with any other offer or discount, viz. reward points will not be awarded in the following cases.
              <ul>
                <li class="bullet-points-circle">
                  Special offers / promotions / items excluded by the management
                </li>

                <li class="bullet-points-circle">
                  Points Earning/Accrual will not be applicable during the following schemes: Activation/Consumer scheme, Best Buy, End Of Season Sale(EOSS), Discount given during all exchange offers, Gift With Purchase offer
                </li>

                <li class="bullet-points-circle">
                  Against the purchase of gold coins/bars & silver articles.
                </li>

                <li class="bullet-points-circle">
                  Against purchase of Gift Vouchers, Titan Gift Cards & Extended Maintenance Guarantee card.
                </li>
              </ul>
            </li>

            <li>
              Member's signature is subject to verification at the time of purchase or redemption.
            </li>

            <li>
              Unless specifically gift the accrued points by the member, the name on the invoice should be the same as the registered name in the Encircle program for successful earning of points and redemption of points.
            </li>

            <li>
              Subject to terms and conditions mentioned herein, only encircle member can gift the accumulated reward points to his family and friends and all such gift shall be made through www.titanencircle.com only
            </li>

            <li>
              One Time Password is mandatory for redeeming the available points so customer should carry their register mobile number while redeeming encircle points at Company's' outlet. OTP will be sent to their registered mobile# while billing is done & the same should be shared with cashier for point redemption.
            </li>

            <li>
              Redemption of reward points by relative or friend of encircle member shall be subject to production of appropriate photo ID proof and address proof issued by Government agency and as updated by encircle member in www.titanencircle.com and the sufficiency of any address/ID proof documents is as decided by Company and is at the sole discretion of Company which is final and binding.
            </li>

            <li>
              No cash refund will be entertained for purchases made by redeeming reward points in the case of purchase returns.
            </li>

            <li>
              Reward Points once redeemed against a purchase can in no event be re-credited unless there is return & exchange of the product and upon such return and exchange the points will be reversed to the members account and it will be made available for his next purchase/exchanged product. Redemption of Reward points cannot be clubbed with Enrollment Voucher/Gift Voucher redemption.
            </li>

            <li>
              The points credited to the account of the member shall not be redeemed in cash.
            </li>

            <li>
              The credit of reward points against purchases will take at least 2 days in case purchases made at Company's outlet & 16 days in case purchases made at titan.co.in, 90 days in case of www.caratlane.com from the invoice date to reflect in the member's account. However, Titan Company Ltd. shall not be responsible for any delay in having the reward points credited into the member's account.
            </li>

            <li>
              Reward points will be given on invoice value net of encircle points redeemed.
            </li>

            <li>
            Titan Encircle membership is valid for Lifetime.         
           
            </li>

            <li>
              Reward points & Tiers will be valid for 24 months from the month of last transaction. If not redeemed within 24 months from the date of last accrual, at the Quarter end of the 24th month, such reward points shall lapse. However, subject to the validity mentioned herein, in case of gift of reward points, by a member to his family and friends, the validity is only for 6 (Six) months from the date of such gift.
            </li>
            <li>
            Points validity will extend only when the member Earn or Redeem Points.

            </li>
            <li>
              For details of Tiers, details of earning of points and upgrading of the Tiers please refer to www.titanencircle.com and Titan Company Limited reserves the right to change/modify the terms and conditions of Tiers, its applicability and up gradation thereof from time to time.
            </li>
           
            <li>
              Titan encircle Welcome Gift Vouchers are valid for Six months from the member enrollment date.
            </li>

            <li>
              Titan encircle Birthday & Wedding anniversary offer will be active in member’s account for 30 days(15 days before Birthday/Wedding anniversary day & 15 days after Birthday/Wedding anniversary day)
            </li>

            <li>
              Member can avail any Birthday/Anniversary offer of their choice at any channel only once. Offer will be active in members account for 30 days (15 days before & after Birthday or Wedding anniversary day). Birthday/Wedding anniversary day offer cannot be clubbed with any ongoing offer. Fastrack, WOT & Helios offer can be availed for maximum upto Rs.1500 only. Titan Eyeplus offer is valid only on House brands & not valid on Titan Ace lenses. Flat 10% off on sarees at Taneira, not applicable on stitching of blouses, finishing services (fall, pico, etc). Member will not be eligible for Points accrual if offer is clubbed with any on going offer. SKINN offer can be availed for maximum UPTO Rs 1000. Tanishq Birthday or Wedding anniversary is UPTO 10% off on Diamond Jewellery & Gold making charges
            </li>
            
            <li>
                Titan Encircle Gift Voucher
                <ul>
                    <li class="bullet-points-circle">
                        Titan Encircle Gift voucher can be activated by Member as well as Non member
                    </li>
                    <li class="bullet-points-circle">
                        Gift voucher validity changes from one Campaign to another
                    </li>
                    <li class="bullet-points-circle">
                        If a Non encircle member is trying to activate the Gift voucher then he will be enrolled as a member & then points will get credited to members Titan encircle account
                    </li>
                    <li class="bullet-points-circle">
                        If existing encircle member is trying to activate then post successful validation required points will be credited to his/her titan encircle account
                    </li>
                    <li class="bullet-points-circle">
                        To redeem Points at Showroom or online, user has to mention the same Mobile number used to activate the Gift Voucher
                    </li>
                    <li class="bullet-points-circle">
                        Gift Voucher can be used only once
                    </li>
                    <li class="bullet-points-circle">
                        Each Titan encircle point is worth Re 1
                    </li>
                    <li class="bullet-points-circle">
                        Multiple GVs cannot be redeemed using one mobile number
                    </li>
                </ul>
            </li>
            
            <li>
              Now members can Refer their Friends to Titan Encircle program & Earn Bonus Points
              <ul>
                  <li class="bullet-points-circle">
                      Member Can refer their Friend on following path https://titanencircle.com/referrals/login
                  </li>
                  <li class="bullet-points-circle">
                      Member can refer maximum upto 10 Friends
                  </li>
                  <li class="bullet-points-circle">
                      Confirmation message will trigger to both Referrer as well as Referral
                  </li>
                  <li class="bullet-points-circle">
                    Referral need to register themselves on the link which they have received on their mobile by validating their Registered mobile number through OTP & keying in other details
                  </li>
                  <li class="bullet-points-circle">
                    Referral will be registered with Titan Encircle program post above step
                  </li>
                  <li class="bullet-points-circle">
                      Referrer will get Bonus points only when referrer transacts with any of the Titan Brands
                  </li>
                  <li class="bullet-points-circle">
                      Points will be credited to Referrer's Titan Encircle account with in 48 hours from the Referral transaction
                  </li>
                  <li class="bullet-points-circle">
                        Credited points are valid for 2 years from credited date
                  </li>
              </ul>
            </li>
            
            <li>
                Now members can avail encircle Benefits & Privileges along with their Friends & Families by creating a Friends & Family account at titanencircle.com
                <ul>
                    <li class="bullet-points-circle">
                        Member initiating addition of member is Primary member.
                    </li>
                    <li class="bullet-points-circle">
                        Member being added to a Family & Friends account is Secondary member.
                    </li>
                    <li class="bullet-points-circle">
                        Only 9 members can be added in a Friends & Family(F&F) account by Primary member.
                    </li>
                    <li class="bullet-points-circle">
                        Only Friend, Husband, Wife, Brother, Sister, Father, Mother, son & Daughter can be added in Friends & Family account.
                    </li>
                    <li class="bullet-points-circle">
                        Shared OTP has to be validated within fifteen minutes from its delivery at Primary & secondary member end.
                    </li>
                    <li class="bullet-points-circle">
                        All member points will be pooled in to single account in a F&F account.
                    </li>
                    <li class="bullet-points-circle">
                        Points earning & burning will happen from the same account for all F&F members.
                    </li>
                    <li class="bullet-points-circle">
                        When members are De-linked from the account, De-linked member can’t reclaim his pooled points in a F&F account.
                    </li>
                    <li class="bullet-points-circle">
                        Highest tier in the group will be applicable for all members of group.
                    </li>
                    <li class="bullet-points-circle">
                        Linking & De-linking will be effective within 24 hours from its initiation.
                    </li>
                    <li class="bullet-points-circle">
                        When Goods are returned by De-linked member for the items which he/she bought when he/she was a F&F member in the account, at the time points will be credited back to F&F account but not to his individual account.
                    </li>
                    <li class="bullet-points-circle">
                        When member is De-linked, he will be assigned to tier of the group or to the tier which he/she belonged while becoming a member in F&F account (whichever is Lowest tier).
                    </li>
                    <li class="bullet-points-circle">
                        The Highest tier in the group will be mapped to all members & it will be a Group Tier.
                    </li>
                    <li class="bullet-points-circle">
                        Points earning will be on Group Tier.
                    </li>
                    <li class="bullet-points-circle">
                        Eligible amount of all group member will be considered for tier upgrade.
                    </li>
                    <li class="bullet-points-circle">
                        Primary & authorized members have privilege of redeeming pooled points from the group.
                    </li>
                    <li class="bullet-points-circle">
                        If secondary member use the Pooled points and moves out of the group and then does GRN then points reversal will be effected to group (i.e. Points redemption will reverse to source account).
                    </li>
                    <li class="bullet-points-circle">
                        If Primary member is blocked then rest of secondary cards should get De-linked.
                    </li>
                    <li class="bullet-points-circle">
                        If primary member is active and later, if primary member is Blocked then all the members (primary + secondary’s) will get equal point
                    </li>
                    <li class="bullet-points-circle">
                        If secondary member gets blocked then that member will be De-linked automatically. Family points will still remain with family. Secondary member who got blocked basis, which he was De-linked automatically will move with zero point balance and tier would become either his original tier or the family tier whichever is lowest.
                    </li>
                    <li class="bullet-points-circle">
                        Communication is sent to both parties once linking or De-linking is successful.
                    </li>
                    <li class="bullet-points-circle">
                        De-linking process can be initiated by primary or secondary member
                    </li>
                    <li class="bullet-points-circle">
                        The group will dissolve if primary member is De-linked from the group and all secondary members will fall back to Tier at which they have entered the group. All secondary member will be Delinked with tier whichever is lowest “entered tier” or “group tier” and points will be distributed equally
                    </li>
                    <li class="bullet-points-circle">
                        After De-linking the members will fall back to Tier at which they have entered the group
                    </li>
                    <li class="bullet-points-circle">
                        Individual Member Tier Upgrade & Downgrade rules are applicable in F&F account tier upgrade & downgrade also. If members are not able to accumulate required points to retain the tier within stipulated time then tier will be downgraded to one level below. Attained Tier is valid for 24 months from the upgrade/downgrade date
                    </li>
                </ul>
            </li>

            <li>
              Some facilities and benefits offered to members of the Titan Encircle program which may be provided by any organization/s with whom Company have some arrangements however, over which Company have no control. Company does not guarantee or warranty that, such benefits and facilities will be available, nor will it be liable for any loss or damage arising from the provision or non- provision, whether in whole or in part, of any such benefits or facilities. However, in the event of any problems faced in availing such benefits or facilities, members are requested to inform the Company.
            </li>

            <li>
              The Titan Encircle membership is only for individual use and not for corporate purchases. The Titan Encircle membership is non-transferable.
            </li>

            <li>
              At any point, the Company reserves the right to withdraw, alter/modify any or all of the membership issued. Company will have limited liability to the encircle member, only to the extent of the points available in the credit of the member's account. At any point in time, the Company shall not be liable to the Primary Member or his/her Family Members/Friends for any inter-se disputes between them.
            </li>

            <li>
              For breach of any of these terms and conditions by the member or failure to pay for purchases made, the member shall lose his accumulated reward points.
            </li>

            <li>
              Titan Company Ltd. reserves the right to refuse to award points or redeem accumulated points, for any breach of these conditions or failure to pay for purchases.
            </li>

            <li>
              Company will maintain and update in electronic form data that is provided by Members at the time of enrollment or through subsequent contact. Company reserves hereby the right and Member/family member hereby agrees & authorize Company to share his/her data with agencies or organizations for the provision of member services, facilities & benefits and on requirement of any Government agencies/authorities.
            </li>

            <li>
              Titan Encircle membership will be issued solely at the discretion of the management and the final decision on all matters relating to the membership shall rest with Titan Company Ltd.
            </li>

            <li>
              This membership is not valid for the staff of Titan Company Ltd, Business Associates including staff of Business Associates of Titan Company Ltd., unless specially approved.
            </li>

            <li>
              At the sole discretion of the Company, all special offers, updates & account activity details will be communicated to the respective encircle Primary Member/Family Member/his/her Friend through SMS, direct mail and/or email. However, Company shall not be responsible in any manner for any loss, delayed, incorrect or incomplete communications.
            </li>

            <li>
              Being a part of Titan Encircle program encircle members hereby authorize Titan Company Limited to call/sms/email to communicate on offers from us or any company associated with us
            </li>

            <li>
              Members are requested to update any change in their address and profile from time to time. Contact the Titan encircle desk at our outlets or write to: Netcarrots Loyalty Services, ‘Legacy’, 6, Convent Road, 1st Floor, Richmond Road, Bangalore-560025, India. Email: encircle@titan.co.in to update any change in their address and profile other than mobile number. Members are required to send a communication from their registered email id to our customer care help desk at customercare@titan.co.in or make a call at out customer care contact number 18001084826 only to update any change in their mobile number.
            </li>
            
            <li>
                Company reserves the right to change, cancel and modify the terms & conditions, reward structure relating to the Titan encircle Program including discontinuation of the program itself. Company will not be held responsible if any of the participating brands withdraw from the Titan encircle program.
            </li>
            
            <li>
                In the event of a dispute between Primary Member and Company and/or between Family Member/Friend of Primary Member and Company, with regard to or in relation to the Titan encircle program, the decision of Company shall be deemed final. All such disputes shall be subject to exclusive jurisdiction of Courts at Bangalore only.
            </li>
          </ol>
        </div>
      </div>



      <div class="col-lg-12">
        <h2 class="heading-light-1">
        Titan Encircle Valentines day exclusive Terms & Conditions
        </h2>
        <div class="terms-and-condition-content">
          <ol>
            <li>
            As an Encircle member you enjoy a special discount of 5% on the products listed on the encircle website page- https://www.titanencircle.com/valentines-day-exclusive
            </li>

            <li>
            Offer is valid from 3rd Feb till 14th Feb 2023 only on the above mentioned URL.
            </li>
            <li>
            This offer is an online exclusive offer & eligible for the products listed on https://www.titanencircle.com/valentines-day-exclusive only. This offer is not available on our retail stores.
          </li>
          <li>
          Additional discount can be availed by applying promo code "ENCVAL", at the time of checkout from the brand site.
          </li>
          <li>
          Coupon code can be applied in "Enter Voucher Code" section in the cart page.
          </li>
          <li>
          Gift Card & Encircle points can be redeemed on this offer
          </li>
          <li>
          The coupon is applicable on select discounted and non-discounted items.
          </li>

          <li>
          Company has the right to withdraw the offer any time without prior notice.
          </li>

          <li>
          This coupon cannot be clubbed with any other offer/code.
      </li>
      <li>
      This coupon will not be applicable if order is returned.
      </li>
      <li>
      The coupon will be applicable only once per customer.
      </li>
      <li>
      For any queries, contact our customer care.
      </li>
      <li>
      This offer is valid till the stock last.
      </li>
          </ol>
        </div>
      </div>


      
      <div class="col-lg-12">
        <h2 class="heading-light-1">
        Titan Encircle EazyDiner Terms & Conditions
        </h2>

        <div class="terms-and-condition-content">
          <ol>
            <li>
            The offer is valid on purchase only of the products listed on https://www.titanencircle.com/valentines-day-exclusive , that are part of the Valentine's Day collection. The purchase needs to get completed using the discount code ENCVAL. 
            </li>
            <li>The coupon code to take this membership free , would be provided via sms from Titan Co post product purchase within 3 days of purchase</li>
            <li>The offer is valid for EazyDiner users only, so to get complimentary membership the user needs to register on EazyDiner using the link provided. 






            <ul>
                    <li class="bullet-points-circle">The offer is valid for 3 months EazyDiner Prime membership.</li>
                    <li class="bullet-points-circle">The memberships unlock deals with 25% to 50% discounts at over 2000 restaurants in India and Dubai.</li>
                    <li class="bullet-points-circle">All 1+1 deals are valid for a minimum of 2 guests. Example - In the case of 3 guests, 2 guests will be charged, and 1 guest will eat for free, in the case of 5 guests, 3 guests will be charged, and 2 guests will eat for free.</li>
                    <li class="bullet-points-circle">Please inform the restaurant about your reservation through EazyDiner upon your arrival at the restaurant to have a hassle-free experience.</li>
                    <li class="bullet-points-circle">EazyDiner shall not be liable for the experience at the partner restaurant as we only assist in fulfilling the reservations with the special offers</li>
                    <li class="bullet-points-circle">EazyDiner shall not be liable if any restaurant is temporarily or permanently shut</li>
                    <li class="bullet-points-circle">The offer money cannot be refunded in cash or kind for any reason whatsoever.</li>
                    <li class="bullet-points-circle">Not all restaurants bookable on EazyDiner will have prime deals. Some restaurants may also have non-prime deals. All discounts are mentioned clearly against each deal for all restaurants bookable on EazyDiner.</li>
                    <li class="bullet-points-circle">The restaurants and the offers are dynamic as we constantly improve the product, hence the offers at restaurants may change without any prior notice. In case there are any concerns, please call the EazyDiner Prime Concierge at 786 100 4400</li>
                    <li class="bullet-points-circle"> The highlighted savings of Rs.1000 are for a table of 4 and on a blended average in over 2000+ premium restaurants. These savings may vary depending on the number of people dining per table and the average cost per diner.</li>

            </ul>
            </li>
           
            </ol>




            </div>
            </div>










    </div>
    @endif
    @if(!$country_selected || ($country_selected && $country == 'uae'))
    <div class="row" style="margin-top: {{$margin_top}}px;">
      <div class="col-lg-12">
        <br/>
        <h2 class="heading-light-1">
        {{ $title2 }}
        </h2>
        
        <div class="terms-and-condition-content">
          <ol>
            <li>
            Membership is voluntary and open to individuals who have attained the age of 18 and who are residents of the United Arab Emirates or Indian Citizens visiting the United Arab Emirates.  Members shall be recognized through concerned individual’s UAE or Indian mobile numbers registered with the Company and verification of their address. 
            </li>

            <li>
            The Titan Encircle Program membership is non-transferable.
            </li>

            <li>
            No physical membership cards will be issued to the enrolled members.
            </li>

            <li>
            Reward points can be redeemed only at any Company operated stores and/or kiosks of the brands "Tanishq" and "Titan" across UAE ("Stores").  It may be noted that points cannot be availed or redeemed, as the case may be, for purchases made from stores of authorised dealers.
            </li>

            <li>
            To earn points, while making purchases at the Stores across UAE, members shall be required to mention their Encircle Membership number or registered mobile number to the cashier before billing. Points will be credited only when membership number or registered mobile number is provided while billing. Please ensure to check mobile number matching with your name and update us with change of mobile number by visiting the stores or by calling the store at +971 42281458, +971 565475393. 
            </li>

            <li>
            The value of one point is equal to AED 1. However, reward points cannot be redeemed for cash. 
            </li>

            <li>
            Encircle Reward Points issued by Company to the members are free and is without any consideration to be paid by member who is enrolled. The allotment and redemption of reward points will be solely at the discretion of Company. 
            </li>

            <li>
            Titan Encircle Program benefit cannot be clubbed with any other offer or discount, viz. reward points will not be awarded in the following cases:
              <ul>
                <li class="bullet-points-circle">
                Special offers / promotions / items excluded by the management
                </li>

                <li class="bullet-points-circle">
                Points Earning/Accrual will not be applicable during the following schemes: Activation/Consumer scheme, Best Buy, End Of Season Sale(EOSS), Discount given during all exchange offers, Gift With Purchase offer
                </li>

                <li class="bullet-points-circle">
                Against the purchase of gold coins/bars and silver articles.
                </li>

                <li class="bullet-points-circle">
                Against purchase of Gift Vouchers, Gift Cards & Extended Maintenance Guarantee card.
                </li>
              </ul>
            </li>

            <li>
            The name on the invoice should be the same as the registered name in the Encircle Program for successful earning of points and redemption of points.
            </li>

            <li>
            One Time Password is mandatory for redeeming the available points, so Members should carry their phone with the registered mobile number while redeeming Encircle points at Company's' outlet. OTP will be sent to the registered mobile number while billing is done and the same should be shared with cashier for point redemption.
            </li>

            <li>
            Redemption of reward points by relative or friend of an Encircle Member shall be subject to production of appropriate photo ID proof and address proof issued by Government authority and the sufficiency of any address/ID proof documents is as decided by Company and is at the sole discretion of Company, which is final and binding.
            </li>

            <li>
            Reward Points once redeemed against a purchase can in no event be re-credited unless there is return or  exchange of the product and upon such return and/or exchange, the points will be reversed to the Member’s account and it will be made available for their next purchase/exchanged product. No cash refund will be entertained for purchases made by redeeming reward points in the case of purchase returns.
            </li>

            <li>
            The credit of reward points against purchases will take at least 2 days from the invoice date to reflect in the Member's account. However, Titan shall not be responsible for any delay in having the reward points credited into the member's account.
            </li>

            <li>
            Reward points will be given on invoice value net of Encircle Points redeemed.
            </li>

            <li>
            Titan Encircle membership is valid for Lifetime.
            </li>

            <li>
            Reward points and Tiers will be valid for 24 months from the month of last transaction. If not redeemed within 24 months from the date of last transaction, such reward points shall lapse.
            </li>

            <li>
            For details of Tiers, details of earning of points and upgrading of the Tiers please refer to www.titanencircle.com and the Company reserves the right to change/modify the terms and conditions of Tiers, its applicability and up gradation thereof from time to time.
            </li>

            <li>
            Encircle Program "Birthday and Wedding anniversary" offer will be active in member’s account for 30 days (15 days before Birthday/Wedding anniversary day and 15 days after Birthday/Wedding anniversary day). Member can avail any Birthday/Anniversary offer of their choice only once during the validity of this offer. Birthday/Wedding anniversary day offer cannot be clubbed with any ongoing offer. The offers shall vary from time to time and may be checked at the applicable store/kiosk.
            </li>

            <li>
            For purchase of Tanishq products for a customer’s Birthday or Wedding anniversary, discount is UPTO 10% off on Diamond Jewellery and Gold making charges.
            </li>

            <li>
            Some facilities and benefits may be offered to Members of the Titan Encircle Program by organization/s with whom the Company have some arrangements, over which Company has no control. Company does not guarantee or warrants that such benefits and facilities will be available, nor will it be liable for any loss or damage arising from the provision or non- provision, whether in whole or in part, of any such benefits or facilities.  Company will not be held responsible if any of the participating brands withdraw from the Titan Encircle Program. However, in the event of any problems faced in availing such benefits or facilities, Members are requested to inform the Company.
            </li>

            <li>
            At any point, the Company reserves the right to withdraw, alter/modify any or all of the membership issued. Company will have limited liability to the Encircle Member, only to the extent of the points available in the credit of the Member's account. 
            </li>

            <li>
            For breach of any of these terms and conditions by the Member or failure to pay for purchases made, the Member shall lose his accumulated reward points.
            </li>

            <li>
            Titan reserves the right to refuse award points or redeem accumulated points, for any breach of these conditions or failure to pay for purchases.
            </li>

            <li>
            Company will maintain and update in electronic form, data that is provided by Members at the time of enrolment or through subsequent contact. You herby consent and permit the Company to share your data to the Company’s parent companies, associate and affiliates, with agencies or organizations for the provision of Member services, facilities and benefits and on requirement of any Government agencies/authorities.
            </li>

            <li>
            Titan Encircle Program membership will be issued solely at the discretion of the Company and the final decision on all matters relating to the membership shall rest with the Company.
            </li>

            <li>
            This membership is not valid for the staff of the Company, Business Associates including staff of Business Associates of the Company, unless specially approved.
            </li>

            <li>
            Company reserves the right to change, cancel and modify the terms & conditions, reward structure relating to the Titan Encircle Program including discontinuation of the Program itself. 
            </li>


          </ol>
        </div>
      </div>
    </div>
    @endif
  </div>
</section>
@endsection