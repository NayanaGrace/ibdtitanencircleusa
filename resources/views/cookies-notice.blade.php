@extends('layouts.app')
@section('title', 'Cookies Notice')
@section('bodyclass', 'cookies-notice')
@section('content')


 <section class="faqs bg-yellow-white first-section-pt">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
 <!-- OneTrust Privacy Notice start -->
<!-- Container in which the privacy notice will be rendered -->
<div id="otnotice-3b63648a-4dbd-40ad-9550-a11fcd7d4e6b" class="otnotice"></div>

                         </div>
                         </div>
                         </div>
                         </section>
              
                         <script src="https://privacyportal-in-cdn.onetrust.com/privacy-notice-scripts/otnotice-1.0.min.js" type="text/javascript" charset="UTF-8" id="otprivacy-notice-script">
      settings="eyJjYWxsYmFja1VybCI6Imh0dHBzOi8vcHJpdmFjeXBvcnRhbC1pbi5vbmV0cnVzdC5jb20vcmVxdWVzdC92MS9wcml2YWN5Tm90aWNlcy9zdGF0cy92aWV3cyJ9"
    </script>
 
  <script type="text/javascript" charset="UTF-8">
      // To ensure external settings are loaded, use the Initialized promise:
      OneTrust.NoticeApi.Initialized.then(function() {
        OneTrust.NoticeApi.LoadNotices(["https://privacyportal-in-cdn.onetrust.com/53ec83ca-0693-46f3-a55b-110c3f8f5a64/privacy-notices/3b63648a-4dbd-40ad-9550-a11fcd7d4e6b.json"]);
      });
    </script>
   
  <!-- OneTrust Cookies Notice end -->              
@endsection