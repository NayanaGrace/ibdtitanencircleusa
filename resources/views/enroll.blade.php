@extends('layouts.app')
@section('title', 'Enroll')
@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link href="/home/css/pages/home_page.css?<?php echo date('Y-m-d_H:i:s'); ?>" rel="stylesheet" integrity="aLTBTeGDitgZY3aUpdqjdiqcSUpEfm66ncTtLb/nx1yNGjQlrWja+Y87ffa47IWx" type="text/css"> 

<style>
.personal-info, .personal-info-min, .life-style, .back-to-profile {
 display:none;
}
</style>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<script src="/assets/js/enroll.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
 $(document).ready(function(){
$(".button__loader11").hide();
/* $('#OneTrustLoyalty').modal('show'); */
$('#OneTrustokey').modal({backdrop: 'static', keyboard: false}, 'show');
/* $('#OneTrustLoyalty').modal({backdrop: 'static', keyboard: false}, 'show'); */
$('.OneTrustSend').click(function (event) {
var birthday =   $("#birthday:checked").length;
var survey =   $("#survey:checked").length;
var offer =   $("#offer:checked").length;
var marketing =   $("#marketing:checked").length;

/* $("#OneTrustForm").hide();
showLoader("#OneTrustForm");  */
$(".OneTrustSend").hide();
$(".OneTrustCancel").hide();
$(".button__loader11").show();
$.ajax({
        type: 'POST',
        data: {
          "_token": $('meta[name="csrf-token"]').attr('content'),
          "birthday": birthday,
          "survey": survey,
          "offer": offer,
          "marketing": marketing,
          
         },
        url: '/consentreceipts',
        success: function(data){

            $('#OneTrustLoyalty').modal('hide');
            var back_link = $('input[name = back_link]').val();
            if(back_link == null || back_link =='') {
                window.location = back_link;

            }
           else {

            $('.back-to-profile').css('display', 'flex');

          }

    }       
});
});

$(document).on('click', '[data-dismiss="modal"]', function(){
  
  // alert('what you want to do');

  $('#OneTrustLoyalty').modal('hide');

  var back_link = $('input[name = back_link]').val();
            if(back_link == null || back_link =='') {
                window.location = back_link;

            }
           else {

            $('.back-to-profile').css('display', 'flex');

          }
  
  });

  $(document).on('click', '.OneTrustCancel', function(){


    var back_link = $('input[name = back_link]').val();
            if(back_link == null || back_link =='') {
                window.location = back_link;

            }
           else {

            $('.back-to-profile').css('display', 'flex');

          }
   
  });

$(document).on('click', '.OneTrustoky', function(){

    var mobile ={{session('SESS_MOBILE')}};
    
    $.ajax({
        type: 'POST',
        data: {
          "_token": $('meta[name="csrf-token"]').attr('content'),
          "mobile": mobile
         },
        url: '/onetrustsignup',
        success: function(data){

           /*  $('#OneTrustLoyalty').modal('hide');
            var back_link = $('input[name = back_link]').val();
            if(back_link == null || back_link =='') {
                window.location = back_link;

            }
           else {

            $('.back-to-profile').css('display', 'flex');

          } */

          $('#OneTrustokey').modal('hide');
          $('input[name = logged_country]').val('usa1');

    }       
});
  /*   $('#OneTrustokey').modal('hide');
    $('input[name = logged_country]').val('usa1'); */

});

$(document).on('click', '.okycancel', function(){
    window.location = '/logout';

});

});

</script>
@endsection
@section('content')
<section class="update-profile">
    <div class="container">
        <div class="row">
            <div class="updat-hd top-hds">Hi! We would like to know you better to offer you a great experience. Please help us
                with your details below</div>

            <div class="steps clearfix my-5rem my-md-3rem">
                <ul role="tablist">
                    <li role="tab" aria-disabled="false" class="first current d-flex flex-column justify-content-center align-items-center" aria-selected="true">
                        <a href="#form-total-h-0" id="form-total-t-0" aria-controls="form-total-p-0">
                            <span class="current-info audible"></span>
                            <span class="title active b-info"></span>
                        </a>
                        <span class="mt-2 head active b-info">Basic Information</span>
                    </li>

                    <li role="tab" aria-disabled="false" aria-selected="true" class="d-flex flex-column justify-content-center align-items-center">
                        <a href="#form-total-h-0" id="form-total-t-0" aria-controls="form-total-p-0">
                            <span class="current-info audible"></span>
                            <span class="title p-info"></span>
                        </a>
                        <span class="mt-2 head p-info">Personal Information</span>
                    </li>

                    <li role="tab" aria-disabled="false" class="last d-flex flex-column justify-content-center align-items-center" aria-selected="true">
                        <a href="#form-total-h-2" id="form-total-t-2" aria-controls="form-total-p-2">
                            <span class="title l-info"></span>
                        </a>
                        <span class="mt-2 head l-info">Lifestyle Preferences</span>
                    </li>
                </ul>
            </div>
        </div>
            <div class="update-profile-div">
                <form id="updateprofile" class="updateprofile w-80 w-md-90">
            @csrf
                <div class="row updat-form basic-info">
                    <div class="updat-form-hd">BASIC INFORMATION</div>

            <input value="<?=session('SESSION_BACKLINK')?>" name="back_link" id="prelogin_redeem" class="name-input" type="hidden">
            <input value="one_trust_co" name="logged_country" id="logged_country" class="name-input" type="hidden">

          
                    <div class="form-fields row">
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label ps-3">Title</label>
                            <select name="title" class="form-select updat-form-placeh">
                                <option value="">Select Title</option>
                                @foreach(getDoList("profile_title") as $row)
                                <option value="{{$row->title}}">{{$row->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label ps-3">First Name</label>
                            <input value="" type="text" class="form-control updat-form-placeh" name="first_name" id="first_name" placeholder="Enter First Name">
                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label ps-3">Last Name</label>
                            <input value="" type="text" class="form-control updat-form-placeh" id="last_name" name="last_name" placeholder="Enter Last Name">
                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="gender" class="form-label ps-3">Gender</label>
                            <select id="gender" name="gender" class="form-select">
                                <option value="">Select Gender</option>
                                @foreach(getDoList("gender") as $row)
                                <option value="{{$row->value}}">{{$row->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp input-daterange">
                            <label for="" class="form-label ps-3">Date of Birth</label>
                            <input value="" type="date" class="form-control updat-form-placeh datepicker" id="dob" name="dob" placeholder="DD / MM / YYYY">
                          <!--   <span class="fa fa-calendar calen" id="fa-1"></span> -->
                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label updat-form-placeh ps-3">Contact Number</label>
                            <input value="{{session('SESS_MOBILE')}}" type="text" class="form-control updat-form-placeh" id="mobile" name="mobile" placeholder="Ente Contact Number">
                        </div>
                        <div class="col-md-12">
                            <div class="row d-flex justify-content-center">
                                <div class="col-md-8 col-sm-12 form-label-tp email-padding">
                                    <label for="" class="form-label ps-3">Email ID</label>
                                    <input value="" type="email" class="form-control updat-form-placeh" id="email" name="email"
                                        placeholder="Enter your email id">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12  text-center">
                        <button type="button" class="btn profile-update-btn basic-info-btn mt-lg-5 mt-md-5 mt-2" >Next</button>
                    </div>
                </div>
            
                <!--personal information-->
                <div class="row updat-form personal-info">
                    <div class="updat-form-hd">Personal Information</div>
                    <div class="col-md-12">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-8 col-sm-12 form-label-tp ">
                                <label for="" class="form-label">Marital Status</label>
                                <select id="marital_status" name="marital_status" class="form-select updat-form-placeh marital_status">
                                    <option value="">Select Marital Status</option>
                                    @foreach(getDoList("maritalstatus") as $row)
                                    <option  value="{{$row->value}}">{{$row->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  text-center">
                       <button type="button" class="btn profile-update-btn go-back-basic-info mt-lg-5 mt-md-5 mt-2">Previous</button>
                        <button type="button" class="btn profile-update-btn personal-info-btn mt-lg-5 mt-md-5 mt-2">Next</button>
                    </div>
                </div>
                <!--personal information end-->
                <!--personal information2-->
                <div class="row updat-form personal-info-min">
                    <div class="updat-form-hd">Personal Information</div>
                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Martial Status</label>
                        <select id="" class="form-select marital_status_min">
                        <option value="">Select Martial Status</option>
                        @foreach(getDoList("maritalstatus_enroll") as $row)
                        <option value="{{$row->value}}">{{$row->title}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 form-label-tp input-daterange">
                        <label for="" class="form-label">Birthday of Spouse</label>
                        <input value="" type="date" class="form-control updat-form-placeh datepicker" id="spouse_dob" name="spouse_dob" placeholder="DD / MM / YYYY">
                      <!--   <span class="fa fa-calendar calen" id="fa-1"></span> -->
                    </div>
                    <div class="col-md-4 col-sm-12 form-label-tp input-daterange">
                        <label for="" class="form-label updat-form-placeh">Anniversary</label>
                        <input value="" type="date" class="form-control updat-form-placeh datepicker" id="anniversary_date" name="anniversary_date" placeholder="DD / MM / YYYY">
                     <!--    <span class="fa fa-calendar calen" id="fa-1"></span> -->
                    </div>
                    <div class="col-md-12  text-center">
                       <button type="button" class="btn profile-update-btn go-back-basic-info mt-lg-5 mt-md-5 mt-2">Previous</button>
                        <button type="button" class="btn profile-update-btn personal-info-min-btn mt-lg-5 mt-md-5 mt-2">Next</button>
                    </div>
                </div>
                <!--personal information2 end-->
                <!--Lifestyle Preferences-->
                <div class="row updat-form life-style">
                    <div class="updat-form-hd">Lifestyle Preferences</div>
                    <div class="col-md-12">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-8 col-sm-12 form-label-tp ">
                                <label for="" class="form-label">Things you love</label>
                                <select id="multiple-selected" class="form-select updat-form-placeh js-example-basic-multiple" name="things[]" multiple="multiple">
                                    <option>Select Things you love</option>
                                    @foreach(getDoList("interests") as $row)
                                    <option value="{{$row->class}}">{{$row->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  text-center">
                        <button type="button" class="btn profile-update-btn go-back-life-style mt-lg-5 mt-md-5 mt-2">Previous</button>
                      <!--   <button type="submit" class="btn profile-update-btn life-style-btn mt-lg-5 mt-md-5 mt-2">Submit</button> -->
                      <a id="btnLoader" type="submit" class="button__loader btn reffer-btn reffer_btn mt-lg-5 mt-md-5 mt-2" onclick="btnFunction()">
                    <span class="button__text">Submit</span>
</a >
                    </div>
                </div>
                <!--Lifestyle Preferences end-->
            </div>
            </section>
        </form> 
    </div> 
    <section class="update-profile back-to-profile">
    <div class="container">
        <div class="row">
            <div class="updat-hd text-center">That’s all for now. Thanks for filling all the details. We’ll customize offers
                according to the details you have provided. </div>
                <div class="col-md-12  text-center">
                    <a href="/MyProfile" class="btn profile-submit-btn back-btn mt-lg-5 mt-md-5 mt-2">Back to profile</a>
                </div>
        </div>
    </div>

</section>

<!--  One Trust Loyalty Programs form start here-->
<div class="modal-wrapper">
    <div class="modal fade modal-light show" id="OneTrustLoyalty" style="padding-right: 8px;" aria-modal="true" role="dialog">
      <div class="modal-dialog modal-lg modal-dialog-centered align-items-center d-flex justify-content-center">
        <div class="modal-content bg-yellow">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
          <!-- Modal body -->
          <div class="modal-body">
            <h2 class="page-info-heading-1">
            Communication Preferences
            </h2>
          <form id="OneTrustForm" autocomplete="off" class="row">
           @csrf
            <div>
             <label class="custom-checkbox">
              Marketing Offer and Updates	
              <input id="marketing" name="marketing" type="checkbox">
              <span class="checkboxmarker"></span>
              
            </label>
            </div>

            <div>
             <label class="custom-checkbox">
              Personalized offers	
              <input id="offer" name="offer" type="checkbox">
              <span class="checkboxmarker"></span>
              </label>
            </div>

            <div>
              <label class="custom-checkbox">
            <!--   Feedback &amp; Suggestions to improve our Services -->
              Survey communication
              <input id="survey" name="survey" type="checkbox">
              <span class="checkboxmarker"></span>
              </label>
            </div>
           
            <div>
             <label class="custom-checkbox">
             Birthday & Anniversary Greeting
              <input id="birthday" name="birthday" type="checkbox">
              <span class="checkboxmarker"></span>
             </label>
            </div>
                <div class="btn-grp d-flex align-items-center col-md-6 col-5 p-0 mt-5">
                   <a href="javascript:void(0)" class="profile-update-btn OneTrustSend" style="box-shadow: none"><span>Submit</span></a>

                <a class="button__loader button__loader11">
                <span class="button__text">Loading...</span>
              </a>
                </div>

                <div class="btn-grp d-flex align-items-center col-md-6 col-5 p-0 mt-5">
                <a href="javascript:void(0)" class="profile-update-btn OneTrustCancel" style="box-shadow: none;background: #FFf;color: #000;border: 0.5px solid #e1e1e1;">
                    <span>Cancel</span>
                </a>
                </div>

                
            </form>
          </div>
        
        </div>
      </div>
    </div>
    
  
</div>
<!--  One Trust Loyalty Programs form ends here-->
<!--  One Trust Loyalty Programs  concent start form here-->

    <div class="modal fade modal-light show" id="OneTrustokey" style="padding-right: 8px;" aria-modal="true" role="dialog">
      <div class="modal-dialog modal-lg modal-dialog-centered align-items-center d-flex justify-content-center">
        <div class="modal-content bg-yellow">
  
          <div class="modal-body">
            <p style="color: #000;
    font-size: 1.5714285714rem;
    line-height: 2rem;
    overflow: hidden;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    margin: 16px 17px 25px 17px;
    min-height: 56px">
            I Agree to enrol into Loyalty program &  the Account related communication </p>
          <form id="OneTrustoky" class="row">
           @csrf
                <div class="btn-grp d-flex align-items-center col-md-6 col-5 pl-3 mt-1">
                   <a href="javascript:void(0)" class="btn btn-member OneTrustoky" style="box-shadow: none"><span>OK</span></a>
              <!--  </div>
               <div class="btn-grp d-flex align-items-center col-md-6 col-5 pr-12 mt-1"> -->
                <a href="javascript:void(0)" class="btn btn-member okycancel" style="box-shadow: none;background: #FFf;color: #000;border: 0.5px solid #e1e1e1;">
                    <span>Cancel</span>
                </a>
                </div>
            </form>
          </div>
        
        </div>
      </div>
    </div>


<!--  One Trust Loyalty Programs  concent ends here-->
<script>
/* $(document).ready(function(){

$('.input-daterange').datepicker({
   format: 'dd/mm/yyyy',
   autoclose: true,
     calendarWeeks : true,
    clearBtn: true,
     disableTouchKeyboard: true
 });

 }); */

$(document).ready(function() {
       /*  $('#multiple-selected').multiselect(); */
       $('.js-example-basic-multiple').select2();
       $('.select2-container').css("width","100%")
    });
</script>
@endsection