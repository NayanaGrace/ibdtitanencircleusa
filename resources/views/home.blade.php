@extends('layouts.app')
@section('title', 'Welcome to the World of Privileges')
@section('bodyclass', 'home')
@section('content')
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.3/jquery.scrollTo.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
@endsection
<section class="banner-new" style="background:url('assets/titan-bg.png');background-size: cover;background-repeat: no-repeat;">
<div class="owl-carousel owl-theme">
    <div class="item"> <div class="container"><a href="{{$_ENV['APP_URL']}}"><img width="100%" src="assets/banner-1.png"> </a></div> </div>
    <div class="item"> <div class="container"><img width="100%" src="assets/banner-2.png"> </div> </div>
</div>
</section>

<section class="reward">
  <div class="container py-5rem py-md-4rem py-sm-2rem">
    <div class="row">
      <h1 class="reward-head">Get Rewarded Now</h1>
      <div class="reward-step">
        
        <div class="d-flex align-items-center flex-column justify-content-between reward-logo mt-md-0 mt-3">
          <a href="/" class="d-flex align-items-center flex-column justify-content-between">
            <img alt="register" src="assets/image/Step-1.png" class="img-fluid">
            <h3 class="mt-md-3 my-3 text-uppercase">Register</h3>
          </a>
        </div>

        <div class="d-flex align-items-end justify-content-center h-50">
          <img alt="register" src="assets/image/Arrowright.png" class="img-fluid arrowright">
        </div>
        
        @if(Session::get('SESS_USER_INFO')??null)
        
        <div class="d-flex align-items-center flex-column justify-content-between reward-logo mt-md-0 mt-3">
          <a href="{{$_ENV['APP_URL']}}" class="d-flex align-items-center flex-column justify-content-between">
            <img alt="refer a friend" src="assets/image/Step-2.png" class="img-fluid">
            <h3 class="mt-md-3 my-3 text-uppercase">Refer</h3>
          </a>
        </div>

        @else

        <div class="d-flex align-items-center flex-column justify-content-between reward-logo mt-md-0 mt-3">
          <a class="d-flex align-items-center flex-column justify-content-between">
            <img alt="refer a friend"  src="assets/image/Step-2.png" class="img-fluid pre_click" data-id="/myprofile/referral">
            <h3 class="mt-md-3 my-3 text-uppercase pre_click" data-id="{{$_ENV['APP_URL']}}">Refer</h3>
          </a>
        </div>

        @endif

        <div class="d-flex align-items-end justify-content-center h-50">
          <img alt="refer a friend" src="assets/image/Arrowright.png" class="img-fluid arrowright">
        </div>

        @if(Session::get('SESS_USER_INFO')??null)
        <div class="d-flex align-items-center flex-column justify-content-between reward-logo mt-md-0 mt-3">
          <a href="/games" class="d-flex align-items-center flex-column justify-content-between">
            <img alt="play with us game zone"  src="assets/image/Step-3.png" class="img-fluid">
            <h3 class="mt-md-3 my-3 text-uppercase">Play With Us</h3>
          </a>
        </div>
        @else
        <div class="d-flex align-items-center flex-column justify-content-between reward-logo mt-md-0 mt-3">
          <a  class="d-flex align-items-center flex-column justify-content-between">
            <img alt="play with us game zone"  src="assets/image/Step-3.png" class="img-fluid pre_click" data-id="/games">
            <h3 class="mt-md-3 my-3 text-uppercase pre_click" data-id="{{$_ENV['APP_URL']}}">Play With Us</h3>
          </a>
        </div>
        @endif
        <div class="d-flex align-items-end justify-content-center h-50">
          <img alt="play with us game zone"  src="assets/image/Arrowright.png" class="img-fluid arrowright">
        </div>

        <div class="d-flex align-items-center flex-column justify-content-between reward-logo mt-md-0 mt-3">
          <a href="{{$_ENV['APP_URL']}}" class="d-flex align-items-center flex-column justify-content-between">
            <img alt="update your profile"  src="assets/image/Step-4.png" class="img-fluid">
            <h3 class="mt-md-3 my-3 text-uppercase">Update Profile</h3>
          </a>
        </div>
        
        <div class="d-flex align-items-end justify-content-center h-50">
          <img alt="shop" src="assets/image/Arrowright.png" class="img-fluid arrowright">
        </div>

        <div class="d-flex align-items-center flex-column justify-content-between reward-logo mt-md-0 mt-3">
          <a href="{{$_ENV['APP_URL']}}" class="d-flex align-items-center flex-column justify-content-between">
            <img alt="shop"  src="assets/image/Step-5.png" class="img-fluid">
            <h3 class="mt-md-3 my-3 text-uppercase">Shop</h3>
          </a>
        </div>

      </div>
    </div>
  </div>
</section>




<script
  src="https://maps.googleapis.com/maps/api/js?key={{config('map.GOOGLE_API_KEY')}}&callback=initMapGoogle&libraries=geometry"
      async>
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script>
function initMapGoogle(locations) {
  var store_latitude    = locations[0][1];
  var store_longitude   = locations[0][2];
var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 12,
  center: new google.maps.LatLng(store_latitude, store_longitude),
  mapTypeId: google.maps.MapTypeId.ROADMAP
});

var infowindow = new google.maps.InfoWindow();
var marker, i;

for (i = 0; i < locations.length; i++) { 
  marker = new google.maps.Marker({
    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
    map: map,
    title:locations[i][0],
    icon: {
      path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
      fillColor: '#000',
      fillOpacity: 1,
      strokeColor: '#f5f4f5',
      strokeWeight: 2,
      scale: 1,
    }
  });

  google.maps.event.addListener(marker, 'click', (function(marker, i) {
    return function() {
      infowindow.setContent(locations[i][0]);
      infowindow.open(map, marker);
    }
  })(marker, i));
}
}

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
</script>
