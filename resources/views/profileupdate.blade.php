@extends('layouts.app')
@section('title', 'Update Profile')
@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<style>
.personal-info, .personal-info-min, .life-style, .back-to-profile {
 display:none;
}
</style>
@if(Session::get('log_country',null)==='usa')
<link href="{{$_ENV['APP_URL']}}/home/css/pages/home_page.css?<?php echo date('Y-m-d_H:i:s'); ?>" rel="stylesheet" integrity="aLTBTeGDitgZY3aUpdqjdiqcSUpEfm66ncTtLb/nx1yNGjQlrWja+Y87ffa47IWx" type="text/css">
@endif
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<script src="{{$_ENV['APP_URL']}}/assets/js/profileupdate.js?<?php echo date('Y-m-d_H:i:s'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.3/jquery.scrollTo.min.js"></script>
<script>
$(document).ready(function(){
    $(".button__loader11").hide();
    $('#OneTrustokey').modal({backdrop: 'static', keyboard: false}, 'show');
  $('#OneTrustokey').modal('show'); 

 
//   $('#Thankyoumodal').modal('show'); 

  $(document).on('click', '.ThankYouCancel', function(){
    $('#Thankyoumodal').modal('hide');
    $('.back-to-profile').css('display', 'flex');
    $(".update-popup").hide();
    $.scrollTo($('.updat-hd'), 0);
  });

});

$(document).on('click', '.OneTrustoky', function(){


$(".button__loader11").show();
$(".OneTrustoky").hide();
$(".okycancel").hide();

var APP_URL = $("input[name=APP_URL]").val();

var mobile ={{$Data['mobile']}};


$.ajax({
    type: 'POST',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "mobile": mobile
     },
    url: APP_URL + '/onetrustsignup',
    success: function(data){
      $('#OneTrustokey').modal('hide');
      $(".button__loader11").hide();

}       
});


});

$(document).on('click', '.okycancel', function(){

var APP_URL = $("input[name=APP_URL]").val();

window.location = APP_URL + '/logout';

});
</script>
@endsection
@section('content')
<?php
$marital_status = $Data['marital_status'];
if($marital_status==1) {
    $marital_status="No";
} else {
    $marital_status="Yes";
}

$maritalstatus_enroll = [

    array(
      "title" => "Single", 
      "value" => "No"
    ),
    array(
      "title" => "Married", 
      "value" => "Yes"
    )
    ];

?>
<section class="update-profile"  style="min-height: calc(100vh - 111px);">  
    <div class="container">
        <div class="row">
            <div class="updat-hd top-hds">Hi! We would like to know you better to offer you a great experience. Please help us
                with your details below</div>

            <div class="steps clearfix my-5rem my-md-3rem">
                <ul role="tablist">
                    <li role="tab" aria-disabled="false" class="first current d-flex flex-column justify-content-center align-items-center" aria-selected="true">
                        <a href="#form-total-h-0" id="form-total-t-0" aria-controls="form-total-p-0">
                            <span class="current-info audible"></span>
                            <span class="title active b-info"></span>
                        </a>
                        <span class="mt-2 head active b-info">Basic Information</span>
                    </li>

                    <li role="tab" aria-disabled="false" aria-selected="true" class="d-flex flex-column justify-content-center align-items-center">
                        <a href="#form-total-h-0" id="form-total-t-0" aria-controls="form-total-p-0">
                            <span class="current-info audible"></span>
                            <span class="title p-info"></span>
                        </a>
                        <span class="mt-2 head p-info">Personal Information</span>
                    </li>

                    <li role="tab" aria-disabled="false" class="last d-flex flex-column justify-content-center align-items-center" aria-selected="true">
                        <a href="#form-total-h-2" id="form-total-t-2" aria-controls="form-total-p-2">
                            <span class="title l-info"></span>
                        </a>
                        <span class="mt-2 head l-info">Address / Location</span>
                    </li>
                </ul>
            </div>
        </div>
        


            <div class="update-profile-div">
                <form id="updateprofile" class="updateprofile w-80 w-md-90">
            @csrf
                <div class="row updat-form basic-info">
                    <div class="updat-form-hd">BASIC INFORMATION</div>
                    <div class="form-fields row">
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label ps-3">Title</label>
                            <select name="title" class="form-select updat-form-placeh">
                                <option value="">Select Title</option>
                                @foreach(getDoList('salutation') as $row)
                                <option  @if($Data['title']==$row->title) selected @endif value="{{$row->title}}">{{$row->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label ps-3">First Name</label>
                            <input value="{{$Data['first_name']}}" type="text" class="form-control updat-form-placeh" name="first_name" id="first_name" placeholder="Enter First Name">
                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label ps-3">Last Name</label>
                            <input value="{{$Data['last_name']}}" type="text" class="form-control updat-form-placeh" id="last_name" name="last_name" placeholder="Enter Last Name">
                        </div>
                     
                        <div class="col-md-4 col-sm-12 form-label-tp input-daterange">
                            <label for="" class="form-label ps-3">Date of Birth</label>


                          @if(empty($Data['dob']))
                           <input value="" type="date" class="form-control updat-form-placeh datepicker" id="dob" name="dob" placeholder="DD / MM / YYYY">
                          @else

                          <input value="{{date("Y-m-d", strtotime($Data['dob']))}}" type="date" class="form-control updat-form-placeh datepicker" id="dob" name="dob" placeholder="DD / MM / YYYY">

                          @endif

                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label updat-form-placeh ps-3">Contact Number</label>
                            <input value="{{$Data['mobile']}}" type="text" class="form-control updat-form-placeh" id="mobile" name="mobile" placeholder="Ente Contact Number" readonly>
                        </div>


                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label updat-form-placeh ps-3">Email ID</label>
                            <input value="{{$Data['email']}}" type="text" class="form-control updat-form-placeh" id="email" name="email" placeholder="Enter your email id">
                        </div>
                    </div>

                    <div class="col-md-12  text-center">
                        <button type="button" class="btn profile-update-btn basic-info-btn mt-lg-5 mt-md-5 mt-2" >Next</button>
                    </div>
                </div>
            
                <!--personal information-->
                <div class="row updat-form personal-info">
                    <div class="updat-form-hd">Personal Information</div>
                    <div class="col-md-12">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-8 col-sm-12 form-label-tp ">
                                <label for="" class="form-label">Marital Status</label>
                                <select id="marital_status" name="marital_status" class="form-select updat-form-placeh marital_status">
                                    <option value="">Select Marital Status</option>
                                    @foreach($maritalstatus_enroll as $row)
                                    <option @if($marital_status==$row['value']) selected @endif value="{{$row['value']}}">{{$row['title']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  text-center">
                       <button type="button" class="btn profile-update-btn go-back-basic-info mt-lg-5 mt-md-5 mt-2">Previous</button>
                        <button type="button" class="btn profile-update-btn personal-info-btn mt-lg-5 mt-md-5 mt-2">Next</button>
                    </div>
                </div>
                <!--personal information end-->
                <!--personal information2-->
                <div class="row updat-form personal-info-min">
                    <div class="updat-form-hd">Personal Information</div>
                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Marital Status</label>
                        <select id="" class="form-select marital_status_min">
                        <option value="">Select Marital Status</option>
                        @foreach($maritalstatus_enroll as $row)
                        <option @if($marital_status==$row['value']) selected @endif value="{{$row['value']}}">{{$row['title']}}</option>
                        @endforeach
                        </select>
                    </div>
                
                    <div class="col-md-4 col-sm-12 form-label-tp input-daterange">
                        <label for="" class="form-label updat-form-placeh">Anniversary</label>
                        @if(empty($Data['dob']))
                        <input value="" type="date" class="form-control updat-form-placeh datepicker" id="doa" name="doa" placeholder="DD / MM / YYYY">
                        @else
                        <input value="{{date("Y-m-d", strtotime($Data['doa']))}}" type="date" class="form-control updat-form-placeh datepicker" id="doa" name="doa" placeholder="DD / MM / YYYY">
                        @endif
                    </div>
                    <div class="col-md-12  text-center">
                       <button type="button" class="btn profile-update-btn go-back-basic-info mt-lg-5 mt-md-5 mt-2">Previous</button>
                        <button type="button" class="btn profile-update-btn personal-info-min-btn mt-lg-5 mt-md-5 mt-2">Next</button>
                    </div>
                </div>
                <!--personal information2 end-->
                <!--Lifestyle Preferences-->
                <div class="row updat-form life-style">
                    <div class="updat-form-hd">Address / Location</div>

                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Country</label>
                        <select id="country" name="country" class="form-select select_country">
                        <option value="">Select Country</option>
                       
                         @foreach($country as $row)
                         <option @if($Data['country']==$row->title) selected @endif value="{{$row->title}}">{{$row->title}}</option>
                        @endforeach 


                        

                      <!--   @if(Session::get('logged_in_country',null)=='uae') 
                        <option @if($Data['country']=='United Arab Emirates') selected @endif value="United Arab Emirates">UAE</option>
                        @else
                        <option @if($Data['country']=='India') selected @endif value="United States">United States</option>
                        @endif
                        <option @if($Data['country']=='India') selected @endif value="India">India</option> -->

                    
                        </select>
                    </div>


                    <div class="col-md-4 col-sm-12 form-label-tp">

                        @if($logged_in_country == 'uae')
                            <label for="" class="form-label">Emirates / States</label>
                        @else
                        <label for="" class="form-label">State</label>
                        @endif
                      
                      <!--   <input value="{{$Data['state']}}" type="text" class="form-control updat-form-placeh" id="state" name="state" placeholder="Ente State"> -->

                      <select id="state" name="state" class="form-select updat-form-placeh select_state">
                                <option value="">Select state</option>
                                @foreach($state as $row)
                                <option @if($Data['state']==$row->title) selected @endif value="{{$row->title}}">{{$row->title}}</option>
                                @endforeach
                            </select>
                    </div>


                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">City</label>
                    

                      <select id="city" name="city" class="form-select updat-form-placeh">
                                <option value="">Select city</option>
                                @foreach($city as $row)
                                <option @if($Data['city']==$row->title) selected @endif value="{{$row->title}}">{{$row->title}}</option>
                                @endforeach
                            </select>
                    </div>

                    @if($logged_in_country == 'uae')

                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">City Other</label>
                        <input value="{{$Data['city_others']}}" type="text" class="form-control updat-form-placeh" id="city_others" name="city_others" placeholder="Ente city Others">
                    </div>

                    <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label">Colony</label>
                            <input value="{{$Data['colony']}}" type="text" class="form-control updat-form-placeh" id="colony" name="colony" placeholder="Ente Colony">
                    </div>
                   


                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Street</label>
                        <input value="{{$Data['street_name']}}" type="text" class="form-control updat-form-placeh" id="state" name="street_name" placeholder="Ente street name">
                    </div>


                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">Building Name</label>
                        <input value="{{$Data['building_name']}}" type="text" class="form-control updat-form-placeh" id="building_name" name="building_name" placeholder="Enter Building Name">
                    </div>




                    <div class="col-md-4 col-sm-12 form-label-tp">
                        <label for="" class="form-label">House No</label>
                        <input value="{{$Data['house_no']}}" type="text" class="form-control updat-form-placeh" id="house_no" name="house_no" placeholder="Ente House No">
                    </div>  
                    @endif

                    @if($logged_in_country == 'usa')
                        
                        <input value="{{$Data['city_others']}}" type="hidden"  id="city_others" name="city_others" >
                        <input value="{{$Data['colony']}}" type="hidden"  id="colony" name="colony">
                        <input value="{{$Data['street_name']}}" type="hidden"  id="state" name="street_name" >
                        <input value="{{$Data['building_name']}}" type="hidden"  id="building_name" name="building_name">
                        <input value="{{$Data['house_no']}}" type="hidden" id="house_no" name="house_no">

                        <div class="col-md-4 col-sm-12 form-label-tp">
                                <label for="" class="form-label">Address</label>
                                <input value="{{$Data['address']}}" type="text" class="form-control updat-form-placeh" id="address" name="address" placeholder="Enter Address">
                        </div>
                        <div class="col-md-4 col-sm-12 form-label-tp">
                            <label for="" class="form-label">Zipcode</label>
                            <input value="{{$Data['zipcode']}}" type="text" class="form-control updat-form-placeh" id="zipcode" name="zipcode" placeholder="Enter Zipcode">
                        </div>
                    @endif


                    <div class="col-md-12  text-center">
                        <button type="button" class="btn profile-update-btn go-back-life-style mt-lg-5 mt-md-5 mt-2">Previous</button>
                  

                <button id="btnLoader" type="submit" class="button__loader btn reffer-btn reffer_btn mt-lg-5 mt-md-5 mt-2" onclick="btnFunction(); return false;">
                    <span class="button__text">Submit</span>
                </button>




                    </div>
                </div>
                <!--Lifestyle Preferences end-->
            </div>
            </section>
        </form> 
    </div> 
    <section class="update-profile back-to-profile" style="min-height: calc(100vh - 111px);align-items: center;">
    <div class="container">
        <div class="row">
            <div class="updat-hd text-center">That’s all for now. Thanks for filling all the details. We’ll customize offers
                according to the details you have provided. </div>
                <div class="col-md-12  text-center">
                    <a href="{{$_ENV['APP_URL']}}/myprofile/personal-information/myprofile-info" class="btn profile-submit-btn back-btn mt-lg-5 mt-md-5 mt-2">Back to profile</a>
                </div>
        </div>
    </div>

</section>

<section class="update-popup back-to-profile" style="min-height: calc(100vh - 111px);">
   
    </div>

</section>


<div class="modal fade modal-light show" id="OneTrustokey" style="padding-right: 8px;" aria-modal="true" role="dialog">
      <div class="modal-dialog modal-lg modal-dialog-centered align-items-center d-flex justify-content-center">
        <div class="modal-content bg-yellow">
  
    <div class="modal-body">
          <p style="
    text-align: center;
    color: #000;
    font-size: 18px;
    line-height: 2rem;
    overflow: hidden;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    margin: 16px 17px 25px 17px;">
            I Agree to enrol into Loyalty program &amp;  the Account related communication </p>
          <form id="OneTrustoky" class="row">
           @csrf
             

                <div class="btn-grp d-flex align-items-center  pl-3 mt-1" style="
    gap: 15px;
    justify-content: center;
">
                   <a href="javascript:void(0)" class="btn btn-member OneTrustoky" style="box-shadow: none"><span>OK</span></a>
             
                <a href="javascript:void(0)" class="btn btn-member okycancel" style="box-shadow: none;background: #FFf;color: #000;border: 0.5px solid #e1e1e1;">
                    <span>Cancel</span>
                </a>

                <a class="button__loader button__loader11">
                <span class="button__text">Loading...</span>
              </a>

                </div>
            </form>
          </div>
        
        </div>
      </div>
    </div>

<div class="modal-wrapper" >
    <div class="modal fade modal-light show" id="Thankyoumodal" style="padding-right: 8px;" aria-modal="true" role="dialog">
      <div class="modal-dialog modal-md modal-dialog-centered align-items-center d-flex justify-content-center">
        <div class="modal-content bg-yellow" style=" padding: 15px;text-align: center; background: url({{$_ENV['APP_URL']}}/assets/background_ibd.png);">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div> -->
          <!-- Modal body -->
      
          <div class="modal-body">

            @if(Session::get('logged_in_country',null)==='uae')
                <img src="{{$_ENV['APP_URL']}}/assets/logo-white.png" style="width:100px;">
            @else
                <img src="{{$_ENV['APP_URL']}}/assets/logo-white-usa.png" style="width:100px;">
            @endif
            <img src="{{$_ENV['APP_URL']}}/assets/thankyou-m.png" style="width:100%;margin-bottom: 10px;">
           <!--  <h4class="page-info-heading-1">
           
            </h4> -->
       
            <p style="
    color: #ffffff;
"> You are credited with <b style="
    font-size: 18px;
    color: #f59301;
">@if(Session::get('logged_in_country',null)==='usa') 50 USD @else 100 AED @endif</b> Bonus Points @if(Session::get('logged_in_country',null)==='uae') valid till 31st July 2023 @else  . To Redeem, please visit you nearest Tanishq showroom @endif  </p>

                 
<div class="btn-grp d-flex align-items-center p-0 mt-5" style="
    justify-content: center;
">
                <a href="javascript:void(0)" class="profile-update-btn ThankYouCancel" style="box-shadow: none;background: #FFf;color: #000;border: 0.5px solid #e1e1e1;">
                    <span>OK</span>
                </a>
                </div>
          </div>
        
        </div>
      </div>
    </div>
</div>
<!--  Thnk you pop up ends here-->

<script>

$(document).ready(function() {
       /* $('#multiple-selected').multiselect(); */
       $('.js-example-basic-multiple').select2();
       $('.select2-container').css("width","100%")
    });
</script>
@endsection