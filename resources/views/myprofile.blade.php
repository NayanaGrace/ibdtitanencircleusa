@extends('layouts.app')
@section('title', 'My Profile')
@section('content')

<div class="myaccount-section d-flex justify-content-center align-items-center" style="
    min-height: calc(100vh - 111px);">
  <div class="myaccount-wrapper col-11 col-md-11 col-lg-9" style="box-shadow:none;">
    <section class="titan-carousel account-carousel d-flex justify-content-center align-items-center">
      
   
    
    </section>

    <section class="d-flex justify-content-center align-items-center">
    <div class="wrapper account-btm-bx mb-md-5 md-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-md-3 col-xl-2 col-sm-12 p-0 account-list-web">
            <div class="d-flex flex-column">
              <ul class="nav nav-pills flex-column mb-sm-auto mb-0 account-bx-ul" id="menu">
                <li class="nav-item ">
                  <a href="#" class="nav-link align-middle px-0 active">
                    Overview
                  </a>
                </li>
              
                
                <li class="nav-item">
                  <a href="{{$_ENV['APP_URL']}}/myprofile/personal-information/edit" class="nav-link px-0 align-middle">
                    Update Profile</a>
                </li class="nav-item">
              

             

             
              </ul>
            </div>
          </div>

          <div class="col-12 p-0 account-list-mob">
            <div class="account-inline-list d-flex">
              <div class="col d-flex align-items-center justify-content-center down-arrow active"> Overview</div>
           
              <div class="col d-flex align-items-center justify-content-center down-arrow "><a href="{{$_ENV['APP_URL']}}/myprofile/personal-information/edit">Update Profile</a></div>
           
             

            </div>
          </div>

          <div class="col-md-12 col-xl-10 col-xs-12 col-sm-12 account-bx-rgt account-bx-h">
            <div class="row">
              <div class="d-flex justify-content-between align-items-center">
                <div class="account-bx-wlcm"><p style="margin: 0;">Hello {{ $Data['first_name']." ".$Data['last_name'] }}!</p></div>
               <!--  <div class="account-bx-mem d-flex justify-content-end"><p style="margin: 0;">Member Since: {{ $Data['last_name'] }}</p></div> -->
              </div>
              
              <div class="col-md-6 col-xs-12 account-bx-tp mt-3">
                
                <div class="account-bx-1 d-flex flex-column justify-content-between px-4 py-3">
                  <div class="account-coin-bal">Credited Points </div>
                  <div class="d-flex justify-content-between align-items-center w-100 mb-6">
                    <div class="account-coin float-start">@if(Session::get('logged_in_country',null)==='usa') 50 USD @else 100 AED @endif</div>


                   
                   <!--  <div class="account-coin-end d-flex align-items-center justify-content-between"><p>Redeem now</p>
                    <a href="/shop">
                    <img class="redeem-img img-fluid" src="/assets/image/account-nxt-arrow.png" style="width: 80%;">
                    </a>
                    </div> -->
                  </div>
                </div>
              </div>


              <div class="col-md-6 col-xs-12 account-bx-tp mt-3">
                
                <div class="account-bx-1 d-flex flex-column justify-content-between px-4 py-3">
                  <div class="account-coin-bal">Date of Birth </div>
                  <div class="d-flex justify-content-between align-items-center w-100 mb-6">
                  <?php
                  $time_zone = 'Asia/Kolkata';
                  ?>
                    <div class="account-coin float-start">{{ \Carbon\Carbon::parse($Data['dob'])->setTimezone($time_zone)->format('d-M-Y') }}</div>
                  
                  </div>
                </div>
              </div>


              <div class="col-md-6 col-xs-12 account-bx-tp mt-3">
                
                <div class="account-bx-1 d-flex flex-column justify-content-between px-4 py-3">
                  <div class="account-coin-bal">Contact Number </div>
                  <div class="d-flex justify-content-between align-items-center w-100 mb-6">
                  <?php
                  $time_zone = 'Asia/Kolkata';
                  ?>
                    <div class="account-coin float-start">{{$Data['mobile']}}</div>
                  
                  </div>
                </div>
              </div>



              <div class="col-md-6 col-xs-12 account-bx-tp mt-3">
                
                <div class="account-bx-1 d-flex flex-column justify-content-between px-4 py-3">
                  <div class="account-coin-bal">Email Address </div>
                  <div class="d-flex justify-content-between align-items-center w-100 mb-6">
                  <?php
                  $time_zone = 'Asia/Kolkata';
                  ?>
                    <div class="account-coin float-start">{{$Data['email']}}</div>
                  
                  </div>
                </div>
              </div>

       
          </div>
        </div>
      </div>
    </div>
  </section>
  
  </div>
</div>
@endsection