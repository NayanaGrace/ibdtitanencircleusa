@extends('layouts.app')
@section('title', 'Disclaimer')
@section('bodyclass', 'disclaimer')
@section('content')
  <section class="faqs bg-yellow-white first-section-pt">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <h2 class="heading-light-1">Disclaimer.</h2>
                <h5 class="games-details-card__content">TCL reserves the right to recover the cost of goods, damages caused to TCL and lawyers’ fees from persons using the site fraudulently.</h5></br>

                <div class="faqs-accordian">
                  <div id="accordion" class="accordion">
                    <div class="card">
                      <div class="card-header" id="headingOne" data-toggle="collapse"
                      data-target="#faqOne"
                      aria-expanded="true"
                      aria-controls="faqOne">
                          <a
                            href="javascript:void(0)"
                            class="accordion-link"
                            
                          >
                          Website Content
                        </a>
                        </h5>
                      </div>
                      <div
                        id="faqOne"
                        class="collapse"
                        aria-labelledby="headingOne"
                        data-parent="#accordion"
                      >
                        <div class="card-body">
                         Titan owns the copyright to all the contents of this website, including images. All trademarks and other intellectual property are owned or licensed by us (unless otherwise specified). You shall not copy, reproduce, distribute, republish, download, display, post or transmit any part of the website without written consent from us (except as stated below). You may print or download any page(s) for your own personal and non-commercial use only. If you have any doubts about what you can do, please go to our contact us page to apply for permission to reproduce the content. While our website is as accurate as possible, we cannot accept responsibility for any inaccuracies or errors beyond our reasonable control. We cannot guarantee that colours in our images will be rendered correctly on different computer monitors.
                        </div>
                      </div>
                      </div>
                    <div class="card">
                      <div class="card-header" id="headingTwo" data-toggle="collapse"
                      data-target="#faqTwo"
                      aria-controls="faqTwo">
                          <a
                            href="javacript:void(0)"
                            class="accordion-link"
                            
                          >
                          External Material
                        </a>
                        </h5>
                      </div>

                      <div
                        id="faqTwo"
                        class="collapse"
                        aria-labelledby="headingTwo"
                        data-parent="#accordion"
                      >
                        <div class="card-body">
                         You shall not use the website in any way that will damage it or interrupt its provision. You shall not use the website to transmit or post any computer viruses. We cannot guarantee that the website is free from computer viruses, and you should take your own precautions in this respect. We will try to make sure that the website is always available. However, this is not always possible, and we are not liable if the website is unavailable.

                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" id="headingTree" data-toggle="collapse"
                      data-target="#faqThree"
                      aria-controls="faqThree">
                          <a
                            href="javacript:void(0)"
                            class="accordion-link"
                            
                          >
                         Links
                        </a>
                        </h5>
                      </div>

                      <div
                        id="faqThree"
                        class="collapse"
                        aria-labelledby="headingTree"
                        data-parent="#accordion"
                      >
                        <div class="card-body">
                           The website may contain links to other websites; these external websites are not under our control. We cannot be held responsible for such websites and cannot make any guarantees about them. We provide these links because we think they might interest you, but we do not monitor or endorse these other websites.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" id="heading4" data-toggle="collapse"
                      data-target="#faq4"
                      aria-controls="faq4">
                          <a
                            href="javacript:void(0)"
                            class="accordion-link"
                            
                          >
                         Liability
                        </a>
                        </h5>
                      </div>

                      <div
                        id="faq4"
                        class="collapse"
                        aria-labelledby="heading4"
                        data-parent="#accordion"
                      >
                        <div class="card-body">
                           We take no responsibility for any loss or damage suffered as a result of our "order online" service, or goods supplied using it, except as required by law, even if we could have foreseen the loss, or the possibility of it was brought to our attention.

                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" id="heading5" data-toggle="collapse"
                      data-target="#faq5"
                      aria-controls="faq5">
                          <a
                            href="javacript:void(0)"
                            class="accordion-link"
                            
                          >
                         Changes to the Site Disclaimer

                        </a>
                        </h5>
                      </div>

                      <div
                        id="faq5"
                        class="collapse"
                        aria-labelledby="heading5"
                        data-parent="#accordion"
                      >
                        <div class="card-body">
                           We take no responsibility for any loss or damage suffered as a result of our "order online" service, or goods supplied using it, except as required by law, even if we could have foreseen the loss, or the possibility of it was brought to our attention.

                        </div>
                      </div>
                    </div>
                    
                  </div>
                </div>
                <h5 class="games-details-card__content">This site disclaimer is governed by the law of India and you and we agree to use the Bangalore jurisdiction if there is any dispute between us.
If any part of this Site Disclaimer is found to be invalid by law, the rest of them remain valid and enforceable.</h5></br>
              </div>
            </div>
          </div>
        </section>
@endsection