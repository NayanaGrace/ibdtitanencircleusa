@extends('PreLogin.layouts.app')
@section('title', '404')
@section('styles')
@endsection
@section('content')

<section class="d-flex justify-content-center align-items-center mt-5 mb-5">
  <div class="container">
    <div class="row store-form">
  <!-- Main content -->

        <div class="container d-flex justify-content-center align-items-center">
            <div class="row">
                
                <div class="mask-text">
                    <h1>Oops!</h1>
                </div>
                
                <div class="sub-mask-text">
                    <p>404 - PAGE NOT FOUND</p>
                </div>
                
                <div class="content-text">
                    <p>The page you are looking for might have been removed <br>had its name changed or is temporary available.</p>
                </div>

                <div class=" d-flex justify-content-center nav-home">
                    <a href="{{$_ENV['APP_URL']}}">Back to Home</a>   
                </div>

            </div>
        </div>
  
    </div>
            <!-- /.col -->
        </div>
    </section>
    <script>
        document.addEventListener("DOMContentLoaded", function(){
  window.addEventListener('scroll', function() {
      if (window.scrollY >= 1) {
        document.getElementById('main-menu-list').classList.add('fixed-top', 'mt-6', 'nav-box-shadow');
        document.getElementById('main-header').classList.add('fixed-top');
        // add padding top to show content behind navbar
        navbar_height = document.querySelector('.navbar').offsetHeight;
        // document.body.style.paddingTop = navbar_height + 'px';
      } else {
        document.getElementById('main-menu-list').classList.remove('fixed-top', 'mt-6', 'nav-box-shadow');
        document.getElementById('main-header').classList.remove('fixed-top');
         // remove padding top from body
        document.body.style.paddingTop = '0';
      } 
  });
});
    </script>
@endsection
