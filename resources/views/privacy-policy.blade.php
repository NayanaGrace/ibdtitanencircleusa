@extends('layouts.app')
@section('title', 'Privacy Policy')
@section('bodyclass', 'privacy-policy')
@section('content')

 @if($_ENV['ENABLED']=='enabled')

 <section class="faqs mt-xl-5 mt-lg-3 mt-md-3 mb-3 mt-5">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">

              <select id="kindlyselect_0Dropdown"></select>

<!-- Button element that will trigger the notice update -->
<button id="otnotice-bundle-button">Update</button>


<!-- Container in which the privacy notices will be rendered -->
<div id="otnotice-bundle-container" class="otnotice"></div>

<script src="https://privacyportal-uat-cdn.onetrust.com/privacy-notice-scripts/otnotice-bundle-1.0.min.js" type="text/javascript" charset="UTF-8" id="otprivacy-notice-script">
    settings="eyJjYWxsYmFja1VybCI6Imh0dHBzOi8vcHJpdmFjeXBvcnRhbHVhdC5vbmV0cnVzdC5jb20vcmVxdWVzdC92MS9wcml2YWN5Tm90aWNlcy9zdGF0cy92aWV3cyJ9";
    bundleUrl="https://privacyportal-uat-cdn.onetrust.com/07bbcec1-1e84-4a22-bde7-75062ee48776/notice-bundles/58f41a26-a3bb-4916-a386-61423fb50de8.json";
</script>

<script type="text/javascript" charset="UTF-8">
    OneTrust.NoticeManagerApi.Initialized.then(function() {
        OneTrust.NoticeManagerApi.InitializeOptions(
            "otnotice-bundle-container",
            ["kindlyselect_0Dropdown"],
            "otnotice-bundle-button"
        );
    });
</script>


              </div>
                         </div>
                         </div>
                         </section>

 @else
 <section class="faqs mt-xl-5 mt-lg-3 mt-md-3 mb-3 mt-5">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
 
<!-- OneTrust Privacy Notice Bundle start -->

<!-- Drop-down elements that will control which notice is displayed -->
<select id="country_undefinedDropdown"></select>

<!-- Button element that will trigger the notice update -->
<button id="otnotice-bundle-button">Update</button>


<!-- Container in which the privacy notices will be rendered -->
<div id="otnotice-bundle-container" class="otnotice"></div>

<script src="https://privacyportal-in-cdn.onetrust.com/privacy-notice-scripts/otnotice-bundle-1.0.min.js" type="text/javascript" charset="UTF-8" id="otprivacy-notice-script">
    settings="eyJjYWxsYmFja1VybCI6Imh0dHBzOi8vcHJpdmFjeXBvcnRhbC1pbi5vbmV0cnVzdC5jb20vcmVxdWVzdC92MS9wcml2YWN5Tm90aWNlcy9zdGF0cy92aWV3cyJ9";
    bundleUrl="https://privacyportal-in-cdn.onetrust.com/53ec83ca-0693-46f3-a55b-110c3f8f5a64/notice-bundles/draft/764a198a-997a-4a00-9b86-a03031ee71ef.json";
</script>

<script type="text/javascript" charset="UTF-8">
    OneTrust.NoticeManagerApi.Initialized.then(function() {
        OneTrust.NoticeManagerApi.InitializeOptions(
            "otnotice-bundle-container",
            ["country_undefinedDropdown"],
            "otnotice-bundle-button"
        );
    });
</script>

<!-- OneTrust Privacy Notice end -->

                         </div>
                         </div>
                         </div>
                         </section>
              

  @endif
@endsection