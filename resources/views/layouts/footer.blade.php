<section class="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-sm-6 col-xs-12 d-flex justify-content-center">
          <ul class="list-inline footer-btm-ul">
            <li class="list-inline-item footer-btm-li"><a href="{{$_ENV['APP_URL']}}/terms-condition" target="_blank">Terms of Use</a></li>
            <li class="list-inline-item footer-btm-li"><a href="{{$_ENV['APP_URL']}}/privacy-policy" target="_blank">Privacy Center</a></li>
            <li class="list-inline-item footer-btm-li"><a href="{{$_ENV['APP_URL']}}/cookie-notice" target="_blank">Cookies</a></li>
            <li class="list-inline-item "><a  style="color: #fff;" href="{{$_ENV['APP_URL']}}/disclaimer" target="_blank">Disclaimer</a></li>
          </ul>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 d-flex justify-content-center">
          <p class="copyright">Copyright © 2023 Titan Company Limited. All Rights Reserved.</p>
        </div>
      </div>
  </div>
</section>