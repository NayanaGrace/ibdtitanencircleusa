<!--Font Awesome CDN-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
<style>
.toast-info {
  background-color: #2f96b4;
}
.pre_click {
  cursor: pointer;
}

  </style>
<nav id="main-header" class="navbar navbar-expand-sm titan-navbar py-1" style="z-index: 3;">
<input type="hidden" value="{{iplocation()}}"/>
  <div class="container desk-nav">
   <!--  <div class="navbar-brand col-md-2 store-locator" href="#">
     <a href="{{$_ENV['APP_URL']}}#store_locater_home"><img alt="store locator"src="/assets/image/location-icon.png" class="img-fluid header-ico"><span class="nav-loctn">Find a store near you</span></a>
    </div> -->
    <div class="d-flex justify-content-center col-md-7 brand-logo" style="max-width: fit-content; width: fit-content;">
      <a href="{{$_ENV['APP_URL']}}" class="d-flex justify-content-center align-items-center">
        <!-- <img alt="titan encircle" src="/assets/image/Titan Logo-highres.png" class="img-fluid"> -->
        <img alt="titan encircle" src="{{$_ENV['APP_URL']}}/assets/Titan_Encircle_new-logo.png" class="img-fluid">

      
      
      </a>



     
      <!-- <button class="navbar-toggler toggle-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button> -->
    </div>
    <div class="justify-content-end" >
      <ul class="justify-content-end align-items-center navbar-nav">
       <!--  <li class="nav-item srch-li me-3">
          <a class="nav-link srch-ico" href="#">
            <img src="/assets/image/search-icon.png" class="img-fluid nav-icon">
          </a>
        </li> -->
        @if(Session::get('SESS_USER_INFO')??null)
        <li class="nav-item me-3">  
            <a class="nav-link profile-ico dropdown-toggle titan-nav-dropdown" id="navbarDropdownProfileLink" href="#" role="button" data-bs-toggle="dropdown">
                <img alt="my profile" src="{{$_ENV['APP_URL']}}/assets/image/profile-icon.png" class="img-fluid nav-icon">
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdownProfileLink" data-bs-toggle="dropdown" style="left: auto; top: auto;">
                  <li><a class="dropdown-item profile-click" href="{{$_ENV['APP_URL']}}/myprofile/personal-information/myprofile-info">My Profile</a></li>
                  <li><a class="dropdown-item logout-click" href="{{$_ENV['APP_URL']}}/logout">Logout</a></li>
                </ul>
                  @else 
                  <a class="nav-link profile-ico" href="{{$_ENV['APP_URL']}}/myprofile/personal-information/myprofile-info">
                <img alt="my profile" src="{{$_ENV['APP_URL']}}/assets/image/profile-icon.png" class="img-fluid nav-icon">
                </a>
        </li>
              @endif
        <!-- <li class="nav-item me-3">
          <a class="nav-link" href="#">
            <img src="/assets/image/basket-icon.png" class="img-fluid nav-icon">
          </a>
        </li> -->
        <li class="nav-item dropdown ">
        <!--   <a class="nav-link dropdown-toggle titan-nav-dropdown" href="#" role="button" data-bs-toggle="dropdown">
            <img src="assets/image/india-icon.png" class="img-fluid in-flag">India (EN)</a> -->
            <a class="nav-link @if(Session::get('SESS_USER_INFO')??null) @else dropdown-toggle @endif titan-nav-dropdown text-uppercas" href="#" role="button" data-bs-toggle="dropdown">
            @if(Session::get('logged_in_country',null)==='usa')
            <img alt="titan encircle usa" src="{{$_ENV['APP_URL']}}/assets/image/united-states-of-america.png" class="img-fluid in-flag"> {{iplocation()}}</a>
            @elseif(Session::get('logged_in_country',null)==='uae')
            <img alt="titan encircle uae" src="{{$_ENV['APP_URL']}}/assets/image/united-arab-emirates.png" class="img-fluid in-flag"> {{iplocation()}}</a>
            @else
            <img alt="titan encircle india" src="{{$_ENV['APP_URL']}}/assets/image/india-icon.png" class="img-fluid in-flag"> {{iplocation()}}</a>
            @endif
            @if(Session::get('SESS_USER_INFO')??null)

          @else
           <ul class="dropdown-menu">
            <li><a data-id="uae" class="dropdown-item country_change" href="#">UAE</a></li>
          </ul>
          @endif
        </li>
      </ul>
    </div>
  </div>
<!-- mobile nav start -->
  <div class="container mob-nav">
    <div class="d-flex justify-content-between align-items-center mob-sm-nav">

        
      <div class="nav-list-mob">

        <div class="topmobnav px-4">
          <div class="nav-close">
            <i class="fas fa-times"></i>
          </div>

          <div class="justify-content-end col-md-3 mob-left-nav">
              <ul class="navbar-nav flex-row align-items-center" style="gap: 20px;">
         

                <li class="nav-item dropdown" style="line-height: 30px;">
                  @if(Session::get('SESS_USER_INFO')??null)  
                      

                          <a class="dropbtn titan-nav-dropdown" href="#" role="button">
                          <img alt="my profile" src="{{$_ENV['APP_URL']}}/assets/image/profile-icon.png" class="img-fluid nav-icon">
                          <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-content px-3" style="line-height: 50px;">
                            <li><a class="dropdown-item" href="{{$_ENV['APP_URL']}}/myprofile/personal-information/myprofile-info">My Profile</a></li>
                            <li><a class="dropdown-item" href="{{$_ENV['APP_URL']}}/logout">Logout</a></li>
                          </ul>
                            @else 
                            <a class="nav-link" href="{{$_ENV['APP_URL']}}/myprofile/personal-information/myprofile-info">
                          <img alt="my profile" src="{{$_ENV['APP_URL']}}/assets/image/profile-icon.png" class="img-fluid nav-icon">
                          </a>

                      @endif
                  </li>
                
             

                <li class="nav-item dropdown ">

               

                    <a class="dropbtn titan-nav-dropdown text-uppercas" href="#" role="button" data-bs-toggle="dropdown">
                    <img al="titan encircle india" src="{{$_ENV['APP_URL']}}/assets/image/india-icon.png" class="img-fluid in-flag"> {{iplocation()}}</a>
                  <!--   <i class="fa fa-caret-down"></i> -->
                    @if(Session::get('SESS_USER_INFO')??null)

                    @else
                    <ul class="dropdown-content px-3">
                      <li><a data-id="uae" class="dropdown-item country_change" href="#">UAE</a></li>
                     
                    </ul>
                    @endif
                </li>
              </ul>
            </div>
        </div>

      
        @if(Session::get('SESS_USER_INFO')??null)
        @if(Session::get('log_country',null)==='usa')
        <!-- usa mobile menu start -->
      
        <!-- usa mobile menu end -->
        @elseif(Session::get('log_country',null)==='uae')
       

   
        @else
       
        <!-- india mobile menu end -->
        @endif
        @else

     
       
        @endif

      </div>

   
    
      <div class="mob-nav-btn">
        <span></span>
        <span></span>
      </div>

      <div class="brand-logo d-flex justify-content-center align-items-center">
        <a href="{{$_ENV['APP_URL']}}" class="d-flex justify-content-center align-items-center">
         <!--  <img alt="titan encircle" src="/assets/image/Titan Logo-highres.png" class="img-fluid"> -->
          <img alt="titan encircle" src="{{$_ENV['APP_URL']}}/assets/Titan_Encircle_new-logo.png" class="img-fluid">

        
        </a>
      </div>

      <div class="d-flex justify-content-end col-md-3 tab-left-nav" >
        <ul class="navbar-nav flex-row">
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">
              <img src="/assets/image/search-icon.png" class="img-fluid nav-icon">
            </a>
          </li> -->
          <li class="nav-item">
          @if(Session::get('SESS_USER_INFO')??null)
          <li class="nav-item" style="position: relative;">  
              <a class="nav-link dropdown-toggle titan-nav-dropdown" href="#" role="button" data-bs-toggle="dropdown">
                  <img src="{{$_ENV['APP_URL']}}/assets/image/profile-icon.png" class="img-fluid nav-icon">
                </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="{{$_ENV['APP_URL']}}/myprofile/personal-information/myprofile-info">My Profile</a></li>
                    <li><a class="dropdown-item" href="{{$_ENV['APP_URL']}}/logout">Logout</a></li>
                  </ul>
                    @else 
                    <a class="nav-link" href="/myprofile/personal-information/myprofile-info">
                  <img alt="my profile" src="{{$_ENV['APP_URL']}}/assets/image/profile-icon.png" class="img-fluid nav-icon">
                  </a>
          </li>
                @endif
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">
              <img src="/assets/image/basket-icon.png" class="img-fluid nav-icon">
            </a>
          </li> -->
          <li class="nav-item dropdown ">
          <!--   <a class="nav-link dropdown-toggle titan-nav-dropdown" href="#" role="button" data-bs-toggle="dropdown">
              <img src="assets/image/india-icon.png" class="img-fluid in-flag">India (EN)</a> -->
              <a class="nav-link @if(Session::get('SESS_USER_INFO')??null) @else dropdown-toggle @endif titan-nav-dropdown text-uppercas" href="#" role="button" data-bs-toggle="dropdown">
              @if(Session::get('logged_in_country',null)==='usa')
              <img alt="titan encircle usa" src="/assets/image/united-states-of-america.png" class="img-fluid in-flag"> {{iplocation()}}</a>
              @elseif(Session::get('logged_in_country',null)==='uae')
              <img alt="titan encircle uae" src="/assets/image/united-arab-emirates.png" class="img-fluid in-flag"> {{iplocation()}}</a>
              @else
              <img alt="titan encircle india" src="/assets/image/india-icon.png" class="img-fluid in-flag"> {{iplocation()}}</a>
              @endif
              @if(Session::get('SESS_USER_INFO')??null) @else 
              
               <ul class="dropdown-menu">
                     <li><a data-id="uae" class="dropdown-item country_change" href="#">UAE</a></li>
               </ul>
                @endif

          </li>
        </ul>
      </div>
  </div>
<!-- mob nav end -->
    </div>
  </div>
</nav>



<script>
  document.addEventListener("DOMContentLoaded", function(){
  window.addEventListener('scroll', function() {
      if (window.scrollY >= 200) {
        document.getElementById('main-menu-list').classList.add('fixed-top', 'mt-66', 'nav-box-shadow');
        document.getElementById('main-header').classList.add('fixed-top');
        // add padding top to show content behind navbar
        navbar_height = document.querySelector('.navbar').offsetHeight;
        // document.body.style.paddingTop = navbar_height + 'px';
      } else {
        document.getElementById('main-menu-list').classList.remove('fixed-top', 'mt-66', 'nav-box-shadow');
        document.getElementById('main-header').classList.remove('fixed-top');
         // remove padding top from body
        document.body.style.paddingTop = '0';
      } 
  });
});

$(".mob-nav-btn").click(function () {
    $(".nav-list-mob").addClass("open");
    $("body").css({ overflow: "hidden" });
  });

  $(".nav-close").click(function () {
    $(".nav-list-mob").removeClass("open");
    $("body").css('overflow', 'visible');
  });
</script>

<script>
  $(document).ready(function(){
    $('.menu-collapse-check-mob').click(function(){
      $('.menu-collapse-check-mob').not(this).prop('checked', false);
    });

    
    $('.pre_click').click(function(){
     var url = $(this).attr("data-id");
     setTimeout(()=>{window.location = url },1000);
      });

  });
</script>