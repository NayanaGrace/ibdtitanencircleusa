<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Titan Encircle | @yield('title')</title> 
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<link id="icon57" rel="apple-touch-icon" href="./media/logos/icon57.png">
<link id="icon72" rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/icon72.png')}}">
<link id="icon76" rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/icon76.png')}}">
<link id="icon114" rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/icon114.png')}}">
<link id="icon120" rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/icon120.png')}}">
<link id="icon152" rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/icon152.png')}}">
<link id="icon167" rel="apple-touch-icon" sizes="167x167" href="{{ asset('img/icon167.png')}}">
<link id="icon180" rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/icon180.png')}}">
<link id="icon144" rel="icon" type="image/png" sizes="144x144" href="{{ asset('img/icon144.png')}}"/>
<link id="icon192" rel="icon" type="image/png" sizes="192x192" href="{{ asset('img/icon192.png')}}"/>
<?php
header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
header("Pragma: no-cache"); //HTTP 1.0
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{$_ENV['APP_URL']}}/assets/css/style.css?v=1.2">
  <link rel="stylesheet" href="{{$_ENV['APP_URL']}}/assets/css/toastr.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
@yield('styles')
</head>
<body class="@yield('bodyclass')" style="display:block;">
<input name="APP_URL" value="{{$_ENV['APP_URL']}}" type="hidden"/>
@include('layouts.header')
@yield('content')
@include('layouts.footer')
@yield('scripts')
<script src="{{$_ENV['APP_URL']}}/assets/js/toastr.min.js"></script>
<script src="{{$_ENV['APP_URL']}}/assets/js/findstores.js"></script>
<script src="{{$_ENV['APP_URL']}}/assets/js/mainscripts.js"></script>
<script>
   // Scroll screen to target element
  var href = location.href;
  var href = href.match(/([^\/]*)\/*$/)[1];
  setTimeout(function() {
  if(href=='#store_locater_home'){
    $("#store_locater_home").addClass("store_class");
    $('body').scrollTo('#store_locater_home');
  }
    }, 2000);
</script>
</body>
