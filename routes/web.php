<?php

use Illuminate\Support\Facades\Route;

/* Route::get('/home', 'App\Http\Controllers\HomeController@index'); */

Route::post('change_country','App\Http\Controllers\HomeController@change_country');

Route::get('/', [ 'as' => 'login', 'uses' => 'App\Http\Controllers\LoginController@index']);
Route::get('/usa', [ 'as' => 'login', 'uses' => 'App\Http\Controllers\LoginController@usa']);
Route::get('/titanwatchmeet', [ 'as' => 'login', 'uses' => 'App\Http\Controllers\LoginController@watches']);


Route::post('/sendloginotp', 'App\Http\Controllers\LoginController@sendotp');
Route::post('/validateotp', 'App\Http\Controllers\LoginController@validateotp');
Route::post('/onetrustsignup', 'App\Http\Controllers\LoginController@signup');


Route::post('/sendloginotpuae', 'App\Http\Controllers\LoginController@sendotpuae');
Route::post('/validateotpuae', 'App\Http\Controllers\LoginController@validateotpuae');

Route::post('/sendloginotpusa', 'App\Http\Controllers\LoginControllerUSA@sendotpusa');
Route::post('/validateotpusa', 'App\Http\Controllers\LoginControllerUSA@validateotpusa');


//Post Login Routes
Route::group(['middleware' => ['post_login', 'web']] , function() {
    Route::get('/myprofile/personal-information/myprofile-info', 'App\Http\Controllers\MyProfileController@index')->name('myprofile-info');
    Route::post('/memberEnroll', 'App\Http\Controllers\EnrollController@memberEnroll');

    Route::get('/myprofile/personal-information/edit', 'App\Http\Controllers\MyProfileController@personalinfoedit')->name('myprofile-info-edits');
    Route::get('/myprofile/personal-information/edit-uae', 'App\Http\Controllers\MyProfileController@personalinfoedituae')->name('myprofile-info-edits-uae');
    Route::get('/myprofile/personal-information/edit-usa', 'App\Http\Controllers\MyProfileControllerUSA@personalinfoeditusa')->name('myprofile-info-edits-usa');

    Route::post('/myprofile/personalinfoupdate','App\Http\Controllers\MyProfileController@personalinfo_update');
    Route::post('/myprofile/personalinfoupdate-uae','App\Http\Controllers\ProfileUpdate@personalinfo_update');
    Route::post('/myprofile/personalinfoupdate-usa','App\Http\Controllers\ProfileUpdate@personalinfo_update');

    Route::get('/enroll', 'App\Http\Controllers\EnrollController@index');



    Route::get('/logout', 'App\Http\Controllers\MyProfileController@logout');

});

// One Trus Api Calls
Route::post('/consentreceipts', 'App\Http\Controllers\OneTrustApis@consentreceipts');
Route::post('/findstates', 'App\Http\Controllers\MyProfileController@findstates');
Route::post('/findsmartcity', 'App\Http\Controllers\MyProfileController@findsmartcity');

Route::get('/privacy-policy', 'App\Http\Controllers\MyProfileController@Privacy');
Route::get('/terms-condition', 'App\Http\Controllers\MyProfileController@Terms');
Route::get('/disclaimer', 'App\Http\Controllers\MyProfileController@Disclaimer');
Route::get('/cookie-notice', 'App\Http\Controllers\MyProfileController@Cookies');


/* Auth::routes(); */

/* Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home'); */
