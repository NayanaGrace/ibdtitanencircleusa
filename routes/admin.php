<?php

use Illuminate\Support\Facades\Route;

route::get('/', function () {
    return Redirect::to('admin/login');
});

Route::get('login', 'App\Http\Controllers\Admin\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'App\Http\Controllers\Admin\Auth\LoginController@login');
Route::get('logout-now', 'App\Http\Controllers\Admin\HomeController@logout');

Route::group(['middleware' => ['web']] , function() {
    Route::get('/set-country-to-session', [App\Http\Controllers\Admin\HomeController::class, 'setCountryToSession'])->name('set-country-to-session');
    Route::get('/home', [App\Http\Controllers\Admin\HomeController::class, 'index']);
    Route::get('/profile-logs', [App\Http\Controllers\Admin\ProfileLogs::class, 'index'])->name('profile-logs');
    Route::get('/export-logs', [App\Http\Controllers\Admin\ProfileLogs::class, 'export'])->name('export-logs');
    
   //  WatchesMeet
   Route::get('/watches-profile-logs', [App\Http\Controllers\Admin\WatchesMeet::class, 'index'])->name('watches-profile-logs');
   Route::get('/export-watches', [App\Http\Controllers\Admin\WatchesMeet::class, 'export'])->name('export-watches');



    Route::post('profile-logs', [App\Http\Controllers\Admin\ProfileLogs::class, 'index'])->name('profile-list');
    Route::get('logout', [App\Http\Controllers\Admin\ProfileLogs::class, 'index'])->name('profile-list');

   

});


