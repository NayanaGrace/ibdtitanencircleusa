<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PostLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if(!Session::has('SESS_USER_INFO')) {
            $backlink = url()->full();

            $link_array = explode('/',$backlink);
            $page = end($link_array);
            
            if($page=="logout"){
                $backlink =$_ENV['APP_URL'].'/MyProfile';
            }
            session(['SESSION_BACKLINK' => $backlink]);
            return redirect('/');
        }
        return $next($request);
    }
}
