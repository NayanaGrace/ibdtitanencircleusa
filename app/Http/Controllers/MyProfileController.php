<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Models\ProfileModel as ProModel; 
use Carbon\Carbon; 
use Cookie;
use App\Models\ApiMessage;
use App\Models\ApiMethods;
use Session;
use Artisan;
use phpseclib3\Crypt\RSA;
use DB;
use phpseclib3\Crypt\PublicKeyLoader;
use Illuminate\Support\Facades\Redirect;
use App\Models\ApiParams;
use Validator;

class MyProfileController extends Controller
{
	public function index(Request $request)
	{

       
        $Data=session('SESS_USER_INFO');

       
     
        if(empty($Data['first_name'])) {


          return redirect()->route('myprofile-info-edits');
        }


        if (array_key_exists('dob', $Data)) {
          return view('myprofile',compact(['Data']));
      }

   
      return view('myprofileuae',compact(['Data']));
   

   
    }

    public function personalinfoedit(Request $request) {



       $concent= $concent = array(

        "Marketing_status" =>"",
 
        "Personalized_status" => "",
 
        "Survey_status" => "",
 
        "Birthday_status" =>  "",
 
       );

          $Data=session('SESS_USER_INFO');
        
          if(empty($Data['first_name'])) {

           session(['login_type' => 'enroll']);

          } else {

            session(['login_type' => 'login']);

          }



       $logged_in_country=session('logged_in_country');


       $country = DB::connection('appdb')
      ->table('tbl_country as a')
      ->where('a.country', $logged_in_country)
      ->get(); 

      if(empty($Data['country'])) {
          $state=[];
          $city =[];
        } else {

          $country_data = DB::connection('appdb')
          ->table('tbl_country')
          ->where('title',$Data['country'])
          ->where('country', $logged_in_country)
          ->first(); 
          
          $country_id = $country_data->id;

          $state = DB::connection('appdb')
          ->table('tbl_state as a')
          ->where('a.country_id', $country_id)
          ->where('a.country', $logged_in_country)
          ->get();
      }

      if(empty($Data['state'])) {
      
        $city =[];
      
      } else {

        $state_title =$Data['state'];

        $states  = DB::connection('appdb')
        ->table('tbl_state')
        ->where('title',$state_title)
        ->where('country', $logged_in_country)
        ->first();
    
        $state_id = $states->id;
    
        $city  = DB::connection('appdb')
        ->table('tbl_city')
        ->where('state_id',$state_id)
        ->where('country', $logged_in_country)
        ->get();
      }

      if(!empty($Data['mobile'])){

          $user_data  = DB::connection('appdb')
          ->table('profiletbl')
          ->where('mobile',$Data['mobile'])
          ->where('country', $logged_in_country)
          ->first();

          if($user_data != ""){
            $address = $user_data->address;
            $zipcode = $user_data->zipcode;
          }
          else{
            $address = "";
            $zipcode = "";
          }
      }
      $Data['address'] = $address;
      $Data['zipcode'] = $zipcode;

      return view('profileupdate',compact(['Data','concent','country','state','city','logged_in_country']));
      
    }

    public function personalinfoedituae(Request $request) {



      $concent= $concent = array(

        "Marketing_status" =>"",
 
        "Personalized_status" => "",
 
        "Survey_status" => "",
 
        "Birthday_status" =>  "",
 
       );
     



      $Data=session('SESS_USER_INFO');



      

         session(['login_type' => 'login']);

      


      $Data['country']="India";
      $Data['state']="KERALA";
      $Data['city_others']="";
      $Data['street_name']="";
      $Data['house_no']=$Data['houseno'];


      $logged_in_country=session('logged_in_country');


      $country = DB::connection('appdb')
     ->table('tbl_country as a')
     ->where('a.country', $logged_in_country)
     ->get(); 


   if(empty($Data['country'])) {
       $state=[];
       $city =[];
     } else {


       $state = DB::connection('appdb')
       ->table('tbl_state as a')
       ->where('a.country', $logged_in_country)
       ->get();
   }

   if(empty($Data['state'])) {
   
     $city =[];
   
   } else {

     $state_title =$Data['state'];

     $states  = DB::connection('appdb')
     ->table('tbl_state')
     ->where('title',$state_title)
     ->where('country', $logged_in_country)
     ->first();
 
     $state_id = $states->id;
 
     $city  = DB::connection('appdb')
     ->table('tbl_city')
     ->where('state_id',$state_id)
     ->where('country', $logged_in_country)
     ->get();
   }


    

     
      if(Session::has('SESS_MOBILE')){
        return view('PostLogin.enroll',compact(['Data','concent','country','state','city']));
      }
      else {
      return view('PostLogin.profileupdate',compact(['Data','concent','country','state','city']));
        }


    }
     
     

public function personalinfo_update(Request $req) {

    $Data=session('SESS_USER_INFO');

    $logged_in_country=session('logged_in_country');

    if($logged_in_country=='uae'){

      $StoreCode="Encircle";
      $CampaignCode="SARAQ1";

    $api_url = $_ENV['UAE_CREDIT_API'];
     } else {

      $StoreCode="Encircle";
      $CampaignCode="CAT01";

      

    $api_url = $_ENV['USA_CREDIT_API'];
    }
  

    if (!empty($_POST['dob'])) {
      $dob =$_POST['dob'];
      $dob = date("Y-M-d", strtotime($dob));
    } else {

      $dob='';
    }


    if (!empty($_POST['doa'])) {
      $doa =$_POST['doa'];
      $doa = date("Y-M-d", strtotime($doa));
    } else {

      $doa='';
    }


    if (!empty($_POST['address'])) {
      $address =$_POST['address'];
      
    } else {
      $address='';
    }


    if (!empty($_POST['zipcode'])) {
      $zipcode =$_POST['zipcode'];
    } else {

      $zipcode='';
    }



    $state=$_POST['state'];
    $city = $_POST['city'];
    /* $city =""; */


    if (empty($_POST['first_name'])) {



      return response()->json([
        'status' => 'error','msg'=>"Please Provide First Name"
        ]);


    }


    if (empty($_POST['title'])) {



      return response()->json([
        'status' => 'error','msg'=>"Please Provide title"
        ]);


    }





    $marital_status= $_POST['marital_status'];
    if($marital_status=='Yes'){

        $marital_status="Married";

        $marital_status_info=2;

       } 
       
       else 
       
       {

        $marital_status="Single";


        $marital_status_info=1;

      }

    // For API

    $post_data = array(
      "Date"=> date('Y-M-d'),
      "ChannelCode"=>"tanishq",
      "StoreCode"=>$StoreCode,
      "MobileNumber"=>$_POST['mobile'],
      "Salutation"=> $_POST['title'],
      "FirstName"=>$_POST['first_name'],
      "LastName"=>$_POST['last_name'],
      "EmailId"=>$_POST['email'],
      "DOB"=>$dob,
      "MaritalStatus"=>$marital_status,
      "DOA"=>$doa,
      "HouseNo"=>$_POST['house_no'],
      "BuildingName"=>$_POST['building_name'],
      "StreetName"=>$_POST['street_name'],
      "Colony"=>$_POST['colony'],
      "City"=>$city,    
      "CityOther"=>$_POST['city_others'],
      "State"=>$state,
      "Country"=>$_POST['country'],
      "CampaignCode"=>$CampaignCode
      );

      // for session
      $member_info =array(
        "title"=>$_POST['title'],
        "first_name"=>$_POST['first_name'],
        "last_name"=>$_POST['last_name'],
        "email"=>$_POST['email'],
        "mobile"=>$_POST['mobile'],
        "dob"=>$_POST['dob'],
        "marital_status"=>$marital_status_info,
        "doa"=>$_POST['doa'],
        "house_no"=>$_POST['house_no'],
        "building_name"=>$_POST['building_name'],
        "street_name"=>$_POST['street_name'],
        "colony"=>$_POST['colony'],
        "city"=>$_POST['city'],
        "city_others"=>$_POST['city_others'],
        "state"=>$_POST['state'],
        "country"=>$_POST['country'],
        "token"=>$Data['token'],
        "address"=>$address,
        "zipcode"=>$zipcode
        );
    
        
   /*    $api_url = $_ENV['UAE_CREDIT_API']; */
   
      $log_url = $api_url."/Enrollment";

      try {


       $response = Http::withToken($Data['token'])->post($log_url, $post_data);

      
      } catch (\Exception $e) {
       $response="";
      
       return false;
      }


      $json = $response->getBody();
      $data = json_decode($json, true);

     
    
  
      if (array_key_exists("MemberEnrollment",$data))
       {
          $errcode = $data['MemberEnrollment']['ErrorCode'];


          $card_number =$data['MemberEnrollment']['CardNo'];

           
       }
   
      
      if(isset($errcode)) {
          if($errcode==0) {
  
            // Set Session & Save Datea inside session
           
            Session::forget('SESS_USER_INFO'); 
     
            session(['SESS_USER_INFO' => $member_info]);

            $loginlog = loginlogs($card_number);


              return response()->json([
                  'status' => 'success','msg'=>''
                  ]);
      
          } else {
            
      
                  $error = ApiMessage::$messages[$errcode];

                

                   return response()->json([
                   'status' => 'error','msg'=>$error,'code'=>$errcode
                   ]);
          }
      }
}



  public function logout()
  {

  $logged_in_country = Session::get('logged_in_country',null);
  $logged_in_url=session('logged_in_url');
  Session::forget('SESS_USER_INFO'); 
  Session::forget('log_country'); 
  Session::forget('logged_in_country'); 
  Session::forget('logged_in_country_user'); 
  Session::forget('SESSION_BACKLINK'); 
  Session::flush(); 
  return Redirect::to($logged_in_url);
     
    }
    


  public function findstates(Request $request){

      $logged_in_country=session('logged_in_country');
      $country_title =$request->country;
    
      $countries  = DB::connection('appdb')
      ->table('tbl_country')
      ->where('title',$country_title)
      ->where('country',$logged_in_country)
      ->first();



      $country = $countries->id;

      $state  = DB::connection('appdb')
      ->table('tbl_state')
      ->where('country_id',$country)
      ->where('country',$logged_in_country)
      ->get()
      ->toArray();

      return response()->json($state);

  }


  public function findsmartcity(Request $request){


    $logged_in_country=session('logged_in_country');
    $state_title =$request->state;

  
    $states  = DB::connection('appdb')
    ->table('tbl_state')
    ->where('title',$state_title)
    ->where('country',$logged_in_country)
    ->first();

    $state = $states->id;

   

    $state  = DB::connection('appdb')
    ->table('tbl_city')
    ->where('state_id',$state)
    ->where('country',$logged_in_country)
    ->get()
    ->toArray();
    

    return response()->json($state);

  }
public function Privacy() {

  return view('privacy-policy');

}

public function Terms() {

  return view('terms-condition');

}

public function Disclaimer() {

  return view('disclaimer');

}

public function Cookies() {

  return view('cookies-notice');


}
}