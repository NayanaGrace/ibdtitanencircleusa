<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Models\ProfileModel as ProModel; 
use Carbon\Carbon; 
use Cookie;
use App\Models\ApiMessage;
use App\Models\ApiMethods;
use App\Models\ApiParamsUae;
use Session;
use Artisan;
use phpseclib3\Crypt\RSA;
use DB;
use phpseclib3\Crypt\PublicKeyLoader;
use Illuminate\Support\Facades\Redirect;
use App\Models\ApiParams;
use Validator;

class EnrollController extends Controller
{
    public function index() {


        $concent= $concent = array(

            "Marketing_status" =>"",
     
            "Personalized_status" => "",
     
            "Survey_status" => "",
     
            "Birthday_status" =>  "",
     
           );
    
           
    
            session(['login_type' => 'enroll']);
    
            
    
           $logged_in_country=session('logged_in_country');
    
    
           $country = DB::connection('appdb')
          ->table('tbl_country as a')
          ->where('a.country', $logged_in_country)
          ->get(); 
    
    
        if(empty($Data['country'])) {
            $state=[];
            $city =[];
          } else {
    
    
            $state = DB::connection('appdb')
            ->table('tbl_state as a')
            ->where('a.country', $logged_in_country)
            ->get();
        }
    
        if(empty($Data['state'])) {
        
          $city =[];
        
        } else {
    
          $state_title =$Data['state'];
    
          $states  = DB::connection('appdb')
          ->table('tbl_state')
          ->where('title',$state_title)
          ->where('country', $logged_in_country)
          ->first();
      
          $state_id = $states->id;
      
          $city  = DB::connection('appdb')
          ->table('tbl_city')
          ->where('state_id',$state_id)
          ->where('country', $logged_in_country)
          ->get();
        }


        $value =session('SESS_USER_INFO');
        $Data=session('SESS_USER_INFO');
        $logged_in_country  = session('logged_in_country',null);
        $Notification=[];
        return view('PostLogin.enroll',compact(['Data','Notification','concent','country','state','city']));
    }
    

    public function memberEnroll(Request $req) {
        $API_URL= API_URL();
        $log_country=session('logged_in_country',null);
        
        $UP_URL = $API_URL.'/UpdateProfile';
        $API_URL = $API_URL.'/MemberEnrollment'; 
      //  $channelcode = API_CHANNEL_CODE();
       $channelcode = "encirclewebsite";
        $_POST = $req->all();
        // Change Date format
 
        if(isset($_POST['spouse_dob']) && strlen($_POST['spouse_dob']) >0){
         $_POST['spouse_dob'] = date("d/m/Y", strtotime($_POST['spouse_dob']));
 
         }
         if(isset($_POST['anniversary_date']) && strlen($_POST['anniversary_date']) >0){
             $_POST['doa'] = date("d/m/Y", strtotime($_POST['anniversary_date']));
     
         }
         if(isset($_POST['dob']) && strlen($_POST['dob']) >0){
                 $_POST['dob'] = date("d/m/Y", strtotime($_POST['dob']));
         
         }
      
 
        $apiPostData = $req->all();
        $mobile = session('SESS_MOBILE');
        /* $mobile="8086467632"; */
        $fields = array(
            'mobile1' =>$mobile,
        );
 
        $_POST = array_merge($_POST,$fields);
         $requiredFields=array(
             "title"=>"Title",
             "first_name"=>"FirstName",  
             // "last_name"=>"Lastname",
             "mobile1"=>"MobileNo",
            /*  "email"=>"EmailId",
             "marital_status"=>"Marital Status",
             "dob"=>"DOBDay",
             "marital_status"=>"MaritalStatus",
             "gender"=>"Gender",
             "who_wear_spectacles"=>"WhoWearSpectacles" */
             );
             
             if( $log_country=='usa' ||  $log_country=='uae') {
           $mappingColumns = ApiParamsUae::apiRequestMemberEnrollmentParams();
            } else {
             $mappingColumns = ApiParams::apiRequestMemberEnrollmentParams();
            }
          foreach($requiredFields as $key=>$val){
          if(!isset($_POST[$key]) || strlen($_POST[$key])<1){ 
            $val="Please enter $val";
            return ApiParams::printResponse($val,"error");
          }
     }
    
     if(isset($_POST['dob']) && strlen($_POST['dob']) >0){
     $dobAry=explode("/",$_POST['dob']);
      $apiPostData["dob_day"]=$dobAry[0] ;
     $apiPostData["dob_month"]=strtoupper(date("M",strtotime(implode("-",$dobAry))));
     $dob_month = strtoupper(date("M",strtotime(implode("-",$dobAry))));
     $apiPostData["dob_year"]=$dobAry[2] ;
     } else {
 
         $apiPostData["dob_day"]="" ;
         $apiPostData["dob_month"]="";
         $dob_month = "";
         $apiPostData["dob_year"]="";
 
     }
     $anniversary_date = "";
     $anniversary_day = "";
     $anniversary_year = "";
     $anniversary_month = "";
     if(isset($_POST["marital_status"]) and $_POST["marital_status"] == "Yes"){
         if(isset($_POST['doa']) && strlen($_POST['doa']) >0){
             $doaAry=explode("/",$_POST['doa']);
             $apiPostData["anniversary_day"]=$doaAry[0] ;
             $apiPostData["anniversary_month"]=strtoupper(date("M",strtotime(implode("-",$doaAry))));
             $apiPostData["anniversary_year"]=$doaAry[2] ;
             $apiPostData['anniversary_date']=$_POST['doa'];
             $anniversary_date = $_POST['doa'];
             $anniversary_day =  $doaAry[0] ;
             $anniversary_year = $doaAry[2] ;
             $anniversary_month = strtoupper(date("M",strtotime(implode("-",$doaAry))));
             // Spouse DOB
             $dobAry=explode("/",$_POST['spouse_dob']);
             $apiPostData["spouse_dobday"]=$dobAry[0]??'';
             $apiPostData["spouse_dobmonth"]=strtoupper(date("M",strtotime(implode("-",$doaAry))));
             $apiPostData["spouse_dobyear"]=$dobAry[2]??'';
   
         }
     }
     else {
         // $marital_status="No";
         $anniversary_day =  "";
         $anniversary_month = "";
         $anniversary_year = "";
     }
 
 
     if($req->has('self'))
         {
             $info['self']=$_POST['self'];
             
         }
         if($req->has('spouse'))
         {
             $info['spouse']=$_POST['spouse'];
         }
         if($req->has('parents'))
         {
             $info['parents']=$_POST['parents'];
            
         }
         if($req->has('children'))
         {
             $info['children']=$_POST['children'];
            
         }
         $info['who_wear_spectacles']=$_POST['who_wear_spectacles']??'';
     
   //  $apiPostData['channelcode']=API_CHANNEL_CODE();
  
   $apiPostData['channelcode']=  "encirclewebsite";
 
     $apiPostData['mobile1']=$mobile; 
     $apiPostData['country']="United Arab Emirates";
 
     if($log_country=='usa') {
         $apiPostData['country']="United States";
      /*    $apiPostData['pin_code']="99501"; */
 
      if($_ENV['ENABLED']=='enabled') {
         $apiPostData['channelcode']= "";
    } else {
 
     $apiPostData['channelcode']=  $_ENV['CHANNEL_CODE_USA'];
    }
 
     }
    /*  $apiPostData['marital_status']="";
     $apiPostData['last_name']=""; */
     
         $post_data=[];
         $post_data1=[];
         foreach($mappingColumns as $k=>$datakey){
            $data="";
             if(isset($apiPostData[$k])) {
                 $data=$apiPostData[$k]; 
             } 
             if($log_country=='usa') {
                 $edata=$data;
             }
             else {
                 $edata=encryptData($data);
             }
            
             $post_data[$datakey] = $edata;
             $post_data1[$datakey] = $data;
         } 
 
        $update_data = $post_data;
       // $response = Http::post($API_URL, $post_data);
       $log_url=$API_URL;
        try {
         if($log_country=='usa') {
 
         
             $token =session('token',null);
             $response = Http::withToken($token)->post($API_URL, $post_data);
 
 
         } else {
 
             $response = Http::post($API_URL, $post_data);
 
         }
         
        /*  ApiMethods::api_logs($response,$post_data,$log_url,$log_country); */
        } catch (\Exception $e) {
         $response="";
       /*   ApiMethods::api_logs($response,$post_data,$log_url,$log_country); */
         return false;
        }
        $json = $response->getBody();
        $data = json_decode($json, true);
        if($log_country=='usa') {
 
         $errcode = $data['MemberEnrollment']['ErrorCode'];
 
 
        } else {
 
         $errcode = decryptData($data['MemberEnrollment']['ErrorCode']);
 
        }
        if(isset($data['MemberEnrollment']['ErrorCode'])) {
 
         if($log_country=='usa') {
        $errcode = $data['MemberEnrollment']['ErrorCode'];
         }
        else {
 
         $errcode = decryptData($data['MemberEnrollment']['ErrorCode']);
 
        }
        $member_no = decryptMemberEnrollData($data);
        
        if($errcode==0) {
 
        /*  $update_data['MembershipNo']=encryptData($member_no);
         $update_data['EmailID']=encryptData('');
 
         $response = Http::post($UP_URL, $update_data); */
 
 
 
         $fullname = $post_data1['FirstName']." ".$post_data1['LastName'];
         $DOBMonth = $post_data1['DOBMonth'];
         $post_data['ULPNo']=encryptData($member_no);
         $post_data['Mobile1']=encryptData($mobile);
         $post_data['MemberTier']=encryptData('Encircle Silver');
         $post_data['MemberName']=encryptData($fullname);
         $post_data['LastName']=encryptData($_POST['last_name']);
         $post_data['DayMonth']=encryptData($dob_month);
         // if(empty($_POST['gender'])) {
         //     $post_data['Gender']=encryptData('male');
         // } else {
         //     $post_data['Gender']=encryptData($_POST['gender']);
         // }
        // $post_data['Gender']=encryptData(empty($_POST['gender'])?"":$_POST['gender']);
         $info =  decryptMemberInfoEnroll($post_data);
         $info['last_name']=$_POST['last_name'];
         $info['email']=empty($_POST['email'])?"":$_POST['email'];
         $info['dob_day']= $apiPostData["dob_day"];
         $info['dob_month']=$apiPostData["dob_month"];
         $info['dob_year']=$apiPostData["dob_year"];
         $info['gender']=$apiPostData["gender"];
         $info['title']=$apiPostData["title"];
 
 
 
         if (isset($_POST["things"])) {
 
             $things = $_POST["things"];
       
            foreach($things as $x => $val) {
             $info[$val]="Y";
       
             }
           }
         
         $info['spouse_dobday']="";
         $info['spouse_dobmonth']="";
         $info['spouse_dobyear']="";
         $marital_status = "";
         if($req->has('marital_status'))
         {
             if($_POST['marital_status']=='Yes') {
                 $marital_status=2;
 
                 $dobAry=explode("/",$_POST['spouse_dob']);
                 $info["spouse_dobday"]=$dobAry[0]??'';
                 $info["spouse_dobmonth"]=$dobAry[1]??'';
                 $info["spouse_dobyear"]=$dobAry[2]??'';
 
 
             } else if($_POST['marital_status']=='No') {
                 $marital_status=1;
             }
         }
         if($req->has('self'))
         {
             $info['self']=$_POST['self'];
             
         }
         if($req->has('spouse'))
         {
             $info['spouse']=$_POST['spouse'];
         }
         if($req->has('parents'))
         {
             $info['parents']=$_POST['parents'];
            
         }
         if($req->has('children'))
         {
             $info['children']=$_POST['children'];
            
         }
         $info['marital_status']=$marital_status;
         $info['who_wear_spectacles']=$_POST['who_wear_spectacles']??'';
         $info['anniversary_day']= $anniversary_day;
         $info['anniversary_month']=  $anniversary_month;
         $info['anniversary_year']=   $anniversary_year;
         $info['anniversary_date']=   $anniversary_date;
         if($log_country=='usa') {
         $info['token']=   session('token',null);
         $info['first_name']=  $apiPostData["first_name"];
         
         }
        // $info['gender']=  'male';

         $mobile1 = session('SESS_MOBILE');
         
         Session::forget('SESS_USER_INFO');
         Session::forget('SESS_MOBILE');


         $info['street_name'] = $req->input('street_name');
         $info['city_others'] = $req->input('city_others');
         $info['state'] = $req->input('state');
         $info['country'] = $req->input('country');
         $info['mobile1'] = $mobile1;

         Session::put('SESS_USER_INFO', $info);
         session()->save();
         $member_info=$info;
         $save = loginlogswatches($member_info);
         if(empty($member_info['gender'])) {
             $member_info['gender']='male';
         }
         $ip = $req->ip();
         $date = Carbon::now();
         $date = $date->toDateTimeString();

         return ApiParams::printResponse("success","success");
 
        } else {
        
 
         if($log_country=='usa') {
             $error = ApiMessage::$messages[$data['MemberEnrollment']['ErrorCode']];
 
              }
              else {
 
                 $error = ApiMessage::$messages[decryptData($data['MemberEnrollment']['ErrorCode'])];
 
              }
 
         return ApiParams::printResponse($error,"error"); 
      }
     }
     }
}
