<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Models\ProfileModel as ProModel; 
use Carbon\Carbon; 
use Cookie;
use App\Models\ApiMessage;
use App\Models\ApiMethods;
use Session;
use Artisan;
use phpseclib3\Crypt\RSA;
use DB;
use phpseclib3\Crypt\PublicKeyLoader;
use Illuminate\Support\Facades\Redirect;
use App\Models\ApiParams;
use Validator;

class ProfileUpdate extends Controller
{
   

        public function personalinfo_update(Request $req) {


            $API_URL = session('API_URL');
                $log_country  = Session::get('log_country',null);
                $API_URL = $API_URL.'/UpdateProfile';
                $_POST = $req->all();
                // Change Date format
                if(!empty($_POST['spouse_dob'])) {
                  $_POST['spouse_dob'] = date("d/m/Y", strtotime($_POST['spouse_dob']));
                  } else {
                    $_POST['spouse_dob'] ="";
            
                  }
            
                  if(!empty($_POST['anniversary_date'])) {
                    $_POST['anniversary_date'] = date("d/m/Y", strtotime($_POST['anniversary_date']));
                    } else {
            
                      $_POST['anniversary_date'] ="";
            
                    }
            
                    if(!empty($_POST['dob'])) {
                      $_POST['dob'] = date("d/m/Y", strtotime($_POST['dob']));
                      } else {
            
                        $_POST['dob'] ="";
                      }
            
            
            
            
                $requiredFields=array(
                "title"=>"Title",
                "first_name"=>"FirstName",
                "last_name"=>"Lastname",
                "email"=>"EmailId",
                "mobile1"=>"Mobile",
                "houseno"=>"HouseNo",
                "building_name"=>"BuildingName",
                "street_road"=>"StreetRoad",
                "colony"=>"Colony",
                "city"=>"City",
                "pin_code"=>"PinCode",
                "dob"=>"DOBDay",
                "marital_status"=>"MaritalStatus",
                "gender"=>"Gender",
                "mothertongue"=>"MotherTongue",
                "age_group"=>"AgeGroup",
                "profession"=>"Profession",
                // "public_private_sector"=>"WhereDoYouWork",
                "who_wear_spectacles"=>"WhoWearSpectacles",
                // "celebreate"=>"Festivals You Celebrate"
                );
                foreach($requiredFields as $key=>$val){
                  if(!isset($_POST[$key]) ){
                        // return ApiParams::printResponse($val,"error");
                      }
                }
                $requiredMFields=array("spouse_name","spouse_dob","anniversary_date");
                $requiredCH1Fields=array("child1","child1_name","child1_dob");
                $requiredCH2Fields=array("child2","child2_name","child2_dob");
            
                if(isset($_POST["marital_status"]) and $_POST["marital_status"] == "Yes"){
            
                  $_POST["marital_status"]="2";
            
                } else {
            
                  $_POST["marital_status"]="1";
            
            
                }
                
                if(isset($_POST["marital_status"]) and $_POST["marital_status"] == "2"){
                   
                    
                 /*    foreach($requiredMFields as $key=>$val){
                        if(!isset($_POST[$val]) || strlen($_POST[$val])<1){
                            $msg="Please enter ".toTitle($val);
                            return ApiParams::printResponse($msg,"error");
                        }
                    } */
                    $dobAry=explode("/",$_POST['spouse_dob']);
                    $_POST["spouse_dobday"]=$dobAry[0]??'';
                    // $_POST["spouse_dobmonth"]=strtoupper(date("M",strtotime($dobAry[1])));
                    $_POST["spouse_dobmonth"]=$dobAry[1]??'';
                    $_POST["spouse_dobyear"]=$dobAry[2]??'';
                    
                    $dobAry=explode("/",$_POST['anniversary_date']);
                    $_POST["anniversary_day"]=$dobAry[0]??'';
                    // $_POST["anniversary_month"]=strtoupper(date("M",strtotime($dobAry[1])));
                    $_POST["anniversary_month"]=$dobAry[1]??'';
                    $_POST["anniversary_year"]=$dobAry[2]??'';
                    
                    
                    if(isset($_POST["have_children"]) and $_POST["have_children"] == "Y"){
                        
                      /*   foreach($requiredCH1Fields as $key=>$val){
                            if(!isset($_POST[$val]) || strlen($_POST[$val])<1){
                                $msg="Please enter ".toTitle($val);
                                return ApiParams::printResponse($msg,"error");
                            }
                        } */
                        $dobAry=explode("/",$_POST['child1_dob']);
                        $_POST["child1_dob_day"]=$dobAry[0] ;
                        // $_POST["child1_dob_month"]=strtoupper(date("M",strtotime($dobAry[1])));
                        $_POST["child1_dob_month"]=$dobAry[1] ;
                        $_POST["child1_dob_year"]=$dobAry[2] ;
                        
                        if(isset($_POST["child2_dob"]) && $_POST["child2_dob"] > 0){
                            $dobAry=explode("/",$_POST['child2_dob']);
                            $_POST["child2_dob_day"]=$dobAry[0] ;
                            // $_POST["child2_dob_month"]=strtoupper(date("M",strtotime($dobAry[1])));
                            $_POST["child2_dob_month"]=$dobAry[1] ;
                            $_POST["child2_dob_year"]=$dobAry[2] ;
                        }else{
                            foreach($requiredCH2Fields as $key=>$val){
                                if(isset($_POST[$val])){
                                    $_POST[$val]="";
                                }
                            }
                        }
                    }else {
                        foreach($requiredCH1Fields as $key=>$val){
                            if(isset($_POST[$val])){
                                $_POST[$val]="";
                            }
                        }
                        
                        foreach($requiredCH2Fields as $key=>$val){
                            if(isset($_POST[$val])){
                                $_POST[$val]="";
                            }
                        }
                    }
                }else {
                    foreach($requiredMFields as $key=>$val){
                        if(isset($_POST[$val])){
                            $_POST[$val]="";
                        }
                    }
                    foreach($requiredCH1Fields as $key=>$val){
                        if(isset($_POST[$val])){
                            $_POST[$val]="";
                        }
                    }
                    
                    foreach($requiredCH2Fields as $key=>$val){
                        if(isset($_POST[$val])){
                            $_POST[$val]="";
                        }
                    }
                }
                if(isset($_POST["celebreate"]) and is_array($_POST["celebreate"])){
                    $_POST["celebreate"]=implode(",",$_POST["celebreate"]);
                }
                  $dobAry=explode("/",$_POST['dob']);
                  $_POST["dob_day"]=$dobAry[0]??'';
                  $_POST["dob_month"]=$dobAry[1]??'';
                  $_POST["dob_year"]=$dobAry[2]??'';
                  if (isset($_POST["things"])) {
            
                  $things = $_POST["things"];
            
                 foreach($things as $x => $val) {
                  $_POST[$val]="Y";
            
                  }
                }
                
                $setDefaultFields=array(
                         "self",
                        "spouse",
                        "parents",
                        "children",
                        "adventure",
                        "cricket",
                        "carmotors",
                        "football",
                        "swimming",
                        "tennis",
                        "water",
                        "golf",
                        "sports_others",
                        "art",
                        "cinema",
                        "music",
                        "culinary",
                        "reading",
                        "photography",
                        "social_networking",
                        "theatre",
                        "yoga",
                        "healthwellness",
                        "hobbies_others"
                        );
                foreach($setDefaultFields as $key){
                    if(!isset($_POST[$key]))$_POST[$key]='N';
                    if(isset($_POST[$key]) && $_POST[$key] !='Y')$_POST[$key]='N';
                }
              $Data=session('SESS_USER_INFO');
              $apiPostData = $_POST;
              $apiPostData['member_no']=$Data['member_no'];
              $apiPostData['landline']=$Data['mobile1'];
            
              if(!$apiPostData || count($apiPostData) < 1)return false;
              $mappingColumns = ApiParams::updateMemberParams();
            
              $post_data=[];
              
              $post_dataDUMMY=[];
              foreach($mappingColumns as $k=>$datakey){
                  $data="";
                  if(isset($apiPostData[$k])){
                      $data=$apiPostData[$k]; 
                  }
                  if ($log_country == 'usa') {
                    $edata=$data;
                  } else {
            
                    $edata=encryptData($data);
                  }
               
                  $post_data[$datakey] = $edata;
                  $post_dataDUMMY[$datakey] = $data;
                  
              }
                
            
              $log_url = $API_URL;


              try {
              
                if ($log_country == 'usa') {
                  $response = Http::withToken($Data['token'])->post($API_URL, $post_data);
                 }else{
                  $response = Http::post($API_URL, $post_data); 
                 }
               ApiMethods::api_logs($response,$post_data,$log_url,$log_country);
            } catch (\Exception $e) {
               $response="";
               ApiMethods::api_logs($response,$post_data,$log_url,$log_country);
               return false;
            }

            $json = $response->getBody();
            $data = json_decode($json, true); 
            if ($log_country == 'usa') {
              $errcode = $data['UpdateProfile']['ErrorCode'];
            } else {
            $errcode = decryptData($data['UpdateProfile']['ErrorCode']);
          }
            if($errcode==0) 
            {
              $member_info  = $this->sessionUpdate($post_dataDUMMY,$Data);

              $member_info['street_name'] = $req->input('street_name');
              $member_info['city_others'] = $req->input('city_others');
              $member_info['state'] = $req->input('state');
              $member_info['country'] = $req->input('country');

              

              Session::forget('SESS_USER_INFO');
              Session::put('SESS_USER_INFO', $member_info);
              session()->save();
              $save = loginlogswatches($member_info);
              return ApiParams::printResponse("success","success");    
            } 
            else 
            {
              if ($log_country == 'usa') {
              $error = ApiMessage::$messages[$data['UpdateProfile']['ErrorCode']];
          
              } else {
          
                $error = ApiMessage::$messages[decryptData($data['UpdateProfile']['ErrorCode'])];
          
              }
              return ApiParams::printResponse($error,"error");
            }  

}



public function sessionUpdate($post_dataDUMMY,$Data)
{

  $logged_in_country  = (Session::get('API_URL',null)===$_ENV['API_URL_UAE']) ? "uae" : "india";
  $get_title  = DB::connection('coredb')
                  ->table('do_lists')
                  ->where('country','=', $logged_in_country)
                  ->where('blocked','=','false')
                  ->where('groupid','=', 'profile_title')
                  ->where('value','=', $post_dataDUMMY['TitleCode'])
                  ->first(); 
  $title_name = ($get_title) ? $get_title->title : $Data['title'];
  $get_city   = DB::connection('coredb')
                  ->table('do_lists')
                  ->where('country','=', $logged_in_country)
                  ->where('blocked','=','false')
                  ->where('groupid','=','profile_city')
                  ->where('value', $post_dataDUMMY['City'])
                  ->first();
  $city       = ($get_city) ? $get_city->title : $Data['city'];
  $festivals  = DB::connection('coredb')
                  ->table('do_lists')
                  ->where('country','=', $logged_in_country)
                  ->where('blocked','=','false')
                  ->where('groupid','=','festival')
                  ->get();
    $updated_array=[];
    foreach($festivals as $newFestivals)
    {
      $updated_array[$newFestivals->title ]=$newFestivals->value;
      //array_push($updated_array, [$newFestivals->title => $newFestivals->value]);   
    }
    $updated_array['Ramdan']='016';
    $updated_array['Eid al-Adha']='017';
    $updated_array['Ras as-Sana ( Hijri New Year )']='018';
    $updated_array['Prophet’s Birthday']='019';
    //array_push($updated_array, ['Ramdan' => '016', 'Eid al-Adha' => '017', 'Ras as-Sana ( Hijri New Year )' => '018', 'Prophet’s Birthday' => '019']);
    //dd($updated_array);
    //$updated_array1=['Ramdan','016','Eid al-Adha','017','Ras as-Sana ( Hijri New Year )','018','Prophet’s Birthday','019'];
    //$newFestival_array=array_merge($updated_array,$updated_array1);
    
    

  $selected_festivals = array_map('trim', explode(',', $post_dataDUMMY['FestivalsICelebrate'])); 
  $selected_festival_name_array  = []; 

  foreach($updated_array as $key => $value)
  {
   
    if(in_array(trim($value),$selected_festivals)){
      array_push($selected_festival_name_array,$key);
    }
  }
  $festival_names = (count($selected_festival_name_array) > 0) ? implode(',', $selected_festival_name_array) : '';
  $updated_session = [
    "member_no"           => $post_dataDUMMY['MembershipNo'],
    "full_name"           => $post_dataDUMMY['FirstName'].' '.$post_dataDUMMY['Lastname'],
    "member_since"        => $Data['member_since'],
    "balance"             => $Data['balance'],
    "member_tier"         => $Data['member_tier'],
    "title"               => $title_name,
    "first_name"          => $post_dataDUMMY['FirstName'],
    "last_name"           => $post_dataDUMMY['Lastname'],
    "landline"            => $post_dataDUMMY['LandLine'],
    "mobile1"             => $Data['mobile1'],
    "mobile2"             => $Data['mobile2'],
    "email"               => $post_dataDUMMY['EmailId'],
    "houseno"             => $post_dataDUMMY['HouseNo'],
    "building_name"       => $post_dataDUMMY['BuildingName'],
    "street_road"         => $post_dataDUMMY['StreetRoad'],
    "colony"              => $post_dataDUMMY['ColonySociety'],
    "expiry_date"         => $Data['expiry_date'],
    "city"                => $city,
    "city_other"          => $Data['city_other'],
    "pin_code"            => $post_dataDUMMY['PinCode'],
    "dob_day"             => $post_dataDUMMY['DOBDay'],
    "dob_month"           => $this->getMonthName($post_dataDUMMY['DOBMonth']),
    "dob_year"            => $post_dataDUMMY['DOBYear'],
    "marital_status"      => $post_dataDUMMY['MaritalStatus'],
    "spouse_name"         => $post_dataDUMMY['SpouseName'],
    "spouse_dobday"       => $post_dataDUMMY['SpouseDOBDay'],
    "spouse_dobmonth"     => $this->getMonthName($post_dataDUMMY['SpouseDOBMonth']),
    "spouse_dobyear"      => $post_dataDUMMY['SpouseDOBYear'],
    "anniversary_day"     => $post_dataDUMMY['AnniversaryDay'],
    "anniversary_month"   => $this->getMonthName($post_dataDUMMY['AnniversaryMonth']),
    "anniversary_year"    => $post_dataDUMMY['AnniversaryYear'],
    "relationship"        => $Data['relationship'],
    "gender"              => $post_dataDUMMY['Gender'],
    "mothertongue"        => $post_dataDUMMY['MotherTongue'],
    "age_group"           => $post_dataDUMMY['AgeGroup'],
    "have_children"       => $post_dataDUMMY['DoYouhaveChildren'],
    "child1"              => $post_dataDUMMY['Child1'],
    "child1_name"         => $post_dataDUMMY['Child1Name'],
    "child1_dob_day"      => $post_dataDUMMY['Child1DOBDay'],
    "child1_dob_month"    => $this->getMonthName($post_dataDUMMY['Child1DOBMonth']),
    "child1_dob_year"     => $post_dataDUMMY['Child1DOBYear'],
    "child2"              => $post_dataDUMMY['Child2'],
    "child2_name"         => $post_dataDUMMY['Child2Name'],
    "child2_dob_day"      => $post_dataDUMMY['Child2DOBDay'],
    "child2_dob_month"    => $this->getMonthName($post_dataDUMMY['Child2DOBMonth']),
    "child2_dob_year"     => $post_dataDUMMY['Child2DOBYear'],
    "profession"          => $post_dataDUMMY['Profession'],
    "profession_oth"      => $post_dataDUMMY['ProfessionOther'],
    "public_private_sector"=> $Data['public_private_sector'],
    "who_wear_spectacles" => $post_dataDUMMY['WhoWearSpectacles'],
    "self"                => $post_dataDUMMY['Self'],
    "spouse"              => $post_dataDUMMY['Spouse'],
    "parents"             => $post_dataDUMMY['Parents'],
    "children"            => $post_dataDUMMY['Children'],
    "celebreate"          => $festival_names,
    "celebreate_oth"      => $Data['celebreate_oth'],
    "adventure"           => $post_dataDUMMY['Adventure'],
    "cricket"             => $post_dataDUMMY['Cricket'],
    "carmotors"           => $post_dataDUMMY['CarMotors'],
    "football"            => $post_dataDUMMY['Football'],
    "swimming"            => $post_dataDUMMY['Swimming'],
    "tennis"              => $post_dataDUMMY['Tennis'],
    "water"               => $post_dataDUMMY['Water'],
    "golf"                => $post_dataDUMMY['Golf'],
    "sports_others"       => $post_dataDUMMY['SportsOthers'],
    "art"                 => $post_dataDUMMY['Art'],
    "cinema"              => $post_dataDUMMY['Cinema'],
    "music"               => $post_dataDUMMY['Music'],
    "culinary"            => $post_dataDUMMY['Culinary'],
    "reading"             => $post_dataDUMMY['Reading'],
    "photography"         => $post_dataDUMMY['Photography'],
    "social_networking"   => $post_dataDUMMY['SocialNetworking'],
    "theatre"             => $post_dataDUMMY['Theatre'],
    "yoga"                => $post_dataDUMMY['Yoga'],
    "healthwellness"      => $post_dataDUMMY['HealthWellness'],
    "hobbies_others"      => $post_dataDUMMY['HobbiesOthers']
  ];
  return $updated_session;
}
public function getMonthName($month)
{
  return date("F", strtotime(date("d-$month-y")));
}

}
