<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Response;

class WatchesMeet extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {

        if ($request->ajax()) {

        
           $data   = DB::connection('appdb')
                       ->table('profiletbl_watches')
                       ->where('country',Session::get('admin_country'))
                       ->select('*');
                      return Datatables::of($data)
                   ->addIndexColumn()
                   ->make(true);
       }
   
       return view('admin.watches.profile-list-watches');

   }
   public function export(Request $request) {

    $country=Session::get('admin_country');
    $fileName = "Log_Watches"."_".time().".csv";
    $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=$fileName",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
        );
        $columns = array("SI#","Card No","First Name","Last Name","Date Of Birth","Contact No","Email","Marital Status","Anniversary","Country","State","City","City Others","Colony","Street","Building Name","House No","Credited Points","Login Type","Date");

        $callback = function() use($country, $columns) {
        $file = fopen('php://output', 'w');
         fputcsv($file, $columns);
    
            $data = DB::connection('appdb')
            ->table('profiletbl_watches as a')
            ->where('a.country',$country)
            ->select('a.*')
            ->get(); 
            $serial = 1;
            foreach ($data as $datas) {
                $row['id']=$serial++;
                $row['member_no']=$datas->member_no;
                $row['first_name']=$datas->first_name;
                $row['last_name']=$datas->last_name;
                $row['dob']=$datas->dob;
                $row['mobile']=$datas->mobile;
                $row['email']=$datas->email;
                $row['marital_status']=$datas->marital_status;
                $row['doa']=$datas->doa;
                $row['api_country']=$datas->api_country;
                $row['state']=$datas->state;
                $row['city']=$datas->city;
                $row['city_others']=$datas->city_others;
                $row['colony']=$datas->colony;
                $row['street_name']=$datas->street_name;
                $row['building_name']=$datas->building_name;
                $row['house_no']=$datas->house_no;
                $row['points_credited']=$datas->points_credited;
                $row['login_type']=$datas->login_type;
                $row['created_on']=$datas->created_on;
               $xcd = fputcsv($file, array($row['id'],$row['member_no'],$row['first_name'],$row['last_name'],$row['dob'],$row['mobile'],$row['email'],$row['marital_status'],$row['doa'],$row['api_country'],$row['state'],$row['city'],$row['city_others'],$row['colony'],$row['street_name'],$row['building_name'],$row['house_no'],$row['points_credited'],$row['login_type'],$row['created_on']));
               
            } 
         fclose($file);
        };
    return Response::stream($callback, 200, $headers);

    }

}
