<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home');
    }

    public function setCountryToSession(Request $request)
    {
        $admin_country  = $request->admin_country;
        Session::put('admin_country', $admin_country);
        session()->save();
    }
    public function logout()
    {

        Session::flush();
        return redirect()->route('login');

    }
   
}
