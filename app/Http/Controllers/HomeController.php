<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Http;
use phpseclib3\Crypt\RSA;
use phpseclib3\Crypt\PublicKeyLoader;
use Carbon\Carbon; 

class HomeController extends Controller
{
    public function index(Request  $req) {



        $ip_loc= iplocation();
        
        if ($req->has('url')) {
            $url = $req->get ('url');
            session(['SESS_URL_INFO' => $url]);
        } else {

          //  Session::forget('SESS_URL_INFO');
        }

        if (Session::has('SESS_USER_INFO')){
            $Data=session('SESS_USER_INFO')??null;

           /*  return Redirect::to('/MyProfile'); */

           }
          else{
            $Data=array(
                  'first_name'=>''
                 );
           }

        $logged_in_country  = Session::get('logged_in_country',null);
        $now = date('Y-m-d');
   
 
    
           
        return view('home',compact('Data'));

    }
    public function change_country(Request $request) {

            $country  = $request->country;
            Session::put('logged_in_country_user', $country);
            session()->save();
    }
}
