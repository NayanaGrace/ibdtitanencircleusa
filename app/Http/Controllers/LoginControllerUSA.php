<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;

use Illuminate\Http\Request;

use App\Models\ProfileModel as ProModel; 

use Carbon\Carbon; 

use Cookie;

use DB;

use App\Models\ApiMessage;

use App\Models\ApiMethods;

use Session;

use URL;

use Location;

use phpseclib3\Crypt\RSA;

use phpseclib3\Crypt\PublicKeyLoader;


class LoginControllerUSA extends Controller
{
    public function usa() {
       
        Session::put('logged_in_country_user','usa');

        $logged_in_url = $_ENV['APP_URL']."/usa";
  
        Session::put('logged_in_url', $logged_in_url);
     
        if (Session::has('SESS_USER_INFO'))
        {
            return redirect()->route('myprofile-info');
        }

         if(URL::previous()!==url()->full()) {
           $backlink = URL::previous();
           session(['SESSION_BACKLINK' => $backlink]);
        } else {
            $backlink = url()->full();
            session(['SESSION_BACKLINK' => $backlink]);
        }
        return view('usa.login');

    }


    public function sendotpusa(Request $req) {
        $country_value = Session::get('logged_in_country');
        $country_details = countries_details($country_value);
        $API_URL = $country_details->api_url;
        $country_name = $country_details->title;
        $code = $country_details->country_code;
        $digits = $country_details->digits;
        $log_country=Session::get('logged_in_country');
    
        $terms = 1;
        $country = $country_details->country_code;
        $mobile = $req->input('mobile');
        $country_code = $req->input('country_code');
        $email ='';

        // Checking Country india or UAE Starts Here
        $limit = strlen($code);
        $limit = $limit+1;
        $code_plus = "+".$code;
        if($country==$code_plus) {
            $mobile_sub=substr($mobile,0,$limit);
        if($mobile_sub == $code_plus)
        {
            $mobile1 = str_replace($country_code,"",$mobile);
            $mobile2 = trim($mobile1);
            ($country_value=='india') ? $mobile = $mobile2 :  $mobile = str_replace("+","",$mobile);
            
            $length = strlen($mobile2);
            if($length !== $digits){
                return response()->json([
                "status" => "error","msg"=>"MOBILE NUMBER SHOULD BE $digits DIGIT"
               ]);
            }
        }
        else {

            $mobile1 = str_replace("+91","",$mobile);
            $mobile2 = trim($mobile1);
            $mobile = str_replace("+","",$mobile);
            $length = strlen($mobile2);
            if($length !== 10){
                return response()->json([
                'status' => 'error','msg'=>'MOBILE NUMBER SHOULD BE 10 DIGIT'.$length
               ]);
            }


        }
        }
    if($terms==0) {
    return response()->json([
        'status' => 'error','msg'=>'PLEASE CHECK TERMS AND CONDITIONS'
       ]);
    }
    else {

    $post_data =  array(
        "MobileNo"=>$mobile
       );
       
    
       
       $log_url = $API_URL.'/MemberSearch';

       try {
        $response = Http::post($API_URL.'/MemberSearch', $post_data);
       /*  ApiMethods::api_logs($response,$post_data,$log_url,$log_country); */
    } catch (\Exception $e) {
        $response="";
       /*  ApiMethods::api_logs($response,$post_data,$log_url,$log_country); */
        return false;
    }
$json = $response->getBody();
$data = json_decode($json, true);

if($log_country=='usa') {
$errcode = $data['MemberSearch']['ErrorCode'];

} else {

$errcode = decryptData($data['MemberSearch']['ErrorCode']);

}
if(isset($errcode) and  $errcode== "-1344"){
$post_data =  array(
    "MobileNo"=>encryptData($mobile),
    "EmailID"=>encryptData($email) 
   );
   if($log_country=='usa') {
    $post_data =  array(
        "MobileNo"=>$mobile,
        "EmailID"=>$email
       );
   

}



    $log_url = $API_URL.'/MemberVerification';
       try {
        $response = Http::post($API_URL.'/MemberVerification', $post_data);
        /* ApiMethods::api_logs($response,$post_data,$log_url,$log_country); */
    } catch (\Exception $e) {
        $response="";
       /*  ApiMethods::api_logs($response,$post_data,$log_url,$log_country); */
        return false;
    }
$json = $response->getBody();
$data = json_decode($json, true);

if($log_country=='usa') {
$errcode =$data['MemberVerification']['ErrorCode'];

}
else {

$errcode = decryptData($data['MemberVerification']['ErrorCode']);

}
if($errcode==0) {
session(['is_member' => "No"]);
return response()->json([
'status' => 'success','msg'=>1,'msg2'=>'enroll','WhatsApp'=>'N'
]);
} else {
if($log_country=='usa') {

    $error = ApiMessage::$messages[$data['MemberVerification']['ErrorCode']];
} else {

    $error = ApiMessage::$messages[decryptData($data['MemberVerification']['ErrorCode'])];
}

return response()->json([
 'status' => 'error','msg'=>$error
]);
}
}
if(isset($data['MemberSearch']['ErrorCode'])) {

if($log_country=='usa') {

$errcode = $data['MemberSearch']['ErrorCode'];

} else {
$errcode = decryptData($data['MemberSearch']['ErrorCode']);
}
if($errcode==0) {

// $WhatsApp = decryptData($data['MemberSearch']['WhatsAppStatus']);
$WhatsApp = "Y";


session(['is_member' => "YES"]);
return response()->json([
'status' => 'success','msg'=>1,'msg2'=>'member','WhatsApp'=>$WhatsApp
]);
} else {



if($log_country=='usa') {
    $error = ApiMessage::$messages[$data['MemberSearch']['ErrorCode']];
} else {
    $error = ApiMessage::$messages[decryptData($data['MemberSearch']['ErrorCode'])];
}
return response()->json([
 'status' => 'error','msg'=>$error
]);
}
}  
}

}


public function validateotpusa(Request $req) {

    $country_value = Session::get('logged_in_country');

   /*  $country_value = $req->input('country_value'); */
    $country_details = countries_details($country_value);
    $API_URL = $country_details->api_url;
    $country_name = $country_details->title;
    $code = $country_details->country_code;
    $digits = $country_details->digits;
    /* $log_country=$req->input('country_value'); */
    $log_country=Session::get('logged_in_country');
    $Whatsapp=$req->input('Whatsapp');
        $country = $req->input('country');
        $mobile = $req->input('mobile');
        $terms = $req->input('terms');
     /*    $loyalty = $req->input('loyalty'); */
        $email ='';

        if($log_country=='usa') {
           /*  if($loyalty==0) {
                return response()->json([
                    'status' => 'error','msg'=>'PLEASE CHECK  Loyalty Program Enrollment & Account related communication'
                   ]);
                } */
        }
       
        if($country_value=="india") {
            $mobile = str_replace("+91","",$mobile); 
        } else {
            $mobile = str_replace("+","",$mobile);
        }
        
$digits = $req->input('otp');
$post_data = array(
"MobileNo"=>encryptData($mobile),
"OTPNumber"=>encryptData($digits),
"WhatsAppStatus"=>encryptData($Whatsapp)
);
if($log_country=='usa') {   

    $post_data = array(
        "MobileNo"=>$mobile,
        "OTPNumber"=>$digits
        );
}
if($terms==0) {
    return response()->json([
        'status' => 'error','msg'=>'PLEASE CHECK TERMS AND CONDITIONS'
       ]);
    } 
    else {
 if(session('is_member')=="YES")  {
    $VAL_URL = $API_URL."/ValidateOTP";

 } else {
  //  $VAL_URL = $API_URL."/ValidateOTP";
    $VAL_URL = $API_URL."/ValidateOTPMemberSite";
 }
$log_url = $VAL_URL;
try {
 $response = Http::post($VAL_URL, $post_data);
 /* ApiMethods::api_logs($response,$post_data,$log_url,$log_country); */
} catch (\Exception $e) {
 $response="";
 /* ApiMethods::api_logs($response,$post_data,$log_url,$log_country); */
 return false;
}
$json = $response->getBody();
$data = json_decode($json, true);
/* $member_info = decryptMemberInfoData($data);
 */$member_info = decryptMemberInfoData($data,$log_country);

if(session('is_member')=="YES")  {

    Session::forget('SESS_MOBILE');
    if($log_country=='usa') {
  

    $errcode = $data['ValidateOTP']['ErrorCode'];
    $Member = $data['ValidateOTP']['ErrorCode'];
    }
    else {
       
        $errcode = decryptData($data['ValidateOTP']['ErrorCode']);
        $Member = decryptData($data['ValidateOTP']['ErrorCode']);
    }

} else {
    session(['SESS_MOBILE' => $mobile]);
    if($log_country=='usa') {

        $errcode = $data['ValidateOTPMemberSite']['ErrorCode'];
        $Member = $data['ValidateOTPMemberSite']['ErrorCode'];
    
    }
    else {
    $errcode = decryptData($data['ValidateOTPMemberSite']['ErrorCode']);
    $Member = decryptData($data['ValidateOTPMemberSite']['ErrorCode']);

}
}

if(isset($errcode)) {
if($errcode==0) {
   
    session(['SESS_USER_INFO' => $member_info]);
    $ip = $req->ip();
    $date = Carbon::now();
    $date = $date->toDateTimeString();
if(count($member_info)>0) {
     $login_type="login";
    /*  ApiMethods::logsession_api($member_info,$ip,$date,$log_country,$login_type);
     
     $For_You_Only =for_you_only_eligibility($member_info,$ip,$date,$log_country,$login_type); */

       // track back links for enroll
      /*  $enroll_tracker = enroll_tracker($member_info,$ip,$date,$log_country,$login_type); */
} else {

    if($log_country=='usa') {
       $token = $data['ValidateOTPMemberSite']['Token'];
       session(['token' => $token]);
    }
}
session(['API_URL' => $API_URL]);
session(['log_country' => $log_country]);
 /* if(Session::get('API_URL',null)===$_ENV['API_URL']) {
  get_notification($mobile,$log_country);
  WhatsappInfoData($mobile,$country_value,$Whatsapp,$ip);
 }  */
// Whatsapp Info Data if country is india
 if($log_country=='india') {
   /*  get_notification($mobile,$log_country);
    WhatsappInfoData($mobile,$country_value,$Whatsapp,$ip); */
   } 




/* if($_ENV['ENABLED']=='enabled' && Session::get('API_URL',null)===$_ENV['API_URL']) {
    get_notification($mobile);
} */
$flag = session('is_member');

return response()->json([
'status' => 'success','member' =>$flag
]);
} else {
   
$error = ApiMessage::$messages[$errcode];
return response()->json([
'status' => 'error','msg'=>$error
]);
}
}
}
}



public function signup(Request $req) {

             $mobile= $req->input('mobile');
             $log_country=Session::get('logged_in_country');
             if($log_country == 'uae'){
                $mobile="+".$mobile;
                     $log_url =$_ENV['one_trust_api_sign_up_url'];
                     $post_data=[
                         "identifier"=>$mobile,
                         "requestInformation"=>$_ENV['sign_up_request_Info'],
                         "purposes"=> [
                             [
                               "Id"=> $_ENV['P1'],
                               "TransactionType"=> "CONFIRMED"
                             ]
                           ]
                        ];
             }
             else{
                $mobile="+".$mobile;
                     $log_url =$_ENV['one_trust_api_sign_up_url_usa'];
                     $post_data=[
                         "identifier"=>$mobile,
                         "requestInformation"=>$_ENV['sign_up_request_Info_usa'],
                         "purposes"=> [
                             [
                               "Id"=> $_ENV['P1_usa'],
                               "TransactionType"=> "CONFIRMED"
                             ]
                           ]
                        ];
             }
                     
                 
                  try {
                     $response = Http::post($log_url, $post_data);
                   //  ApiMethods::api_logs($response,$post_data,$log_url,$log_country);
                    } catch (\Exception $e) {
                     $response="";
                  //   ApiMethods::api_logs($response,$post_data,$log_url,$log_country);
                     return false;
                    } 
                 
} 

}
