<?php

use phpseclib3\Crypt\PublicKeyLoader;
use phpseclib3\Crypt\RSA;
use App\Models\ApiParams;
use App\Models\ApiMethods;
use App\Models\ConfigFileReader;
use App\Models\TblwhatsappStatus;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\ValentinesDaygame as GameModel;
use App\Models\EnrollLogsUrlTrack;

function encryptData($str){

    $key_txt = '<RSAKeyValue><Modulus>8vJswGQk3fl8q0Co6j7/NfkPSVcTNQWlZlVk1Xs47lOGI6VYamrb5u19RR5/JcdNBKPjKQNhfBsFdsYND2O7y5yfCGP1RNtRBHWbhzpnw6iRESXtChwBWOV6gLTpBYTVssJXd9huqhVak6AeP+UncD1areB9ZnsRNy+fHQJJv90=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>'; // public key
    
    $key = PublicKeyLoader::load($key_txt, $password = false);
    $private = $key->withPadding(RSA::ENCRYPTION_PKCS1);
    $ciphertext = $private->encrypt($str);
    // dd($ciphertext);
    // $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1); 
    // $ciphertext = $rsa->encrypt($str);
    return base64_encode($ciphertext);
}
function decryptData($str){
    $decryptedtxt = "";
    $privateKey1 = <<<EOD
    -----BEGIN PRIVATE KEY-----
    MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAPLybMBkJN35fKtAqOo+/zX5D0lXEzUFpWZVZNV7OO5ThiOlWGpq2+btfUUefyXHTQSj4ykDYXwbBXbGDQ9ju8ucnwhj9UTbUQR1m4c6Z8OokREl7QocAVjleoC06QWE1bLCV3fYbqoVWpOgHj/lJ3A9Wq3gfWZ7ETcvnx0CSb/dAgMBAAECgYASy6cWvlis1K2suRioYL3RYOaqG6K4hiqM73vY2UrnA068KMFdoE1JgSOhKpIXK+NdQcmkBWcwGQ4T6ZAeme5wFGIqddw8xvVU8MDmvhD+WDNUYnSueB+/a1X5VlX8ba2mmyj9iBzq0q9k5yBVbLgBtIkqOT+wG46l1s6Q1ZlbUQJBAP5TKFoX7XCPAw+t3xUoket6KFGE4/jX1mhLHRL3J4q9OB0WGViLBBzDateVkq3i5aSoj6UwIGpfH7T9+Lld2S0CQQD0jBTzjOcqoRc36gWOA/xl69ltpHn2Kf/0NaDwE0dN/pAjikSUffNrIUwTQrtgOBYwL9BhJiaVplhjEVmYOE9xAkEAgcqin0fyzyzNHSVY4g1wWsJe7StqxMYyD8u1rMYhkfZ7Oqzahot7r8ozLo6Q6tkO5Xd4x47GHEMrhUzPsJoqiQJBAJNVAhppvawVOaPaPlZ9WEcffPNtsHz6eNZsqPV65+65e6Wt2/uKlmjouqiv/6vMnrGRXP3qujxW59Fn1o66/kECQQDq2rn6uf2X+lAEyUXhKDZyiDmJBT1aEt3welRParhV9nllpcEqU6GoNiUqKQ1qS/tSzASjPdJCLajOgyEJy3ML
    -----END PRIVATE KEY-----
    EOD;

    $res = openssl_get_privatekey($privateKey1);

    $encrypted = base64_decode($str); // decode the encrypted query string
    if (!openssl_private_decrypt($encrypted, $decryptedtxt, $res))
  //  return ('Failed to decrypt data');
  return "";
    return $decryptedtxt; 
}
function formatDateForAPI($date){
    if(strlen($date)>0){
    $dateAry=explode("/",$date);
    $formatedDate=$dateAry[0]."-".strtoupper(date("M",$dateAry[1]))."-".$dateAry[2];
    return $formatedDate;
    }
    return false;
    }
function SendMail($to,$subject,$msg) {
   
    $EMAIL_URL =$_ENV['EMAIL_URL'];
    $POST_MAIL = array(
        "username"=>$_ENV['EMAIL_USER'],
        "password"=>$_ENV['EMAIL_PASS'],
        "emailTo"=>$to,
        "text"=>$msg,
        "subject"=>$subject,
        "fromAddress"=>'encircle@titan.co.in',
        "fromName"=>'Encircle'
    );
  $response = Http::get($EMAIL_URL, $POST_MAIL);
  //return  $response ;

}

function sendSMS($to,$msg) {
    if($_ENV['ENABLED']=='enabled') {
         $to= $_ENV['TEST_MOBILE'];
    }
        $SMS_URL = $_ENV['SMS_URL'];
        $POST_SMS = array(
        "username"=>$_ENV['SMS_USER'],
        "password"=>$_ENV['SMS_PWD'],
        "from"=>$_ENV['SMS_SENDER'],
        "sender"=>$_ENV['SMS_SENDER'],
        "to"=>$to,
        "text"=>$msg
    );
  $response = Http::get($SMS_URL, $POST_SMS);
  return $response;
}

function sendInternationalSMS($mobile,$sender,$message) 
{
    if($_ENV['ENABLED']=='enabled') {
        $mobile= $_ENV['TEST_MOBILE'];
   }
    $INTERNATIONAL_SMS_URL  = $_ENV['INTERNATIONAL_SMS_URL'];
    $INTERNATIONAL_POST_SMS = array(
                                "ver"   => $_ENV['INTERNATIONAL_VERSION'],
                                "key"   => $_ENV['INTERNATIONAL_API_KEY'],
                                "encrpt"=> $_ENV['INTERNATIONAL_ENCRYPT'],
                                "dest"  => $mobile,
                                "send"  => $sender,
                                "text"  => $message
                            );
    $response = Http::get($INTERNATIONAL_SMS_URL, $INTERNATIONAL_POST_SMS);
    return $response;
}
function sendInternationalSMSUsa($mobile,$message) 
{
    if($_ENV['ENABLED']=='enabled') {
        $mobile= $_ENV['TEST_MOBILE'];
   }
    $INTERNATIONAL_SMS_URL  = $_ENV['INTERNATIONAL_SMS_URL_USA'];
    $INTERNATIONAL_POST_SMS = array(
                                "username"   => $_ENV['USA_USER_NAME'],
                                "password"   => $_ENV['USA_PASSWORD'],
                                "mobile"  => $mobile,
                                "smstext"  => $message
                            );
    $response = Http::post($INTERNATIONAL_SMS_URL, $INTERNATIONAL_POST_SMS);
    return $response;
}
function logStaticOfferCode($log_detail,$offercode,$offertypes,$session_data)
{
    $data = DB::connection('appdb')
            ->table('offers_static_codes')
            ->select('id','offer_code')
            ->where('blocked','=','false')
            ->where('offer_types','=', $offertypes)
            ->where('offer_id','=', $log_detail->id)
            ->where('member_no','=', $session_data['member_no'])
            ->where('offer_code','=', $offercode)
            ->orderBy('id','ASC')
            ->get();
    if(count($data) < 1)
    {
    $date     = date("Y-m-d H:i:s");
    $column   = array(
        "offer_id"      => $log_detail->id,
        "offer_code"    => $offercode,
        "offer_types"   => $offertypes,
        "member_no"     => $session_data['member_no'],
        "mobile"        => $session_data['mobile1'],
        "email"         => $session_data['email'],
        "offer_details" => $log_detail->offer_details,
        "offer_source"  => $log_detail->offer_source,
        "offer_type"    => $log_detail->offer_type,
        "offer_category"=> $log_detail->offer_category,
        "offer_brand"   => $log_detail->offer_brand,
        "offer_start_date"=> $log_detail->offer_start_date,
        "offer_end_date"  => $log_detail->offer_end_date,
        "created_by"      => $session_data['member_no'],
        "created_on"      => $date,
        "edited_on"       => $date
        );
    $is_iserted = DB::connection('appdb')
                    ->table('offers_static_codes')
                    ->insert($column);
    if($is_iserted)
    {
        return "new";
    }
    }
    return true;
}

function MemberInfoData($response,$mobile){
    if(isset($response['MemberDetails']['MemberDetails']) and isset($response['MemberDetails']['MemberDetails'])){
        $encrypted_data=$response['MemberDetails']['MemberDetails'];
        if($encrypted_data and count($encrypted_data)>0){
            $columns =array(
                "title"=>"Salutation",
                "first_name"=>"FirstName",
                "last_name"=>"LastName",
                "email"=>"EmailId",
                "dob"=>"DateOfBirth",
                "marital_status"=>"MaritalStatus",
                "doa"=>"DOA",
                "house_no"=>"HouseNo",
                "building_name"=>"BuildingName",
                "street_name"=>"StreetName",
                "colony"=>"Colony",
                "city"=>"City",
                "city_others"=>"cityOthers",
                "state"=>"State",
                "country"=>"Country",
                "token"=>"Token"

                
                );
            $final_data=[];
            foreach($encrypted_data as $row){
                $columnData=[];
                foreach($columns as $key=>$api_key){
                    if(isset($row[$api_key])) { 
                        $data=$row[$api_key]; 
                            $decry_data=$data;
                       
                        $columnData[$key] = $decry_data;
                    }
                }
                $final_data=$columnData;
             //   $_SESSION["SESS_USER_INFO"]=$final_data;
            }
            

            $final_data['mobile']=$mobile;
            if(count($final_data)>0)return $final_data;
            else return [];
        }
    }
    return [];
}


function decryptReferralData($response){
    if(isset($response['MemberReferralHistory']['MemberReferralHistory'])){
        $encrypted_acc_data=$response['MemberReferralHistory']['MemberReferralHistory'];
        if($encrypted_acc_data and count($encrypted_acc_data)>0){
            $columns =ApiParams::apiResponseReferralHistory();
            $final_data=[];
            foreach($encrypted_acc_data as $row){
                $columnData=[];
                foreach($columns as $key=>$api_key){
                    if(isset($row[$api_key])) { 
                        $data=$row[$api_key]; 
                        $decry_data=decryptData($data);
                        $columnData[$key] = $decry_data;
                    }
                }
                $final_data[]=$columnData;
            }
            // printArray([$final_data]);
            if(count($final_data)>0)return $final_data;
            else return false;
        }
        
    }
    return false;
}
function getDoList($list) {
    $logged_in_country  = "uae";
    $lis    = DB::connection('appdb')
                ->table('do_lists')
                ->where('groupid','=',$list)
                ->where('country','=', $logged_in_country)
                ->where('blocked','=','false')
                ->orderBy('id','ASC')
                ->get();
    return $lis;
}
function getDoListCms($list) {
    $lis    = DB::connection('appdb')
                ->table('do_lists')
                ->where('groupid','=',$list)
                ->where('country',Session::get('admin_country'))
                ->where('blocked','=','false')
                ->get();
    return $lis;
}
function API_URL() {
    if(Session::has('API_URL')){
        $API_URL=session('API_URL');
        return $API_URL;
       } else {
        $API_URL = $_ENV['API_URL'];
        return $API_URL;
       }
}
function API_CHANNEL_CODE() {
    if(Session::has('API_URL')){
        $CHANNEL_CODE = $_ENV['API_CHANNEL_CODE'];
       } else {
        $CHANNEL_CODE = $_ENV['API_CHANNEL_CODE_UAE'];
       }
       return $CHANNEL_CODE;
}
function decryptMemberEnrollData($response){

    $log_country=session('log_country',null);
    if(isset($response['MemberEnrollment']['CardNo'])){
        $encrypted_data=$response['MemberEnrollment']['CardNo'];
        if($log_country=='usa') {
        $decry_data=$encrypted_data;
        } else {
            $decry_data=decryptData($encrypted_data);

        }
        return $decry_data;
    }
    return false;
}
function decryptFamilyHistoryData($response){
    $log_country=session('log_country',null);
    if(isset($response['FamilyMembershipHistory']['FamilyMemberHistory'])){
        $encrypted_data=$response['FamilyMembershipHistory']['FamilyMemberHistory'];
        if($encrypted_data and count($encrypted_data)>0){
            $columns =ApiParams::apiResponseFamilyHistoryParams();
            $final_data=[];
            $i=0;
            foreach($encrypted_data as $row){
                $columnData=[];
                foreach($columns as $key=>$api_key){
                    if(isset($row[$api_key])) { 
                        $data=$row[$api_key]; 
                        if($log_country=='usa') {
                            $decry_data = $data;
                        } else {                        
                            $decry_data=decryptData($data);
                        }
                        $columnData[$key] = $decry_data;
                    }
                }
                $final_data[]=$columnData;
            }
            if(count($final_data)>0)return $final_data;
            else return false;
        }
        
    }
    return false;
}
function decryptMemberData($response){
    $log_country  = Session::get('log_country',null);

    if(isset($response['MemberCheck']["MemberCheck"]) && isset($response['MemberCheck']["MemberCheck"][0])){
        $encrypted_data=$response['MemberCheck']["MemberCheck"][0];
        if($encrypted_data and count($encrypted_data)>0){
            $columns =ApiParams::apiResponseMemberCheckInfoParams();
            $final_data=[];

            foreach($columns as $key=>$api_key){
                if(isset($encrypted_data[$api_key])) { 
                    $data=$encrypted_data[$api_key];
                    if($log_country=='usa')  {
                        $decry_data=$data;
                    } else {
                        $decry_data=decryptData($data);

                    }
                    
                    $final_data[$key] = $decry_data;
                }
            }
                
            // printArray([$final_data]);
            if(count($final_data)>0)return $final_data;
            else return false;
        }
        
    }
    if(isset($response["MemberCheck"]) && isset($response["MemberCheckInfo"][0])){
        $encrypted_data=$response["MemberCheckInfo"][0];
        if($encrypted_data and count($encrypted_data)>0){
            $columns =ApiParams::apiResponseMemberCheckInfoParams();
            $final_data=[];
            foreach($columns as $key=>$api_key){
                if(isset($encrypted_data[$api_key])) { 
                    $data=$encrypted_data[$api_key];
                    $decry_data=$data;
                    $final_data[$key] = $decry_data;
                }
            }
                
            // printArray([$final_data]);
            if(count($final_data)>0)return $final_data;
            else return false;
        }
        
    }
    return false;
}
function decryptMemberInfoEnroll($post_data) {
            $encrypted_data=$post_data;
            $columns =ApiParams::apiResponseMemberEnrollInfoParams();
            $dummy ="";
            foreach($columns as $key=>$api_key){
                if(isset($encrypted_data[$api_key])) { 
                    $data=$encrypted_data[$api_key]; 
                } else {
                    $data=encryptData($dummy);
                }
                $decry_data=decryptData($data);
                $final_data[$key] = $decry_data;
            }  
            // printArray([$final_data]);
            if(count($final_data)>0)return $final_data;
            else return false;
}

function getCity($city_id) {
    $logged_in_country  = (Session::get('API_URL',null)===$_ENV['API_URL_UAE']) ? "uae" : "india";
    $cities = DB::connection('appdb')
                ->table('do_lists')
                ->select('title', 'value') 
                ->where('groupid', '=', 'profile_city')
                ->where('country','=', $logged_in_country)
                ->where('value', $city_id) 
                ->where('blocked','=','false')
                ->get()
                ->toArray();
    if(count($cities) >= 1) {
        $city = $cities[0];
        return $city->title;
    }
    return null;
}

function isEmptyDate($day, $month, $year) {
    if(empty($day) || empty($month) || empty($year)) {
        return true;
    }
    return false;
}

function toTitle($val) {
return str_replace("_"," ","$val");

}

function getDateString($day, $month, $year) {
    if(isEmptyDate($day, $month, $year)) {
        return "";
    }
    try {
        $date = Carbon::parse("$day $month $year");
    } catch (\Exception $e) {
        return "";
    }
    /* return $date->format('d/m/Y'); */
    return $date->format('Y-m-d');
}

function getGallery($galleryID,$galleryDir=null) {
    if($galleryDir==null) {
        $galleryDir= public_path() . "/img/gallery/";//APPS_MEDIA_FOLDER.
    }
    
    $dir=$galleryDir.$galleryID."/thumbs/";
    $data=$galleryDir.$galleryID."/data/";
    $cfg=$galleryDir.$galleryID."/config.cfg";
    $sort=$galleryDir.$galleryID."/sort.lst";
    
    $cfgArr=ConfigFileReader::LoadCfgFile($cfg);
    if(!isset($cfgArr['CONFIG'])) $cfgArr['CONFIG']=[];
    
    if(file_exists($sort)) {
        $sort=file_get_contents($sort);
        if(strlen($sort)>1) {
            $sort=explode("\n",$sort);
        } else {
            $sort=[];
        }
    } else {
        $sort=[];
    }
    
    $gal=["title"=>toTitle($galleryID),"photos"=>[],"config"=>$cfgArr['CONFIG']];
    if(is_dir($dir)) {
        $fs=scandir($dir);
        $fs=array_splice($fs,2);
        
        $photos=[];
        foreach($fs as $f) {
            // dd(asset('img/gallery/banner-home/large/1605008745-4e9139bbf96886bf30810ed3fef7c068.jpg'));
            $fCFG=$data.current(explode(".",$f)).".txt";
            $fCFGArr=ConfigFileReader::LoadCfgFile($fCFG);
            $hid=md5($f);
            $photos[$f]=[
                                "fname"=>$f,
                                "hash"=>$hid,
                                "thumb"=>asset("img/gallery/$galleryID/thumbs/$f"),
                                "large"=> asset("img/gallery/$galleryID/large/$f"),
                                "props"=> array_merge(getDefaultPhotoParams(),$fCFGArr['CONFIG'])
                        ];
        }
        $finalPhotos=[];
        foreach($sort as $f) {
            if(isset($photos[$f])) {
                $finalPhotos[$f]=$photos[$f];
                unset($photos[$f]);
            }
        }
        foreach($photos as $f=>$p) {
            $finalPhotos[$f]=$p;
        }
        $gal['photos']=$finalPhotos;
    }
    
    return $gal;
}

function getDefaultPhotoParams() {
    return [
        "title"=>"",
        "label"=>"",
        "descs"=>"",
        "link"=>"",
        "visible"=>true
    ];
}
function get_notification($mobile,$log_country){

    if($log_country=='usa') {

        return false;

        $post_data =  array(
            "MobileNo"=>$mobile
            );

          /*   return false; */

    } else {
    $post_data =  array(
        "MobileNo"=>encryptData($mobile)
    ); 
}
    $API_URL = session('API_URL');
    if(Session::get('API_URL',null)===$_ENV['API_URL']) {
    $API_URL=$API_URL.'/GetNotificationAPI';
    $log_country='india';
    } else {
/*     $API_URL='https://ulpdemo2.netcarrots.in/MemberAPIKey/MemberAPI.svc/GetNotificationAPI';
 */    
$API_URL=$API_URL.'/GetNotificationAPI';


 $log_country=$log_country;

     }
  //  $response = Http::post($API_URL, $post_data);
    $log_url =$API_URL;


    try {
        $response = Http::post($API_URL, $post_data);
        ApiMethods::api_logs($response,$post_data,$log_url,$log_country);
       } catch (\Exception $e) {
        $response="";
        ApiMethods::api_logs($response,$post_data,$log_url,$log_country);
        return false;
       }
    $json = $response->getBody();
    $data = json_decode($json, true);
    if($log_country=='usa') {
        $errcode = $data['GetNotificationAPI']['ErrorCode'];

    } else {
    $errcode = decryptData($data['GetNotificationAPI']['ErrorCode']);
     }
    $encrypted_data=$response['GetNotificationAPI']['Notifications'];
    $final_data=[];
    if($encrypted_data) {
        $columns =array(
        'notification_id'=> 'NotificationId',
        'ref_mobile'=> 'ReferrerMobileNo',
        'ref_name'=> 'ReferrerName',
        'ref_channel'=> 'ReferredChannel',
        'ref_status'=> 'ReferralStatus',
        'ref_validity'=> 'ReferralValiditity',
        'remarks'=> 'Remarks',
        'status'=> 'Status',
        'date'=> 'NotificationDate',
        );
    
        foreach($encrypted_data as $row){
            $columnData=[];
            foreach($columns as $key=>$api_key){
                if(isset($row[$api_key])) { 
                    $data=$row[$api_key]; 
                    if($log_country=='usa') {

                        $decry_data=$data;
                    } else  {
                    $decry_data=decryptData($data);
                }
                    $columnData[$key] = $decry_data;
                }
            }
            if($columnData['status'] == "Unread")
            {
                $final_data[]=$columnData;
            }
        }
    }
    session(['SESSION_NOTIFICATION' => $final_data]);
    if(count($final_data) > 0)
    {
        session(['SESSION_NOTIFICATION_COUNT' => count($final_data)]);
    }
    
    // if(count($final_data)>0)
    // return $final_data;
    // else return [];   
}

function informt2AdminNoVouchercode($points){
    $admin_mobile=$_ENV['ADMIN_CONTACTS'];
    $ADMIN_EMAIL=$_ENV['ADMIN_EMAIL'];
    $admin_ary=explode(",",$admin_mobile);
    if(count($admin_ary)>0){
        foreach($admin_ary as $admin_mobile){
            $admin_mobile=trim($admin_mobile);
            
            if(strlen($admin_mobile) >1){
                $data=array(
                "points"=>$points,
                    );
                $referrer_smsbody  = view('sms.unavailable_vouchercode_admin_sms',compact(['points']))->render();
                $email_body  = view('email.unavailable_vouchercode_admin_email',compact(['points']))->render();
                
                
                $msg =  $referrer_smsbody;
                $to = $admin_mobile;
                $output=sendSMS($to,$msg);

                $to = $ADMIN_EMAIL;
                $subject="Vocher Code Unavailable";
                $msg = $email_body;
                $mail= SendMail($to,$subject,$msg);

                // if(strpos($output, $orderData["mobile"]) !== false){
                //   return true;
                // }
            }
        }
        
    }
    return true;
}

function getChannel($chanel) {
    $logged_in_country  = (Session::get('API_URL',null)===$_ENV['API_URL_UAE']) ? "uae" : "india";
    $lis  = DB::connection('appdb')
    ->table('do_lists')
    ->select('title', 'value') 
    ->where('groupid', '=', 'channel')
    ->where('country','=',$logged_in_country)
    ->where('value',$chanel) 
    ->where('blocked','=','false')
    ->get()
    ->toArray();
              
    if(count($lis) >= 1) {
        $lis = $lis[0];
        return $lis->title;
    }
    return null;
}
function is_purple($mobile){
    $logged_in_country  = Session::get('log_country',null);
// is purple
    $Purple_member=DB::connection('appdb')
    	->table('purple_club_users')
        ->where('country','=', $logged_in_country)
        ->where('mobile','=', $mobile)
        ->where('blocked','=', 'false')
    	->first();
        if($Purple_member==null) {
            session(['IS_PURPLE' => 'no']);

         } else {
              session(['IS_PURPLE' => 'yes']);

         }
// is triton
         $Purple_member=DB::connection('appdb')
         ->table('triton_club_users')
         ->where('country','=', $logged_in_country)
         ->where('mobile','=', $mobile)
         ->where('blocked','=', 'false')
         ->first();
         if($Purple_member==null) {
             session(['IS_TRITON' => 'no']);
 
          } else {
               session(['IS_TRITON' => 'yes']);
 
          }
}

function countries() {
    $country=DB::connection('appdb')
    ->table('countries')
    ->where('status','=', 'true')
    ->where('blocked','=', 'false')
    ->get();
    return $country;
}
function countries_details($country_value) {
    $country_details=DB::connection('appdb')
    ->table('countries')
    ->where('blocked','=', 'false')
    ->where('status','=', 'true')
    ->where('value','=', $country_value)
    ->first();
    return $country_details;
}
function WhatsappInfoData($mobile,$country_value,$Whatsapp,$ip) {
    $posts = TblwhatsappStatus::firstOrNew(['mobile' =>$mobile,'country' =>$country_value]);
    
    $posts->mobile = $mobile;
    $posts->status = $Whatsapp;
    if ($posts->exists) {
        // user already exists and was pulled from database.
        $posts->edited_on = date("Y-m-d H:i:s");
    } else {
        // user created from 'new'; does not exist in database.
        $posts->created_on = date("Y-m-d H:i:s");
    }
    $posts->blocked = "false";
    $posts->country = $country_value;
    $posts->ip = $ip;
    $posts->save();

}


function decryptMemberInfoData($response,$log_country){
    if(isset($response['ValidateOTP']['MemberDetails']) and isset($response['ValidateOTP']['MemberDetails'])){
        $encrypted_data=$response['ValidateOTP']['MemberDetails'];
        if($encrypted_data and count($encrypted_data)>0){
            $columns =array(
                "member_no"=>"ULPNo",
                "full_name"=>"MemberName",
                "member_since"=>"MemberSince",
                "balance"=>"Balance",
                "member_tier"=>"MemberTier",
                "title"=>"Title",
                "first_name"=>"FirstName",
                "last_name"=>"Lastname",
                "landline"=>"LandLine",
                "mobile1"=>"Mobile1",
                "mobile2"=>"Mobile2",
                "email"=>"EmailId",
                "houseno"=>"HouseNo",
                "building_name"=>"BuildingName",
                "street_road"=>"StreetRoad",
                "colony"=>"Colony",
                "expiry_date"=>"ExpiryDate",
                "city"=>"City",
                "city_other"=>"CityOther",
                "pin_code"=>"PinCode",
                "dob_day"=>"DOBDay",
                "dob_month"=>"DayMonth",
                "dob_year"=>"DOBYear",
                "marital_status"=>"MaritalStatus",
                "spouse_name"=>"SpouseName",
                "spouse_dobday"=>"SpouseDOBDay",
                "spouse_dobmonth"=>"SpouseDOBMonth",
                "spouse_dobyear"=>"SpouseDOBYear",
                "anniversary_day"=>"AnniversaryDay",
                "anniversary_month"=>"AnniversaryMonth",
                "anniversary_year"=>"AnniversaryYear",
                "relationship"=>"Relationship",
                "gender"=>"Gender",
                "mothertongue"=>"MotherTongue",
                "age_group"=>"AgeGroup",
                "have_children"=>"HaveChildren",
                "child1"=>"Child1",
                "child1_name"=>"Child1Name",
                "child1_dob_day"=>"Child1DOBDay",
                "child1_dob_month"=>"Child1DOBMonth",
                "child1_dob_year"=>"Child1DOBYear",
                "child2"=>"Child2",
                "child2_name"=>"Child2Name",
                "child2_dob_day"=>"Child2DOBDay",
                "child2_dob_month"=>"Child2DOBMonth",
                "child2_dob_year"=>"Child2DOBYear",
                "profession"=>"Profession",
                "profession_oth"=>"Profession_OTH",
                "public_private_sector"=>"PublicPrivateSector",
                "who_wear_spectacles"=>"WhoWearSpectacles",
                "self"=>"Self",
                "spouse"=>"Spouse",
                "parents"=>"Parents",
                "children"=>"Children",
                "celebreate"=>"celebreate",
                "celebreate_oth"=>"celebreate_OTH",
                "adventure"=>"Adventure",
                "cricket"=>"Cricket",
                "carmotors"=>"CarMotors",
                "football"=>"Football",
                "swimming"=>"Swimming",
                "tennis"=>"Tennis",
                "water"=>"Water",
                "golf"=>"Golf",
                "sports_others"=>"SportsOthers",
                "art"=>"Art",
                "cinema"=>"Cinema",
                "music"=>"Music",
                "culinary"=>"Culinary",
                "reading"=>"Reading",
                "photography"=>"Photography",
                "social_networking"=>"SocialNetworking",
                "theatre"=>"Theatre",
                "yoga"=>"Yoga",
                "healthwellness"=>"HealthWellness",
                "hobbies_others"=>"HobbiesOthers",
                "token"=>"Token"
                );
            $final_data=[];
            foreach($encrypted_data as $row){
                $columnData=[];
                foreach($columns as $key=>$api_key){
                    if(isset($row[$api_key])) { 
                        $data=$row[$api_key]; 
                       
                        if($log_country=='usa') {

                            $decry_data=$data;
                        } else {

                            $decry_data=decryptData($data);
                        }
                        $columnData[$key] = $decry_data;
                    }
                }
                $final_data=$columnData;
               // $_SESSION["SESS_USER_INFO"]=$final_data;
            }
            // printArray([$final_data]);
            if(count($final_data)>0)return $final_data;
            else return [];
        }
    }
    return [];
}









 function for_you_only_eligibility($member_info,$ip,$date,$log_country,$login_type) {


    // is Brides N Grooms
    $brids_member=DB::connection('appdb')
    	->table('offers_token')
        ->where('country','=', $log_country)
        ->where('allocated_member','=',$member_info['member_no'])
        ->where('blocked','=', 'false')
    	->first();
        if($brids_member==null) {
            session(['IS_BRIDES_N_GROOM' => 'no']);

         } else {
              session(['IS_BRIDES_N_GROOM' => 'yes']);

         }

// is heliosanniversary
    $helios_member=DB::connection('appdb')
    ->table('helios_offers_tokens')
    ->where('country','=', $log_country)
    ->where('allocated_member','=',$member_info['member_no'])
    ->where('blocked','=', 'false')
    ->first();
    if($helios_member==null) {
        session(['IS_HELIOS' => 'no']);

     } else {
          session(['IS_HELIOS' => 'yes']);

     }



}
function sign_up($mobile,$log_country) {
    $mobile="+".$mobile;

    $log_url =$_ENV['one_trust_api_sign_up_url'];
    $post_data=[

        "identifier"=>$mobile,
        "requestInformation"=>$_ENV['sign_up_request_Info'],

        "purposes"=> [
            [
              "Id"=> $_ENV['P1'],
              "TransactionType"=> "CONFIRMED"
        ]
          ]
    ];


 try {
    $response = Http::post($log_url, $post_data);
    ApiMethods::api_logs($response,$post_data,$log_url,$log_country);
   } catch (\Exception $e) {
    $response="";
    ApiMethods::api_logs($response,$post_data,$log_url,$log_country);
    return false;
   }

}
function getcountry() {


    env('LOCATION_TESTING')==true ?  $ip =env('TEST_IP') : $ip = getIp();

    

    ($position = Location::get($ip)) ? $log_country = $position->countryCode : $log_country='IND';
    $logged_in_country = ($log_country == 'IN') ? 'india' :(($log_country == 'AE') ? 'uae' : (($log_country == 'US') ? 'usa' : 'india'));

    return $logged_in_country;



}
function iplocation() {




   /*  logged_in_country_user */

   if(Session::has('logged_in_country_user')){

    $logged_in_country=session('logged_in_country_user')??null;

    Session::put('logged_in_country', $logged_in_country);

    if($logged_in_country=='india') {
       return "India (EN)";
    } else if($logged_in_country=='uae') {

       return "UAE (AE)";
    } else if($logged_in_country=='usa') {

       return "USA (US)";
    }



   }

else {


     Session::put('logged_in_country', 'uae');


     return "UAE (AE)";



//    env('LOCATION_TESTING')==true ?  $ip =env('TEST_IP') : $ip = request()->ip(); 

   /* below code just commented for now for slow website */
   // env('LOCATION_TESTING')==true ?  $ip =env('TEST_IP') : $ip = getIp();

    

    ($position = Location::get($ip)) ? $log_country = $position->countryCode : $log_country='IND';
    $logged_in_country = ($log_country == 'IN') ? 'india' :(($log_country == 'AE') ? 'uae' : (($log_country == 'US') ? 'usa' : 'india'));

    Session::put('logged_in_country', $logged_in_country);

     if($logged_in_country=='india') {
        return "India (EN)";
     } else if($logged_in_country=='uae') {

        return "UAE (AE)";
     } else if($logged_in_country=='usa') {

        return "USA (US)";
     }

    } 
}

 function getIp(){
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
        if (array_key_exists($key, $_SERVER) === true){
            foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                    return $ip;
                }
            }
        }
    }
}

function loginlogs($card_number) {

$Data=session('SESS_USER_INFO');
$login_type = session('login_type');




if (!empty($Data['doa'])) {
    $doa =$Data['doa'];
    $doa = date("Y-m-d", strtotime($doa));
  } else {

    $doa=null;
  }

if (!empty($Data['dob'])) {
    $dob =$Data['dob'];
    $dob = date("Y-m-d", strtotime($doa));
  } else {

    $dob=null;
  }

$ip = getIp();

$logged_in_country=session('logged_in_country');

if($logged_in_country=='uae'){
$country="uae";
$points_credited=100;

$logged_in_url=session('logged_in_url');
$brand="tanishq";

} else {

    $country="usa";  
    $points_credited=50;
    $logged_in_url=session('logged_in_url');
    $brand="tanishq";
}


// check record exists

$member = DB::connection('appdb')
->table('profiletbl')
->where('member_no',$card_number)
->where('country',$country)
->first();

if ($member === null) {
    // user doesn't exist

    $member_details_new = [
        'member_no'   => $card_number,
        'first_name'   => $Data['first_name'],
        'last_name'   =>$Data['last_name'],
        'email'  =>$Data['email'],
        'mobile'  =>$Data['mobile'],
        'marital_status'  =>$Data['marital_status'],
        'doa'  =>$doa,
        'dob'  =>$dob,
        'house_no'  =>$Data['house_no'],
        'building_name'  =>$Data['building_name'],
        'street_name'  =>$Data['street_name'],
        'colony'  =>$Data['colony'],
        'city'=>$Data['city'],
        'city_others'  =>$Data['city_others'],
        'state'  =>$Data['state'],
        'points_credited'  =>$points_credited,
        'logged_in_url'  =>$logged_in_url,
        'brand'  =>$brand,
        'api_country'  =>$Data['country'],
        'login_type'  =>$login_type,
        'client_ip'  =>$ip,
        'blocked'  =>'false',
        'created_by'  =>$card_number,
        'created_on'  =>date('Y-m-d H:i:s'),
        'country'  =>$country,
        'address'=>$Data['address'],
        'zipcode'=>$Data['zipcode']
        ];
        $is_inserted = DB::connection('appdb')
        ->table('profiletbl')
        ->insert($member_details_new);

 } else {

    $member_details_ex = [
        'first_name'   => $Data['first_name'],
        'last_name'   =>$Data['last_name'],
        'email'  =>$Data['email'],
        'mobile'  =>$Data['mobile'],
        'marital_status'  =>$Data['marital_status'],
        'doa'  =>$doa,
        'dob'  =>$dob,
        'house_no'  =>$Data['house_no'],
        'building_name'  =>$Data['building_name'],
        'street_name'  =>$Data['street_name'],
        'colony'  =>$Data['colony'],
        'city'=>$Data['city'],
        'city_others'  =>$Data['city_others'],
        'state'  =>$Data['state'],
        'points_credited'  =>$points_credited,
        'logged_in_url'  =>$logged_in_url,
        'brand'  =>$brand,
        'api_country'  =>$Data['country'],
        'client_ip'  =>$ip,
        'blocked'  =>'false',
        'edited_by'  =>$card_number,
        'edited_on'  =>date('Y-m-d H:i:s'),
        'country'  =>$country,
        'address'=>$Data['address'],
        'zipcode'=>$Data['zipcode']
        ];
        $is_inserted = DB::connection('appdb')
        ->table('profiletbl')
        ->where('member_no',$card_number)
        ->where('country',$country)
        ->update($member_details_ex);
 }


}


function loginlogswatches($member_info) {



    $card_number = $member_info['member_no'];


    $Data=session('SESS_USER_INFO');
    $login_type = session('login_type');
    
    
    
    
    if (!empty($Data['doa'])) {
        $doa =$Data['doa'];
        $doa = date("Y-m-d", strtotime($doa));
      } else {
    
        $doa=null;
      }
    
    if (!empty($Data['dob'])) {
        $dob =$Data['dob'];
        $dob = date("Y-m-d", strtotime($doa));
      } else {
    
        $dob=null;
      }
    
    $ip = getIp();
    
    $logged_in_country=session('logged_in_country');
    
    if($logged_in_country=='uae'){
    $country="uae";
    $points_credited=200;
    
    $logged_in_url=session('logged_in_url');
    $brand="Titan";
    
    } else {
    
        $country="usa";  
        $points_credited=50;
        $logged_in_url=session('logged_in_url');
        $brand="titan";
    }
    
    
    // check record exists
    
    $member = DB::connection('appdb')
    ->table('profiletbl_watches')
    ->where('member_no',$card_number)
    ->where('country',$country)
    ->first();


    
    
    if ($member === null) {
        // user doesn't exist
    
        $member_details_new = [
            'member_no'   => $card_number,
            'first_name'   => $Data['first_name'],
            'last_name'   =>$Data['last_name'],
            'email'  =>$Data['email'],
            'mobile'  =>$Data['mobile1'],
            'marital_status'  =>$Data['marital_status'],
            'doa'  =>$doa,
            'dob'  =>$dob,
            'house_no'  =>$Data['houseno'],
            'building_name'  =>$Data['building_name'],
            'street_name'  =>$Data['street_name'],
            'colony'  =>$Data['colony'],
            'city_others'  =>$Data['city_others'],
            'state'  =>$Data['state'],
            'points_credited'  =>$points_credited,
            'logged_in_url'  =>$logged_in_url,
            'brand'  =>$brand,
            'api_country'  =>$Data['country'],
            'login_type'  =>$login_type,
            'client_ip'  =>$ip,
            'blocked'  =>'false',
            'created_by'  =>$card_number,
            'created_on'  =>date('Y-m-d H:i:s'),
            'country'  =>$country
            ];
            $is_inserted = DB::connection('appdb')
            ->table('profiletbl_watches')
            ->insert($member_details_new);
    
     } else {
    
        $member_details_ex = [
            'first_name'   => $Data['first_name'],
            'last_name'   =>$Data['last_name'],
            'email'  =>$Data['email'],
            'mobile'  =>$Data['mobile1'],
            'marital_status'  =>$Data['marital_status'],
            'doa'  =>$doa,
            'dob'  =>$dob,
            'house_no'  =>$Data['houseno'],
            'building_name'  =>$Data['building_name'],
            'street_name'  =>$Data['street_name'],
            'colony'  =>$Data['colony'],
            'city_others'  =>$Data['city_others'],
            'state'  =>$Data['state'],
            'points_credited'  =>$points_credited,
            'logged_in_url'  =>$logged_in_url,
            'brand'  =>$brand,
            'api_country'  =>$Data['country'],
            'client_ip'  =>$ip,
            'blocked'  =>'false',
            'edited_by'  =>$card_number,
            'edited_on'  =>date('Y-m-d H:i:s'),
            'country'  =>$country
            ];
            $is_inserted = DB::connection('appdb')
            ->table('profiletbl_watches')
            ->where('member_no',$card_number)
            ->where('country',$country)
            ->update($member_details_ex);
     }
    
    
    }