<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactModel extends Model
{
    use HasFactory;
    protected $connection = 'appdb';
    public $timestamps = false;
    public $table = "contactus_tbl";
	protected $primaryKey = 'id';

}
