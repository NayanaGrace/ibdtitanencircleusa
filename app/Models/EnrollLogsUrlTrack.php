<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EnrollLogsUrlTrack extends Model
{
    use HasFactory;

    protected $table = 'logdb.enroll_logs_url_tracks';
}
