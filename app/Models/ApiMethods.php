<?php

namespace App\Models;

use Illuminate\Support\Facades\Http;

use Illuminate\Http\Request;

use App\Models\ProfileModel as ProModel;
use Carbon\Carbon;

use Cookie;

use App\Models\ApiMessage;

use App\Models\ApiParams;

use App\Models\ProfileModel;

use Session;

use phpseclib3\Crypt\RSA;

use DB;



use phpseclib3\Crypt\PublicKeyLoader;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApiMethods extends Model
{
  public static function updateMemberInfoApi($post_data) {
      $API_URL =API_URL();
      $url=$API_URL."/UpdateProfile";
      $response = Http::post($url, $post_data);
      $json = $response->getBody();
      $data = json_decode($json, true); 
      $errcode = decryptData($data['UpdateProfile']['ErrorCode']);
      $error = ApiMessage::$messages[decryptData($data['UpdateProfile']['ErrorCode'])];
      if($errcode==0) {
        $error = "success";
        return ApiParams::printResponse($error,"success");
     } else {
        $error = ApiMessage::$messages[decryptData($data['UpdateProfile']['ErrorCode'])];
        return ApiParams::printResponse($error,"error");
     }

  }

  public static function getValueFromSelector($apiValue,$group){
   $master=getDoList($group);
   // foreach($master as $key=>$val){
       if(count($master)>0){
           foreach($master as $row){
               // $val=$row["value"];
               $title=trim($row->title);
               if(trim($apiValue) == $title){
                   $val=$row->value;
                   return $val;
               }
               
           }
       }
   // }
   return "";
}
  public static function decryptFamilyHistoryData($response){
        if(isset($response['FamilyMembershipHistory']['FamilyMemberHistory'])){
            $encrypted_data=$response['FamilyMembershipHistory']['FamilyMemberHistory'];
            if($encrypted_data and count($encrypted_data)>0){
                $columns =apiResponseFamilyHistoryParams();
                $final_data=[];
                $i=0;
                foreach($encrypted_data as $row){
                    $columnData=[];
                    foreach($columns as $key=>$api_key){
                        if(isset($row[$api_key])) { 
                            $data=$row[$api_key]; 
                            $decry_data=decryptData($data);
                            $columnData[$key] = $decry_data;
                        }
                    }
                    $final_data[]=$columnData;
                }
                if(count($final_data)>0)return $final_data;
                else return false;
            }
            
        }
        return false;
    }
    public static function logsession_api($member_info,$ip,$date,$log_country,$login_type) {


        if (Session::has('SESS_URL_INFO')){

            $url = Session::get('SESS_URL_INFO',null);
            $url = trim($url);
            $campaign  = DB::connection('appdb')
            ->table('tbl_campaign as a')
            ->where('a.country',$log_country)
            ->where('a.blocked','false')
            ->where('a.url',$url)
            ->first();
                  
            if(empty($member_info['gender'])) {
                $gender ='male';
              } else {
                $gender =$member_info['gender'];

              }
            $campgain_details = [
                'member_no' => $member_info['member_no'],
                'created_by' => $member_info['member_no'],
                'full_name' => $member_info['full_name'],
                'email' => $member_info['email'],
                'mobile' => $member_info['mobile1'],
                'login_type' => $login_type,
                'last_login' => $date,
                'created_on' => $date,
                'edited_on' => $date,
                'blocked' => "false",
                'blacklist' => "false",
                'privilegeid' => 1,
                'access_rule' => "Yes",
                'campaign' => $campaign->id,
                'access_level' => 1,
                'country' => $log_country,
                'gender' =>  $gender,
                'client_ip' => $ip
            ];
            $is_inserted = DB::connection('appdb')
                         ->table('tbl_campaign_activity')
                         ->insert($campgain_details);

            if($is_inserted)
            {
              //  Session::forget('SESS_URL_INFO');        
                        
            }

        }
        else {


          $posts = ProfileModel::firstOrNew(['loginid' =>$member_info['member_no']]);
          $posts->member_no = $member_info['member_no'];
          $posts->created_by = $member_info['member_no'];
          $posts->full_name = $member_info['full_name'];
          $posts->email = $member_info['email'];
          $posts->mobile = $member_info['mobile1'];
          $posts->login_type =$login_type ;
          $posts->last_login = $date;
          $posts->created_on = $date;
          $posts->edited_on = $date;
          $posts->blocked = "false";
          $posts->blacklist = "false";
          $posts->privilegeid = 1;
          $posts->access_rule = "Yes";
          $posts->access_level = 1;
          $posts->country = $log_country;
        //  $posts->gender = $member_info['gender'];
          if(empty($member_info['gender'])) {
            $posts->gender ='male';
          }
          $posts->client_ip = $ip;
          $posts->save();
}


   }

    public static function api_logs($response,$post_data,$log_url,$log_country) {
    $date = Carbon::now();
    $date = $date->toDateTimeString();
    $logapi_details = [
        'guid'          => 'global',
        'groupuid'      => 'hq',
        'method'   => 'POST',
        'base_url'          =>$log_url,
        'params'       => '',
        'payload'    => json_encode($post_data),
        'status'  => 'success',
        'created_by'     => 'API',
        'created_on'          => $date,
        'edited_by'    => 'API',
        'edited_on'       => $date, 
        'country'       => $log_country
    ];
  
     if(!empty($response)) {
        $logapi_details['status']='success';
        $logapi_details['response']  = $response;
       
    } else {
        $logapi_details['status']='fail';
        $logapi_details['response']  = $response;
    }
 //   dd($logapi_details);
    /* $is_iserted = \DB::connection('logdb')
                 ->table('log_apicalls')
                 ->insert($logapi_details); */
}
}
