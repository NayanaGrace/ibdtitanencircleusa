<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApiParamsUae extends Model
{
    public static function updateMemberParams(){
        //formelement => api key
            return array(
                "member_no"=>"MembershipNo",
                "title"=>"TitleCode",
                "first_name"=>"FirstName",
                "last_name"=>"Lastname",
                "mobile1"=>"LandLine",
                "email"=>"EmailId",
                "houseno"=>"HouseNo",
                "building_name"=>"BuildingName",
                "street_road"=>"StreetRoad",
                "colony"=>"ColonySociety",
                "city"=>"City",
                // "city_other"=>"CityOther",
                "pin_code"=>"PinCode",
                "dob_day"=>"DOBDay",
                "dob_month"=>"DOBMonth",
                "dob_year"=>"DOBYear",
                "marital_status"=>"MaritalStatus",
                "spouse_name"=>"SpouseName",
                "spouse_dobday"=>"SpouseDOBDay",
                "spouse_dobmonth"=>"SpouseDOBMonth",
                "spouse_dobyear"=>"SpouseDOBYear",
                "anniversary_day"=>"AnniversaryDay",
                "anniversary_month"=>"AnniversaryMonth",
                "anniversary_year"=>"AnniversaryYear",
                "gender"=>"Gender",
                "mothertongue"=>"MotherTongue",
                "age_group"=>"AgeGroup",
                "have_children"=>"DoYouhaveChildren",
                "child1"=>"Child1",
                "child1_name"=>"Child1Name",
                "child1_dob_day"=>"Child1DOBDay",
                "child1_dob_month"=>"Child1DOBMonth",
                "child1_dob_year"=>"Child1DOBYear",
                "child2"=>"Child2",
                "child2_name"=>"Child2Name",
                "child2_dob_day"=>"Child2DOBDay",
                "child2_dob_month"=>"Child2DOBMonth",
                "child2_dob_year"=>"Child2DOBYear",
                "profession"=>"Profession",
                "profession_oth"=>"ProfessionOther",
                "public_private_sector"=>"WhereDoYouWork",
                "who_wear_spectacles"=>"WhoWearSpectacles",
                "self"=>"Self",
                "spouse"=>"Spouse",
                "parents"=>"Parents",
                "children"=>"Children",
                "celebreate"=>"FestivalsICelebrate",
                // "celebreate_other"=>"CelebreateOther",
                "adventure"=>"Adventure",
                "cricket"=>"Cricket",
                "carmotors"=>"CarMotors",
                "football"=>"Football",
                "swimming"=>"Swimming",
                "tennis"=>"Tennis",
                "water"=>"Water",
                "golf"=>"Golf",
                "sports_others"=>"SportsOthers",
                "art"=>"Art",
                "cinema"=>"Cinema",
                "music"=>"Music",
                "culinary"=>"Culinary",
                "reading"=>"Reading",
                "photography"=>"Photography",
                "social_networking"=>"SocialNetworking",
                "theatre"=>"Theatre",
                "yoga"=>"Yoga",
                "healthwellness"=>"HealthWellness",
                "hobbies_others"=>"HobbiesOthers"
                );
        }
    
    public static function apiResponseMemberInfoParams(){
    //formelement => api key
        return array(
            "member_no"=>"ULPNo",
            "full_name"=>"MemberName",
            "member_since"=>"MemberSince",
            "balance"=>"Balance",
            "member_tier"=>"MemberTier",
            "title"=>"Title",
            "first_name"=>"FirstName",
            "last_name"=>"Lastname",
            "landline"=>"LandLine",
            "mobile1"=>"Mobile1",
            "mobile2"=>"Mobile2",
            "email"=>"EmailId",
            "houseno"=>"HouseNo",
            "building_name"=>"BuildingName",
            "street_road"=>"StreetRoad",
            "colony"=>"Colony",
            "expiry_date"=>"ExpiryDate",
            "city"=>"City",
            "city_other"=>"CityOther",
            "pin_code"=>"PinCode",
            "dob_day"=>"DOBDay",
            "dob_month"=>"DayMonth",
            "dob_year"=>"DOBYear",
            "marital_status"=>"MaritalStatus",
            "spouse_name"=>"SpouseName",
            "spouse_dobday"=>"SpouseDOBDay",
            "spouse_dobmonth"=>"SpouseDOBMonth",
            "spouse_dobyear"=>"SpouseDOBYear",
            "anniversary_day"=>"AnniversaryDay",
            "anniversary_month"=>"AnniversaryMonth",
            "anniversary_year"=>"AnniversaryYear",
            "relationship"=>"Relationship",
            "gender"=>"Gender",
            "mothertongue"=>"MotherTongue",
            "age_group"=>"AgeGroup",
            "have_children"=>"HaveChildren",
            "child1"=>"Child1",
            "child1_name"=>"Child1Name",
            "child1_dob_day"=>"Child1DOBDay",
            "child1_dob_month"=>"Child1DOBMonth",
            "child1_dob_year"=>"Child1DOBYear",
            "child2"=>"Child2",
            "child2_name"=>"Child2Name",
            "child2_dob_day"=>"Child2DOBDay",
            "child2_dob_month"=>"Child2DOBMonth",
            "child2_dob_year"=>"Child2DOBYear",
            "profession"=>"Profession",
            "profession_oth"=>"Profession_OTH",
            "public_private_sector"=>"PublicPrivateSector",
            "who_wear_spectacles"=>"WhoWearSpectacles",
            "self"=>"Self",
            "spouse"=>"Spouse",
            "parents"=>"Parents",
            "children"=>"Children",
            "celebreate"=>"celebreate",
            "celebreate_oth"=>"celebreate_OTH",
            "adventure"=>"Adventure",
            "cricket"=>"Cricket",
            "carmotors"=>"CarMotors",
            "football"=>"Football",
            "swimming"=>"Swimming",
            "tennis"=>"Tennis",
            "water"=>"Water",
            "golf"=>"Golf",
            "sports_others"=>"SportsOthers",
            "art"=>"Art",
            "cinema"=>"Cinema",
            "music"=>"Music",
            "culinary"=>"Culinary",
            "reading"=>"Reading",
            "photography"=>"Photography",
            "social_networking"=>"SocialNetworking",
            "theatre"=>"Theatre",
            "yoga"=>"Yoga",
            "healthwellness"=>"HealthWellness",
            "hobbies_others"=>"HobbiesOthers"
            );
    }
    
    // public static function updateMemberInfoParams(){
    // //formelement => api key
    //     return array(
    //         "title"=>"Title",
    //         "first_name"=>"FirstName",
    //         "last_name"=>"Lastname",
    //         "landline"=>"LandLine",
    //         "mobile1"=>"Mobile1",
    //         "mobile2"=>"Mobile2",
    //         "email"=>"EmailId",
    //         "houseno"=>"HouseNo",
    //         "building_name"=>"BuildingName",
    //         "street_road"=>"StreetRoad",
    //         "colony"=>"Colony",
    //         "expiry_date"=>"ExpiryDate",
    //         "city"=>"City",
    //         "city_other"=>"CityOther",
    //         "pin_code"=>"PinCode",
    //         "dob_day"=>"DOBDay",
    //         "dob_month"=>"DayMonth",
    //         "dob_year"=>"DOBYear",
    //         "marital_status"=>"MaritalStatus",
    //         "spouse_name"=>"SpouseName",
    //         "spouse_dobday"=>"SpouseDOBDay",
    //         "spouse_dobmonth"=>"SpouseDOBMonth",
    //         "spouse_dobyear"=>"SpouseDOBYear",
    //         "anniversary_day"=>"AnniversaryDay",
    //         "anniversary_month"=>"AnniversaryMonth",
    //         "anniversary_year"=>"AnniversaryYear",
    //         "relationship"=>"Relationship",
    //         "gender"=>"Gender",
    //         "mothertongue"=>"MotherTongue",
    //         "age_group"=>"AgeGroup",
    //         "have_children"=>"HaveChildren",
    //         "child1"=>"Child1",
    //         "child1_name"=>"Child1Name",
    //         "child1_dob_day"=>"Child1DOBDay",
    //         "child1_dob_month"=>"Child1DOBMonth",
    //         "child1_dob_year"=>"Child1DOBYear",
    //         "child2"=>"Child2",
    //         "child2_name"=>"Child2Name",
    //         "child2_dob_day"=>"Child2DOBDay",
    //         "child2_dob_month"=>"Child2DOBMonth",
    //         "child2_dob_year"=>"Child2DOBYear",
    //         "profession"=>"Profession",
    //         "profession_oth"=>"Profession_OTH",
    //         "public_private_sector"=>"PublicPrivateSector",
    //         "who_wear_spectacles"=>"WhoWearSpectacles",
    //         "self"=>"Self",
    //         "spouse"=>"Spouse",
    //         "parents"=>"Parents",
    //         "children"=>"Children",
    //         "celebreate"=>"celebreate",
    //         "celebreate_oth"=>"celebreate_OTH",
    //         "adventure"=>"Adventure",
    //         "cricket"=>"Cricket",
    //         "carmotors"=>"CarMotors",
    //         "football"=>"Football",
    //         "swimming"=>"Swimming",
    //         "tennis"=>"Tennis",
    //         "water"=>"Water",
    //         "golf"=>"Golf",
    //         "sports_others"=>"SportsOthers",
    //         "art"=>"Art",
    //         "cinema"=>"Cinema",
    //         "music"=>"Music",
    //         "culinary"=>"Culinary",
    //         "reading"=>"Reading",
    //         "photography"=>"Photography",
    //         "social_networking"=>"SocialNetworking",
    //         "theatre"=>"Theatre",
    //         "yoga"=>"Yoga",
    //         "healthwellness"=>"HealthWellness",
    //         "hobbies_others"=>"HobbiesOthers"
    //         );
    // }
    
    public static function apiRequestMemberCheckParams(){
    //formelement => api key
        return array(
            "channelcode"=>"ChannelCode",
            "outletcode"=>"OutletCode",
            "member_no"=>"ULPNo",
            "mobile"=>"MobileNo",
            "email"=>"EmailId"
            );
    }
    public static function apiResponseAccountsParams(){
    //formelement => api key
        return array(
            "invoice_date"=>"InvoiceDate",
            "channel"=>"Channel",
            "store"=>"Store",
            "invoice_no"=>"InvoiceNo",
            "earned_points"=>"PointsEarned",
            "redeemed_points"=>"PointsRedeemed",
            "shoppint_amt"=>"ShoppingAmt",
            "remarks"=>"Remarks"
            );
    }
    
    public static function defaultAccountsParams(){
    //formelement => api key
        return array(
            "membershipnumber"=>"MembershipNo",
            "month"=>"Month",
            );
    }
    
    public static function defaultContactParams(){
    //formelement => api key
        return array(
            "member_no"=>"MembershipNo",
            "email"=>"EmailId",
            "mobile"=>"MobileNo",
            "city"=>"City", //no required from form
            "subject"=>"TypeOfQuery",
            "message"=>"Query"
            );
    }
    
    public static function defaultEmailParams(){
    //formelement => api key
        return array(
            "membershipnumber"=>"MembershipNo",
            "chk1"=>"Chk1",
            "chk2"=>"Chk2",
            "chk3"=>"Chk3",
            "chk4"=>"Chk4",
            "emailtype"=>"EmailType",
            "suggestedfeedbacksurveycomm"=>"SuggetionFeedbackSurveyComm"
            );
    }
    public static function apiRequestGifPointsParams(){
    //formelement => api key
        return array(
        "member_no"=>"SourceMembershipNo",
        "mobile"=>"MobileNo",
        "firstname"=>"FirstName",
        "lastname"=>"LastName",
        "email"=>"EmailId",
        "gender"=>"Gender",
        "giftpoints"=>"Points",
        "ismember"=>"IsMember",
        "occasion"=>"GiftingOccasion",
        "otp"=>"OTP"
        );
    }
    public static function apiRequestActivateVoucherParams(){
    //formelement => api key
        return array(
            "name"=>"Name",
            "mobile"=>"MobileNo",
            "email"=>"EmailId",
            "vouchercode"=>"GVCode",
            "dob"=>"DOB",
            "gender"=>"Gender",
            "otp"=>"OTP"
            );
    }
    
    public static function apiResponseReferralHistory(){
        //formelement => api key
        return array (
            "referre_name"=>"ReferreName",
            "referre_mobile"=>"ReferreMobile",
            "referre_email"=>"ReferreEmailId",
            "referre_dob"=>"ReferreDOB",
            "source"=>"Source",
            "channel"=>"Channel",
            "referre_member_no"=>"ReferrerCardNo",
            "referral_date"=>"ReferralDate"
        );
        
    }
    
    public static function apiRequestReferralHistoryParams(){
        return array(
        "membershipnumber"=>"CardNo",
        "from_date"=>"FromDate",
        "to_date"=>"ToDate",
        "referredmobile"=>"MobileNo",
        "referredname"=>"Name",
        "referralstatus"=>"Status"
        );
    }

    public static function giftingPointsParams(){
    //formelement => api key
        return array(
            "member_no"=>"SourceMembershipNo",
            // "outletcode"=>"OutletCode",
            "mobile"=>"MobileNo",
            "email"=>"EmailId",
            "name"=>"FirstName",
            "lastname"=>"LastName",
            "gender"=>"Gender",
            "points"=>"Points",
            "ismember"=>"IsMember",
            "ismember"=>"GiftingOccasion",
            "ismember"=>"OTP"
            );
    }
    
    public static function referMemberParams(){
    //formelement => api key
        return array(
            "channelcode"=>"ChannelCode",
            "outletcode"=>"OutletCode",
            "member_no"=>"ReferrerCardNo",
            "name"=>"ReferreName",
            "mobile"=>"ReferreMobile",
            "email"=>"ReferreEmailId",
            "dob"=>"ReferreDOB"
            );
    }
    
    public static function apiRequestMemberVerificationParams(){
    //formelement => api key
        return array(
            "mobile"=>"MobileNo",
            "email"=>"EmailID"
            );
    }
    public static function apiRequestMemberEnrollmentParams(){
    //formelement => api key
        /* return array(
            "title"=>"Salutation",
            "first_name"=>"FirstName",
            "last_name"=>"LastName",
            "channelcode"=>"ChannelCode",
            "mobile1"=>"MobileNo",
            "email"=>"EmailID",
            "dob_day"=>"DOBDay",
            "dob_month"=>"DOBMonth",
            "dob_year"=>"DOBYear",
            "marital_status"=>"MaritalStatus",
            "spouse_name"=>"SpouseName",
            "spouse_dobday"=>"SDOBDay",
            "spouse_dobmonth"=>"SDOBMonth",
            "spouse_dobyear"=>"SDOBYear",
            "anniversary_day"=>"DOADay",
            "anniversary_month"=>"DOAMonth",
            "anniversary_year"=>"DOAYear",
            "houseno"=>"HouseNo",
            "building_name"=>"BuildingName",
            "street_road"=>"StreetRoad",
            "colony"=>"ColonyLocality",
            "city"=>"City",
            "city_other"=>"CityOthers",
            "pin_code"=>"PinCode",
            "country"=>"Country",
            ); */

            return array(
                "title"=>"Salutation",
                "first_name"=>"FirstName",
                "last_name"=>"LastName",
                "channelcode"=>"ChannelCode",
                "mobile1"=>"MobileNo",
                "email"=>"EmailID",
                "dob_day"=>"DOBDay",
                "dob_month"=>"DOBMonth",
                "dob_year"=>"DOBYear",
                "marital_status"=>"MaritalStatus",
                "spouse_name"=>"SpouseName",
                "spouse_dobday"=>"SDOBDay",
                "spouse_dobmonth"=>"SDOBMonth",
                "spouse_dobyear"=>"SDOBYear",
                "anniversary_day"=>"DOADay",
                "anniversary_month"=>"DOAMonth",
                "anniversary_year"=>"DOAYear",
                "who_wear_spectacles"=>"WhoWearSpectacles",
                "self"=>"Self",
                "spouse"=>"Spouse",
                "parents"=>"Parents",
                "children"=>"Children",
                "houseno"=>"HouseNo",
                "building_name"=>"BuildingName",
                "street_road"=>"StreetRoad",
                "colony"=>"ColonyLocality",
                "city"=>"City",
                "city_other"=>"CityOthers",
                "pin_code"=>"PinCode",
                "country"=>"Country"
            );
    }
    
    public static function apiRequestFamilylinkParams(){
    //formelement => api key
        return array(
            "member_no"=>"PrimaryCardNo",
            "first_name"=>"FirstName",
            "last_name"=>"LastName",
            "mobile"=>"MobileNo",
            "email"=>"EmailId",
            "gender"=>"Gender",
            "redemption_flag"=>"RedemptionFlag",
            "is_member"=>"IsMember",
            "relation"=>"Relation",
            "dob"=>"DOB",
            "otp"=>"OTP",
            "anniversary"=>"Anniversary"
            );
    }
    
    public static function apiResponseFamilyHistoryParams(){
    //formelement => api key
        return array(
            "secondary_member_no"=>"SecondaryCardNo",
            "first_name"=>"FirstName",
            "last_name"=>"LastName",
            "mobile"=>"MobileNumber",
            "redemption_flag"=>"RedemptionPermission",
            "status"=>"Status",
            "created_date"=>"CreateDate",
            
            );
    }
    
    public static function apiRequestFamilyRedemptionPermissionParams(){
    //formelement => api key
        return array(
            "member_no"=>"PrimaryCardNo",
            "secondary_member_no"=>"SecondaryCardNo",
            "redemption_flag"=>"RedemptionFlag",
           
            );
    }
    
    public static function apiRequestfamilyDelinkingParams(){
    //formelement => api key
        return array(
            "member_no"=>"PrimaryCardNo",
            "delinkfor"=>"DelinkFor",
            "secondary_member_no"=>"SecondyCard",
            "otp"=>"OTP"
            );
    }
    
    public static function apiResponseGVAllocation(){
    //formelement => api key
        return array(
            "member_no"=>"CardNo",
            "balance"=>"CustomerBalnce",
            "credited_points"=>"PointsCredited",
            // "error_code"=>"ErrorCode",
            // "msg"=>"ErrorMessage"
            );
    }
    
    public static function apiResponseMemberCheckInfoParams(){
    //formelement => api key
        return array(
            "channelcode"=>"ChannelCode",
            "outletcode"=>"OutletCode",
            "member_no"=>"ULPNo",
            "mobile"=>"MobileNumber",
            "email"=>"EmailId",
            "firstname"=>"Name",
            "balance"=>"PointBalance",
            "member_tier"=>"CurrentTier"
            );
    }
    public static function printResponse($msg, $status = 'error') {
        echo "<script>parent.printMessage(`{$status}`,`{$msg}`);</script>";
        exit();
    }
}