<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivateModel extends Model
{
    use HasFactory;
    protected $connection = 'appdb';
    public $timestamps = false;
    public $table = "gift_points";
	protected $primaryKey = 'id';
}
