<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TblwhatsappStatus extends Model
{
    use HasFactory;
    protected $connection = 'appdb';
    public $timestamps = false;
    protected $fillable=[ 'id','mobile','status','ip','blocked','created_on','edited_on','country'];

}
