<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreModel extends Model
{
    use HasFactory;
    protected $connection = 'appdb';
    public $timestamps = false;
    public $table = "stores_tbl";
	protected $primaryKey = 'id';
    protected $attributes = [
        'old_store_code' => '',
        'channel' => '',
        'contact_name' => '',
        'phone' => '',
        'email_id' => '',
        'store_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'latitude' => '',
        'longitude' => '',
        'region' => '',
        'store_url' => '',
        'store_logo' => '',
        'store_open_days' => ''
    ];
    protected $fillable=[ 'id','guid','store_code','old_store_code','brand_id','channel','store_name','contact_name','phone','email_id','store_address','city','state','pincode','latitude','longitude','region','store_url','store_logo','opening_time','closing_time','store_open_days','blocked','created_by','created_on','edited_by','edited_on','country'];
}
