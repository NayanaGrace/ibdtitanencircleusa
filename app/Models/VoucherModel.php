<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherModel extends Model
{
    use HasFactory;
    protected $connection = 'appdb';
    public $timestamps = false;
    public $table = "vouchers_tbl";
	protected $primaryKey = 'id';
}
