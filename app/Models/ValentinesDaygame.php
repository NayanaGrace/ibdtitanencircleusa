<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ValentinesDaygame extends Model
{
    public $table = "valentines_day_games";

    public $timestamps = false;

    use HasFactory;
}
