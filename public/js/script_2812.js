$(function(){
     $("body").delegate(".call_to_action[title]", "click", function(e) {
        r1 = $(this).data("refer1");
        r2 = $(this).data("refer2");
        r3 = $(this).data("refer3");
        
        r4 = $(this).data("refer4");
        r5 = $(this).data("refer5");
        r6 = $(this).data("refer6");
        
        c1 = $(this).data("category");
        qData = [];
        qData.push("page_url="+window.location.href);
        qData.push("data_refer_1="+r1);
        qData.push("data_refer_2="+r2);
        qData.push("data_refer_3="+r3);
        
        qData.push("data_refer_4="+r4);
        qData.push("data_refer_5="+r5);
        qData.push("data_refer_6="+r6);
        
        qData.push("call_to_action="+$(this).attr("title"));
        if(c1 === null || c1 === undefined || c1.length<1 ) c1 = "general";
        qData.push("click_type="+c1);
    
        processAJAXPostQuery(_service("logclicks","submit"), qData.join('&'));
        // processAJAXPostQuery(_service('giftVouchers',action),q,function(data) {
        
    });
    $("body").delegate(".b#acewebchat_link_add", "click", function(e) {
        let r1 ="";
        let r2 ="";
        let r3 ="";
        let c1 ="";
        let call_to_action ="chatbotclicks";
        qData = [];
        qData.push("page_url="+window.location.href);
        qData.push("data_refer_1="+r1);
        qData.push("data_refer_2="+r2);
        qData.push("data_refer_3="+r3);
        qData.push("call_to_action="+call_to_action);
        if(c1 === null || c1 === undefined || c1.length<1 ) c1 = "general";
        qData.push("click_type="+c1);
    
        // processAJAXPostQuery(_service("logclicks","submit"), qData.join('&'));
    });
    
})
function showLoader(element){
    $(element).append(`<div class="row loaderDiv">
                <div class="col-lg-12 text-center">
                  <a href="javascrip:void(0)" class="tt-btn tt-btn2">
                    <span class="component-loader">
                      <span></span>
                      <span></span>
                      <span></span>
                      <span></span>
                    </span>
                  </a>
                </div>
              </div>`);
}
function hideLoader(element){
    $(".loaderDiv",element).detach();
}

function showLoaderCMS() {
	$("body .loader_bg").detach();
	$("body").append("<div class='loader_bg'><div class='loader_wrapper'><div class='inner'><span>L</span><span>o</span><span>a</span><span>d</span><span>i</span><span>n</span><span>g</span></div></div></div>");
}
function hideLoaderCMS() {
	$("body .loader_bg").detach();
}

function toTitle(s) {
    if(s==null || s.length<=0) return "";
    return s.charAt(0).toUpperCase()+s.substr(1).replace(/_/g, ' ');
}
function validateMobileJq() {
    jQuery.validator.addMethod("mobileno", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || (phone_number.length > 9 &&
              phone_number.match(/^[6-9]\d{9}$/));
    }, "Invalid Mobile");
}

function validateMobile(phone_number) {
    if(phone_number == undefined || phone_number == null || phone_number.length <1)return false;
    phone_number = phone_number.replace(/\s+/g, "");
    return phone_number.length > 9 &&  phone_number.match(/^[6-9]\d{9}$/);
}
// function autoNextElement(formelement){
    
//     setTimeout(function(){
//         $('.pincode-input-container input',formelement).attr('type','password')
//     }, 1000);
    
//     $('.pincode-input',formelement).pincodeInput({
//         hidedigits: true, inputs: 7, placeholders: "- - - - - - -", change: function (input, value, inputnumber) {
//           $("#pincode-callback2").html("onchange from input number " + inputnumber + ", current value: " + value);
//         }
//     });
  
  
//     $(".login-input",formelement).find("input").each(function () {
//         $(this).attr("maxlength", 1);
//         $(this).on("keyup", function (e) {
//           var parent = $($(this).parent());
    
//           if (e.keyCode === 8 || e.keyCode === 37) {
//             var prev = parent.find("input[name=" + $(this).data("previous")+"]");
    
//             if (prev.length) {
//               $(prev).select();
//             }
//           } else if (
//             (e.keyCode >= 48 && e.keyCode <= 57) ||
//             (e.keyCode >= 65 && e.keyCode <= 90) ||
//             (e.keyCode >= 96 && e.keyCode <= 105) ||
//             e.keyCode === 39
//           ) {
//             var next = parent.find("input[name=" + $(this).data("next")+"]");
    
//             if (next.length) {
//               $(next).select();
//             } else {
//               if (parent.data("autosubmit")) {
//                 parent.submit();
//               }
//             }
//           }
//         });
//     });
// }
function validateInputOtp(formelement){
//     $("input[name=digit-6]",formelement).keyup(function () {
//         $(this).parents(".input-block").removeClass("error");
//         let d6=$(this).val();
        
//         let d1=$("input[name=digit-1]",formelement).val();
//         let d2=$("input[name=digit-2]",formelement).val();
//         let d3=$("input[name=digit-3]",formelement).val();
//         let d4=$("input[name=digit-4]",formelement ).val();
//         let d5=$("input[name=digit-5]",formelement).val();
//         if (/^\d{1}$/.test(d1) && /^\d{1}$/.test(d2) && /^\d{1}$/.test(d3) && /^\d{1}$/.test(d4) && /^\d{1}$/.test(d5) && /^\d{1}$/.test(d6)) {
//                 $(".error-txt",formelement).html("").hide();
//                 $(".confirm",formelement).show();
//         } else {
//             // $(this).parents(".input-block").addClass("error");
//             $(".confirm",formelement).hide();
//             // $(".error-txt","#OTPSection").html("Please enter Otp").show();
//         }
//   });
    $("input[name=digits]",formelement).keyup(function () {
        $(this).parents(".input-block").removeClass("error");
        let d1=$(this).val();
        if (/^\d{6}$/.test(d1)) {
                $(".error-txt",formelement).html("").hide();
                $(".confirm",formelement).show();
        } else {
            $(".confirm",formelement).hide();
        }
   });
}
function humanDate(dateStr){
    if(dateStr===null || dateStr.length<=0) return "";
    return moment(dateStr).fromNow();
}
