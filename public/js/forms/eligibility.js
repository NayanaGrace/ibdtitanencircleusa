$(function(){
if($("textarea[name=applies_params]","#Eligibility").length>0){
    $("textarea[name=applies_params]","#Eligibility").attr('id', 'applies_params');
    // $("textarea[name=applies_params]","#Eligibility").after("</br><input type='file' name='input-file' id='input-file' />");
    $("textarea[name=applies_params]","#Eligibility").after("</br><input type='file' name='input-file' id='input-file' accept='.csv,text/plain'/>");
    document.getElementById('input-file').addEventListener('change', loadFileAsText);
}


})
function loadFileAsText(){
  var fileToLoad = document.getElementById("input-file").files[0];

  var fileReader = new FileReader();
  fileReader.onload = function(fileLoadedEvent){
      var textFromFileLoaded = fileLoadedEvent.target.result;
    //   textFromFileLoaded = textFromFileLoaded.split(",").join("\n");
    //   console.log(textFromFileLoaded);
    //   let finalContent = textFromFileLoaded.split("\n").join(",");
    //   console.log(finalContent);
       let finalContent = textFromFileLoaded.replace(/\n|\r|,/g, " ");
       finalContent = finalContent.replace(" ", ",");

      document.getElementById("applies_params").value = finalContent;
  };

  fileReader.readAsText(fileToLoad, "UTF-8");
}