// Loading Screen Animation
$(window).on("load", function () {
  setTimeout(function () {
    $(".loading").fadeOut(200, "", function () {
      $(".loading").remove();
    });
  }, 300);

    if($("body.about").length>0){}
    else{
        $("html, body").animate(
            {
              scrollTop: $("body").offset().top,
            },
            300,
            "swing"
        );
    }
});

$(document).ready(function () {
  if (window.innerWidth > 1024) {
    // var lastScrollTop = 0;
    $(window).scroll(function (event) {
    //   console.log('working');
      if ($(document).scrollTop() < 100 && true) {
        $("nav").addClass("animate");
      }
      if ($(document).scrollTop() == 0 && true) {
        $("nav").removeClass("animate");
      }
    });

    //     // NavBar animation
    // if ($(window).scrollTop() > 100) {
    //   $("nav").fadeOut();
    // }

    // $(".nav-item.nav-select").click(function () {
    //   $(this).toggleClass("active");
    //   $(this).parent().find(".nav-select-list").slideToggle();
    // });

    // var lastScrollTop = 0;
    // $(window).scroll(function (event) {
    //   var st = $(this).scrollTop();
    //   if (st > lastScrollTop) {
    //     $("nav").fadeOut().addClass("animate").removeClass("animate");
    //   } else {
    //     $("nav").fadeIn().addClass("animate");
    //     if ($(document).scrollTop() < 100 && true) {
    //       $("nav").addClass("animate");
    //     }
    //     if ($(document).scrollTop() == 0 && true) {
    //       $("nav").removeClass("animate");
    //     }
    //   }
    //   lastScrollTop = st;
    // });
  }

  // Offer slider
  $(".offer-slider-wrapper").slick({
    slidesToShow: 1,
    draggable: true,
    infinite: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    dots: true,
  });

  //Offer card slider

  var imgWidthRed = 0;
  if (window.innerWidth < 767) {
    imgWidthRed = 55;
  }

//   console.log(imgWidthRed);

  // $(".offer-image").css({ height: $(".offer-image").width() - imgWidthRed });

  // $(".offer-list-xs-card .offer-image").css({
  //   height: $(".offer-list-xs-card .offer-image").width(),
  // });

//   $(".offer-list-container").slick({
//     centerMode: true,
//     slidesToShow: 1,
//     draggable: true,
//     infinite: false,
//     arrows: false,
//     adaptiveHeight: true,
//     mobileFirst: true,

//     responsive: [
//       {
//         breakpoint: 767,
//         settings: "unslick",
//       },
//     ],
//   });

  // Profile card mobile slider
  $(".profile-balance-container").slick({
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    centerMode: false,
    variableWidth: true,
    mobileFirst: true,
    arrows: false,

    responsive: [
      {
        breakpoint: 767,
        settings: "unslick",
      },
    ],
  });

  // Profile complete card mobile slider
  $(".complete-profile-container").slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: false,
    variableWidth: true,
    mobileFirst: true,
    arrows: false,

    responsive: [
      {
        breakpoint: 767,
        settings: "unslick",
      },
    ],
  });
  // Profile complete card mobile slider
  $(".in-store-offer-card-container").slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: false,
    variableWidth: true,
    mobileFirst: true,
    arrows: false,

    responsive: [
      {
        breakpoint: 767,
        settings: "unslick",
      },
    ],
  });
  // Profile complete card mobile slider
//   loadslick_store();

  //redeem slide container

  $(".redeem-list-container").slick({
    centerMode: true,
    slidesToShow: 1,
    draggable: true,
    infinite: false,
    arrows: false,
    adaptiveHeight: true,
    mobileFirst: true,
    dots: true,
    responsive: [
      {
        breakpoint: 767,
        settings: "unslick",
      },
    ],
  });

  // brand slider
  $(".brand-wrapper").slick({
    slidesToShow: 6,
    draggable: true,
    infinite: true,
    arrows: false,
    centerMode: false,
    autoplay: true,
    // autoplaySpeed:100,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
        },
      },
    ],
  });

  // Mob nav

  $(".mob-nav-btn").click(function () {
    $(".nav-list").addClass("open");
    $("body").css({ overflow: "hidden" });
  });

  $(".nav-close").click(function () {
    $(".nav-list").removeClass("open");
    $("body").css({ overflow: "visible" });
  });

  // animation

  new WOW().init();

  //Scroll Magic Code

  TweenLite.defaultEase = Linear.easeNone;
  var controller = new ScrollMagic.Controller();
  var tl = new TimelineMax();

  var slideAdd = 0;

  var SlideDuration = 400; // depends on screen size

  var boxTrigger = 1; // depends on screen size

  // vertical scroll

  var controller = new ScrollMagic.Controller({
    // addIndicators: true
  });

  var trigger1 = new ScrollMagic.Scene({
    triggerElement: ".howItWork-block",
    triggerHook: 0,
    duration: $(".howItWork-block").outerHeight(),
  });

  trigger1.on("enter", function (event) {
    // $('.howItWork-head').addClass('header-fixed');
    // console.log("on enter");
  });

  var trigger1a = new ScrollMagic.Scene({
    triggerElement: ".howItWork-block",
    triggerHook: "onLeave",
    duration: $(".howItWork-slider").outerHeight() - 200,
    // offset: -$('.howItWork-block .tt-block-head').outerHeight()
  });

  trigger1a.on("leave", function (event) {
    // $('.howItWork-head').removeClass('header-fixed');
    // console.log("on leave");
  });

  controller.addScene([trigger1, trigger1a]);

  if (window.innerWidth > 992) {
    if (window.innerWidth < 1366 && true) {
      slideAdd = 50;
    }

    if (window.innerWidth < 1200 && true) {
      slideAdd = 100;
      SlideDuration = 500; // depends on screen size
    }

    if (window.innerWidth < 992 && true) {
      slideAdd = 80;
      SlideDuration = 450; // depends on screen size
    }

    // horizontal scroll

    // var ww = window.innerWidth;
    var noSlides = $(".box-slide").length;
    var slideWidth = $(".box-slide").width() + slideAdd; // + digit is variable --depends on screen size

    // console.log(slideWidth);

    // var slideContainerWidth = slideWidth*noSlides-ww;
    var slideContainerWidth = slideWidth * noSlides - slideWidth;

    var ww = window.innerWidth;
    var actionHorizontal = new TimelineMax().to("#slideContainer", 1, {
      x: -slideContainerWidth,
    });
    var horizontal = createHorizontal();
    function createHorizontal() {
      return new ScrollMagic.Scene({
        triggerElement: "#js-wrapper",
        triggerHook: "onLeave",
        duration: slideContainerWidth,
      })
        .setClassToggle("#id1", "active") // add class toggle
        .setPin("#js-wrapper")
        .setTween(actionHorizontal)
        .addTo(controller);
    }

    var controller2 = new ScrollMagic.Controller({
      globalSceneOptions: { duration: SlideDuration },
      // addIndicators: true
    });

    //slide 1

    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId1",
      triggerHook: boxTrigger,
      offset: 0 + 450,
    })
      .setClassToggle("#slideId1", "active")

      .addTo(controller2);

    //slide 2

    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId2",
      triggerHook: boxTrigger,
      offset: 450 + SlideDuration,
    })
      .setClassToggle("#slideId2", "active")

      .addTo(controller2);

    //slide 3

    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId3",
      triggerHook: boxTrigger,
      offset: 450 + SlideDuration * 2,
    })
      .setClassToggle("#slideId3", "active")

      .addTo(controller2);

    //slide 3

    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId4",
      triggerHook: boxTrigger,
      offset: 450 + SlideDuration * 3,
    })
      .setClassToggle("#slideId4", "active")

      .addTo(controller2);

    //slide 5

    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId5",
      triggerHook: boxTrigger,
      offset: 450 + SlideDuration * 4,
    })
      .setClassToggle("#slideId5", "active")

      .addTo(controller2);

    //slide 6

    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId6",
      triggerHook: boxTrigger,
      offset: 450 + SlideDuration * 5,
    })
      .setClassToggle("#slideId6", "active")
      .addTo(controller2);


    //slide 1 Post
    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId1Post",
      triggerHook: boxTrigger,
      offset: 0 + 200 + SlideDuration * 5,
      
    })
      .setClassToggle("#slideId1Post", "active")
      .addTo(controller2);

    //slide 2 Post
    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId2Post",
      triggerHook: boxTrigger,
      offset: 0 + 200 + SlideDuration * 6,
      
    })
      .setClassToggle("#slideId2Post", "active")
      .addTo(controller2);

    //slide 3 Post
    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId3Post",
      triggerHook: boxTrigger,
      offset: 0 + 200 + SlideDuration * 7,
      
    })
      .setClassToggle("#slideId3Post", "active")
      .addTo(controller2);

    //slide 4 Post
    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId4Post",
      triggerHook: boxTrigger,
      offset: 0 + 200 + SlideDuration * 8,
      
    })
      .setClassToggle("#slideId4Post", "active")
      .addTo(controller2);

    //slide 5 Post
    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId5Post",
      triggerHook: boxTrigger,
      offset: 0 + 200 + SlideDuration * 9,
    })
      .setClassToggle("#slideId5Post", "active")
      .addTo(controller2);

    //slide 6 Post
    var scene1 = new ScrollMagic.Scene({
      triggerEelment: "#slideId6Post",
      triggerHook: boxTrigger,
      offset: 0 + 200 + SlideDuration * 10,
      
    })
      .setClassToggle("#slideId6Post", "active")
      .addTo(controller2);



    $(window).resize(function () {
        let slideHeight=1;
        // let vertical =1;
      // horizontal action

      ww = window.innerWidth;
      slideContainerWidth = slideWidth * noSlides;

      horizontal.destroy(true);
      horizontal = createHorizontal();

      // vertical action

      ww = window.innerHeight;
      slideContainerHeight = slideHeight * noSlides - ww;

      vertical.destroy(true);
      vertical = createVertical();

      TweenLite.set("#line", { height: slideContainerHeight + ww });

    });
  } else {
    //Mobile code

    //Gateway slider
    const boxSlider = $(".box-wrapper");

    boxSlider.slick({
      centerMode: true,
      slidesToShow: 3,
      draggable: true,
      infinite: false,
      arrows: false,
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
          },
        },
      ],
    });
  }

  // New counter code

  if ($("#counter").length) {
    var a = 0;
    $(window).scroll(function () {
      var oTop = $("#counter").offset().top - window.innerHeight;
      // console.log(oTop);
      if (a == 0 && $(window).scrollTop() > oTop) {
        $(".counter-value").each(function () {
          var $this = $(this),
            countTo = $this.attr("data-count");
          $({
            countNum: $this.text(),
          }).animate(
            {
              countNum: countTo,
            },
            {
              duration: 2000,
              easing: "swing",
              step: function () {
                $this.text(Math.floor(this.countNum));
              },
              complete: function () {
                $this.text(this.countNum);
                numCom();
              },
            }
          );
        });
        a = 1;
      }
    });
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function numCom() {
    setTimeout(function () {
      $("#counter .counter-value").each(function () {
        var num = $(this).text();
        //console.log(num, '== num');
        var commaNum = numberWithCommas(num);
        //console.log(commaNum, '-- commNum');
        $(this).text(commaNum);
      });
    }, 100);
  }

  // Login Form

  //custom select box

  $(".selected-item").click(function () {
    $(this).parent().find(".select-list").fadeToggle("1000");
    $(this).parent().siblings().find(".select-list").fadeOut("1000");
  });

  $(document).click(function (e) {
    if ($(e.target).parents(".select-box").length === 0 && true) {
      $(".select-list").fadeOut("1000");
    }

    if ($(e.target).parents(".nav-content").length === 0 && true) {
      $(".nav-list").removeClass("open");
    }

    if (window.innerWidth > 1024) {
      if ($(e.target).parents(".nav-select").length === 0 && true) {
        $(".nav-select-list").fadeOut("1000");
      }
    }
//change by backend developer
    $(".select-list-item").on("click", function () {
      $(this)
        .parent()
        .parent()
        .parent()
        .find(".select-input")
        .val($(this).data('value')).trigger('keyup');
      $(".select-list").slideUp();
    });
  });

  // })

  

  //sign Up click

//   $(".signUp-btn").click(function () {
//     // setTimeout(() => {
//     $(".main-banner-container").addClass("no-overlay");
//     // }, 1000);

//     $(".login-start-screen").fadeOut();
//     $(".form-slide-block").fadeIn();
//     $(".form-pagi").fadeIn().addClass("active");
//     signSlider.get(0).slick.setPosition();
//   });

//   $(".peg-item").click(function () {
//     var index = $(".peg-item").index(this);
//     console.log(index);

//     if ($(this).attr("class").includes("active")) {
//       signSlider.slick("slickGoTo", index);
//     }
//   });

  //Date picker

//   $(function () {
//     $(".datepicker")
//       .datepicker({
//         autoclose: true,
//         todayHighlight: true,
//         format: "dd/mm/yyyy",
//       })
//       .datepicker();
//   });

  // Form Validation

//   $(".name-input").keyup(function () {
//     if ($(this).val().length === 0) {
//       $(this).parent().addClass("error");
//     } else {
//       $(this).parent().removeClass("error");
//     }
//   });


//   $(".form-back").click(function () {
//     signSlider.slick("slickPrev");
//   });

  
});

//Notification
$(document).ready(function(){
  $(".page-notification").click(function(){
    $('body').addClass('overflow-hidden');
    $('.notification-section, .overlay-black').addClass('show');
  });
  $(".notification-close").click(function(){
    $('body').removeClass('overflow-hidden');
    $('.notification-section, .overlay-black').removeClass('show');
  });
});

$('.tt-category-group-wrap a').on('click',function(){
  $('a').removeClass('active-btn');
  $(this).addClass('active-btn');
});

//added by backend developer


