$(function(){
    Handlebars.registerHelper("renderID", function(id) {
        if(id==null || id.length<=0) return "one";
        return md5(id);
    });
    Handlebars.registerHelper('formatDate', function(date) {
    	if(date==null || date<=0) return "";
    	return moment(date).format('D/M/YYYY'); 
    });
    Handlebars.registerHelper('formatDate1', function(date) {
        let  d=new Date(date.split("-").reverse().join("-"));
        let formattedDate=moment(d).format("D MMM, YYYY");
    	return formattedDate;
    });
});