$(function(){
    $("#mobileno").keyup(function () {
        $(this).parents(".input-block").removeClass("error");
        let mobileno=$("#mobileno").val();
         if (/^\d{10}$/.test(mobileno)) {
              $(".error-txt","#loginModal").html("").hide();
              $(".sendOtp","#loginModal").show();
              $(this).parents(".input-block").addClass("error");
            } else {
              $(".sendOtp","#loginModal").hide();
              $(".error-txt","#loginModal").html("Please enter valid Mobile number").show();
            }
      });
    $("#digit-4").keyup(function () {
        $(this).parents(".input-block").removeClass("error");
        let d4=$(this).val();
        let d1=$("#digit-1").val();
        let d2=$("#digit-2").val();
        let d3=$("#digit-3").val();
        if (/^\d{1}$/.test(d1) && /^\d{1}$/.test(d2) && /^\d{1}$/.test(d3) && /^\d{1}$/.test(d4)) {
                $(".confirmOtp","#loginModal").show();
        } else {
            $(this).parents(".input-block").addClass("error");
            $(".confirmOtp","#loginModal").hide();
            $(".error-txt","#loginModal").html("Please enter valid Mobile number").show();
        }
   });
    $(".sendOtp","#loginModal").click(function () {
        $(this).hide();
        $(".send-otp-mobile","#loginModal").hide();
        $(".resend-otp","#loginModal").show();
        $(".login-input","#loginModal").removeClass("d-none");
        $(".modal-subheading","#loginModal").html("OTP has been sent to <span>"+$("#mobileno").val()+"</span>");
    })
    $(".confirmOtp","#loginModal").click(function () {
        window.location=_link("welcome");
    })
});