function deleteProfile(id)
{
    if(confirm('Are you sure to delete this profile?')){
        $.ajax({
            type: "POST",
            url: "profile-delete",
            data: {
                id : id
            },
            headers: {
                    'X-CSRF-Token': $('meta[name = "csrf-token"]').attr('content')
            },
            success: function(response) {
              if(response.status == "success")
              {
                toastr.success(response.message);
              }
              else
              {
                toastr.error(response.message);
              }
              setTimeout(function () {
                location.reload(true);
                }, 2000);
            }
        }); 
    }
}