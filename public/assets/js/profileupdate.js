$(".basic-info-btn").click(function(){
 $('.basic-info').hide();
 $(".b-info").removeClass("active");
 $(".p-info").addClass("active");
 var marital_status = $('.marital_status_min option:selected').val();
    if(marital_status=="Yes") {
      $('.personal-info').hide();
      $('.personal-info-min').css('display', 'flex');
    } else {
      $('.personal-info-min').hide();
      $('.personal-info').css('display', 'flex');
    //  $.scrollTo($('.personal-info'), 1000);
    $.scrollTo($('.steps'), 0);

    }
}); 

$(".personal-info-min-btn").click(function(){
    $(".p-info").removeClass("active");
    $(".l-info").addClass("active");
    $('.personal-info-min').hide();
    $('.personal-info').hide();
    $('.life-style').css('display', 'flex');
    // $.scrollTo($('.life-style'), 1000);
    $.scrollTo($('.steps'), 0);
}); 

$(".personal-info-btn").click(function(){
    $(".p-info").removeClass("active");
    $(".l-info").addClass("active");
    $('.personal-info').hide();
    $('.life-style').css('display', 'flex');
    // $.scrollTo($('.life-style'), 1000);
    $.scrollTo($('.steps'), 0);
}); 


// previous button
$(".go-back-life-style").click(function(){
  $(".l-info").removeClass("active");
  $(".p-info").addClass("active");

  var marital_status = $('.marital_status_min option:selected').val();
  $('.life-style').hide();
  if(marital_status=="No"){
    $('.personal-info-min').hide();
    $('.personal-info').css('display', 'flex');
  //  $.scrollTo($('.personal-info'), 1000);
  $.scrollTo($('.steps'), 0);
   } 
  else if(marital_status=="Yes"){
    $('.personal-info').hide();
    $('.personal-info-min').css('display', 'flex');
 //   $.scrollTo($('.personal-info-min'), 1000);
 $.scrollTo($('.steps'), 0);
   } else {

    $('.personal-info-min').hide();
    $('.personal-info').css('display', 'flex');
   // $.scrollTo($('.personal-info'), 1000);
   $.scrollTo($('.steps'), 0);
    
   }

});

$(".go-back-basic-info").click(function(){
  $(".p-info").removeClass("active");
  $(".b-info").addClass("active");
  $('.personal-info').hide();
  $('.personal-info-min').hide();
  
  $('.basic-info').css('display', 'flex');
  $.scrollTo($('.steps'), 0);

});

// Personal Info

$(".marital_status").change(function() {
   
   var marital_status = $('.marital_status option:selected').val();
  
   if(marital_status!="No"){
    $('.marital_status_min').val("Yes");
    $('.personal-info').hide();
    $('.personal-info-min').css('display', 'flex');
   
   } 

});

$(".marital_status_min").change(function() {

    var marital_status = $('.marital_status_min option:selected').val();
    if(marital_status=="No"){
     $('#marital_status').val("No");
     $('.personal-info-min').hide();
     $('.personal-info').css('display', 'flex');
    } 
 
 });

function btnFunction() {
  $(".go-back-life-style").hide();

  var APP_URL = $("input[name=APP_URL]").val();

  var logged_country=$('input[name = logged_country]').val();
  var btnLoader = document.getElementById("btnLoader");
  btnLoader.classList.toggle('button--loading');
  btnLoader.disabled=true;
  var url= APP_URL + "/myprofile/personalinfoupdate";
  var data=$('#updateprofile').serializeArray();
  $.ajax({
     type: 'POST',
     data: data,
     url: url,
     success: function(result){
       var status = result.status;
       var msg = result.msg;
       if(status=='error') {
         toastr.error(msg);
         var btnLoader = document.getElementById("btnLoader");
         btnLoader.classList.remove('button--loading');
         btnLoader.disabled=false;
         $(".go-back-life-style").show();
       }
       
       else if(status=='success') {


        $('.life-style').hide();
        $('.FormStepList').hide();
        $('.top-hds').hide();
        $('.steps').hide();
        $('.update-profile').hide();
        $('#Thankyoumodal').modal('show');
        $(".update-popup").show();

       }
       
       else {

        toastr.error("Server is busy kindly please try again");
        var btnLoader = document.getElementById("btnLoader");
        btnLoader.classList.remove('button--loading');
        btnLoader.disabled=false;
        $(".go-back-life-style").show();
        
       }
          }       
        }); 
}




$(".select_country").change(function(){

  var country = $(this).val();

  if(country == ''){
      toastr.error("Please select a Country");
  } else {


      findstates(country);
  }

});


$(".select_state").change(function(){

  var state = $(this).val();

  if(state == ''){
      toastr.error("Please select a state");
  } else {


    findsmartcity(state);
  }

});


function findstates(country) {
  var APP_URL = $("input[name=APP_URL]").val();

  $('#city').empty();
      $('#city').append('<option value="">Select City</option>');

  $.ajax({
    type: 'POST',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "country": country
     },
    url: APP_URL + '/findstates',
    success: function(data){
      $('#state').empty();
      $('#state').append('<option value="">Select State</option>');
      $.each(data, function (key, val) {
        console.log(val.title);
        $('#state').append("<option value='"+val.title+"'>"+val.title+"</option>");
    });
       
         }       
       });  

}

function findsmartcity(state) {

  var APP_URL = $("input[name=APP_URL]").val();
  $.ajax({
    type: 'POST',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "state": state
     },
    url: APP_URL + '/findsmartcity',
    success: function(data){
      $('#city').empty();
      $('#city').append('<option value="">Select City</option>');
      $.each(data, function (key, val) {
  
        $('#city').append("<option value='"+val.title+"'>"+val.title+"</option>");
    });
       
         }       
       });  




}