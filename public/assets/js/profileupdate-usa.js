$(".basic-info-btn").click(function(){
    $('.basic-info').hide();
    $(".b-info").removeClass("active");
    $(".p-info").addClass("active");
    var marital_status = $('.marital_status_min option:selected').val();
       if(marital_status=="Yes") {
         $('.personal-info').hide();
         $('.personal-info-min').css('display', 'flex');
       } else {
         $('.personal-info-min').hide();
         $('.personal-info').css('display', 'flex');
       //  $.scrollTo($('.personal-info'), 1000);
       $.scrollTo($('.steps'), 0);
   
       }
   }); 
   
   $(".personal-info-min-btn").click(function(){
       $(".p-info").removeClass("active");
       $(".l-info").addClass("active");
       $('.personal-info-min').hide();
       $('.personal-info').hide();
       $('.life-style').css('display', 'flex');
       // $.scrollTo($('.life-style'), 1000);
       $.scrollTo($('.steps'), 0);
   }); 
   
   $(".personal-info-btn").click(function(){
       $(".p-info").removeClass("active");
       $(".l-info").addClass("active");
       $('.personal-info').hide();
       $('.life-style').css('display', 'flex');
       // $.scrollTo($('.life-style'), 1000);
       $.scrollTo($('.steps'), 0);
   }); 
   
   
   // previous button
   $(".go-back-life-style").click(function(){
     $(".l-info").removeClass("active");
     $(".p-info").addClass("active");
   
     var marital_status = $('.marital_status_min option:selected').val();
     $('.life-style').hide();
     if(marital_status=="No"){
       $('.personal-info-min').hide();
       $('.personal-info').css('display', 'flex');
     //  $.scrollTo($('.personal-info'), 1000);
     $.scrollTo($('.steps'), 0);
      } 
     else if(marital_status=="Yes"){
       $('.personal-info').hide();
       $('.personal-info-min').css('display', 'flex');
    //   $.scrollTo($('.personal-info-min'), 1000);
    $.scrollTo($('.steps'), 0);
      } else {
   
       $('.personal-info-min').hide();
       $('.personal-info').css('display', 'flex');
      // $.scrollTo($('.personal-info'), 1000);
      $.scrollTo($('.steps'), 0);
       
      }
   
   });
   
   $(".go-back-basic-info").click(function(){
     $(".p-info").removeClass("active");
     $(".b-info").addClass("active");
     $('.personal-info').hide();
     $('.personal-info-min').hide();
     
     $('.basic-info').css('display', 'flex');
     $.scrollTo($('.steps'), 0);
   
   });
   
   // Personal Info
   
   $(".marital_status").change(function() {
      
      var marital_status = $('.marital_status option:selected').val();
     
      if(marital_status!="No"){
       $('.marital_status_min').val("Yes");
       $('.personal-info').hide();
       $('.personal-info-min').css('display', 'flex');
      
      } 
   
   });
   
   $(".marital_status_min").change(function() {
   
       var marital_status = $('.marital_status_min option:selected').val();
       if(marital_status=="No"){
        $('#marital_status').val("No");
        $('.personal-info-min').hide();
        $('.personal-info').css('display', 'flex');
       } 
    
    });
   
   function btnFunction() {
  
    var APP_URL = $("input[name=APP_URL]").val();
     $(".go-back-life-style").hide();
     var logged_country=$('input[name = logged_country]').val();
     var btnLoader = document.getElementById("btnLoader");
     btnLoader.classList.toggle('button--loading');
     btnLoader.disabled=true;
     var url=APP_URL + "/myprofile/personalinfoupdate-usa";
     var data=$('#updateprofile').serializeArray();
     $.ajax({
        type: 'POST',
        data: data,
        url: url,
        success: function(result){
  
  
  
          var status = result.status;
          var msg = result.msg;
          if(status=='error') {
            toastr.error(msg);
            var btnLoader = document.getElementById("btnLoader");
            btnLoader.classList.remove('button--loading');
            btnLoader.disabled=false;
            $(".go-back-life-style").show();
          }
          
          else if(status=='success') {
   
   
           $('.life-style').hide();
           $('.FormStepList').hide();
           $('.top-hds').hide();
           $('.steps').hide();
           $('.update-profile').hide();
           $('#Thankyoumodal').modal('show');
           $(".update-popup").show();
   
          }
          
          else {
   
           toastr.error("Server is busy kindly please try again");
           var btnLoader = document.getElementById("btnLoader");
           btnLoader.classList.remove('button--loading');
           btnLoader.disabled=false;
           $(".go-back-life-style").show();
           
          }
          
        
             }       
           }); 
   }