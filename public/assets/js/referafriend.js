
    const btn = document.getElementById('referbtn');

    btn.addEventListener('click', () => {
        const form = document.getElementById('reffer_a_friend');
        if (form.style.display == 'none'){
            form.style.display = 'flex';
            window.scrollBy(0, 250);
        }else{
            form.style.display = 'none';
        }
    });

function btnFunction(){
    var btnLoader = document.getElementById("btnLoader");
    btnLoader.classList.toggle('button--loading');
    btnLoader.disabled=true;
    var first_name = $('input[name="first_name"]').val();
    var last_name = $('input[name="last_name"]').val();
    var name = first_name +" "+last_name;
    var refer_channel = $("#refer_channel option:selected" ).val();
    var mobile =$('input[name="mobile"]').val();
    var remark ="";
    var country_code = $('#contact_country_code').val() || '';
    var email = $('#email').val() || '';  
    var dob = $('#dob').val() || '';  
   
    
      $.ajax({
          type: 'POST',
          data:  {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "refer_channel": refer_channel,
            "name": name,
            "mobile": mobile,
            "email": email,
            "dob": dob,
            "remark": remark,
            "country_code": country_code
            },
          url: '/myprofile/addreferral',
          success: function(result){
          var status = result.status;
          var msg = result.msg;
          if(status=='success'){
            $('#reffer_a_friend').hide();
            $('.referal').hide();
            $('.success-msg').show();

          } else {
            toastr.error(msg);
            var btnLoader = document.getElementById("btnLoader");
            btnLoader.classList.remove('button--loading');
            btnLoader.disabled=false;
          }
          
               }       
             }); 
  }