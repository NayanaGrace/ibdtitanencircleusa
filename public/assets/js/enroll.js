$(".basic-info-btn").click(function(){
  $('.basic-info').hide();
  $(".b-info").removeClass("active");
  $(".p-info").addClass("active");
 var marital_status = $('.marital_status_min option:selected').val();
    if(marital_status=="Yes") {
      $('.personal-info').hide();
      $('.personal-info-min').css('display', 'flex');
    } else {
      $('.personal-info-min').hide();
      $('.personal-info').css('display', 'flex');
    }
}); 

$(".personal-info-min-btn").click(function(){
    $(".p-info").removeClass("active");
    $(".l-info").addClass("active");
    $('.personal-info-min').hide();
    $('.personal-info').hide();
    $('.life-style').css('display', 'flex');
}); 

$(".personal-info-btn").click(function(){
    $(".p-info").removeClass("active");
    $(".l-info").addClass("active");
    $('.personal-info').hide();
    $('.life-style').css('display', 'flex');
}); 
// previous button
$(".go-back-life-style").click(function(){
  $(".l-info").removeClass("active");
  $(".p-info").addClass("active");
  var marital_status = $('.marital_status_min option:selected').val();
  $('.life-style').hide();
  if(marital_status=="No"){
    $('.personal-info-min').hide();
    $('.personal-info').css('display', 'flex');
   } 
  else if(marital_status=="Yes"){
    $('.personal-info').hide();
    $('.personal-info-min').css('display', 'flex');
   } else {

    $('.personal-info-min').hide();
    $('.personal-info').css('display', 'flex');
   }

});

$(".go-back-basic-info").click(function(){
  $(".p-info").removeClass("active");
  $(".b-info").addClass("active");
  $('.personal-info').hide();
  $('.personal-info-min').hide();
  
  $('.basic-info').css('display', 'flex');

});

// Personal Info

$(".marital_status").change(function() {
   
   var marital_status = $('.marital_status option:selected').val();
  
   if(marital_status!="No"){
    $('.marital_status_min').val("Yes");
    $('.personal-info').hide();
    $('.personal-info-min').css('display', 'flex');
   
   } 

});

$(".marital_status_min").change(function() {

    var marital_status = $('.marital_status_min option:selected').val();
    if(marital_status=="No"){
     $('#marital_status').val("No");
     $('.personal-info-min').hide();
     $('.personal-info').css('display', 'flex');
    } 
 
 });

function btnFunction() {

  var logged_country=$('input[name = logged_country]').val();

  var APP_URL = $("input[name=APP_URL]").val();
  if(logged_country=='one_trust_co'){

    $('#OneTrustokey').modal('show');
    return false;

  } else {


  $(".go-back-life-style").hide();
  var btnLoader = document.getElementById("btnLoader");
  btnLoader.classList.toggle('button--loading');
  btnLoader.disabled=true;

  var url=APP_URL+"/memberEnroll";
  var data=$('#updateprofile').serializeArray();
  $.ajax({
     type: 'POST',
     data: data,
     url: url,
     success: function(result){
       var status= result.status;
       var msg = result.msg;
       var logged_country=$('input[name = logged_country]').val();
       if(status=='error') {
         toastr.error(msg);
         var btnLoader = document.getElementById("btnLoader");
         btnLoader.classList.remove('button--loading');
         btnLoader.disabled=false;
         $(".go-back-life-style").show();
       } else {

      /*    $('.life-style').hide();
         $('.FormStepList').hide();
         $('.top-hds').hide();
         $('.steps').hide();
         var back_link = $('input[name = back_link]').val();
         if(back_link == null || back_link =='') {

          if(logged_country =='usa1') {
            $('#OneTrustLoyalty').modal('show');
          }
              else {

           $('.back-to-profile').css('display', 'flex');

        }
        } else {

          if(logged_country =='usa1') {
            $('#OneTrustLoyalty').modal('show');
          }
          else {
           window.location = back_link;

          }
        } */


        $('.life-style').hide();
        $('.FormStepList').hide();
        $('.top-hds').hide();
        $('.steps').hide();
        $('.update-profile').hide();
        $('#Thankyoumodal').modal('show');
        $(".update-popup").show();



       }
          }       
        }); 
      }
}




$(".select_country").change(function(){

  var country = $(this).val();

  if(country == ''){
      toastr.error("Please select a Country");
  } else {


      findstates(country);
  }

});


$(".select_state").change(function(){

  var state = $(this).val();

  if(state == ''){
      toastr.error("Please select a state");
  } else {


    findsmartcity(state);
  }

});


function findstates(country) {
  var APP_URL = $("input[name=APP_URL]").val();

  $('#city').empty();
      $('#city').append('<option value="">Select City</option>');

  $.ajax({
    type: 'POST',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "country": country
     },
    url: APP_URL + '/findstates',
    success: function(data){
      $('#state').empty();
      $('#state').append('<option value="">Select State</option>');
      $.each(data, function (key, val) {
        console.log(val.title);
        $('#state').append("<option value='"+val.title+"'>"+val.title+"</option>");
    });
       
         }       
       });  

}

function findsmartcity(state) {

  var APP_URL = $("input[name=APP_URL]").val();
  $.ajax({
    type: 'POST',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "state": state
     },
    url: APP_URL + '/findsmartcity',
    success: function(data){
      $('#city').empty();
      $('#city').append('<option value="">Select City</option>');
      $.each(data, function (key, val) {
  
        $('#city').append("<option value='"+val.title+"'>"+val.title+"</option>");
    });
       
         }       
       });  




}