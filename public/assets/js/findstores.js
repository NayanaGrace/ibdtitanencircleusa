$(".btn-join").click(function(){

  var APP_URL = $("input[name=APP_URL]").val();
  $(location).prop('href', APP_URL + '/myprofile/personal-information/myprofile-info');

});
$(".logout-click").click(function(){

  var APP_URL = $("input[name=APP_URL]").val();
  $(location).prop('href', APP_URL + '/logout');
  
  });
  $(".profile-click").click(function(){



    var APP_URL = $("input[name=APP_URL]").val();
  $(location).prop('href', APP_URL + '/myprofile/personal-information/myprofile-info');
  
  });
// find state
/* $("#storebrand").change(function(){
    var brand_id = $(this).val();
    if(brand_id == ''){
        toastr.error("Please select a brand");
    } else {

        findstates(brand_id)
    }

  }); */

/*   function findstates(brand_id) {

    $.ajax({
        type: 'POST',
        data: {
          "_token": $('meta[name="csrf-token"]').attr('content'),
          "brand_id": brand_id
         },
        url: '/findstates',
        success: function(data){

          $('#storestate').empty();
          $('#storestate').append('<option value="">Select state</option>');
          $.each(data, function (key, val) {
            console.log(val.state);
            $('#storestate').append("<option value='"+val.state+"'>"+val.state+"</option>");
        });
           
             }       
           }); 
  } */

// find city

$("#storestate").change(function(){ 
  var state = $(this).val();
  if(state == ''){
      toastr.error("Please select a state");
  } else {

      findcity(state)
  }

});

function findcity(state) {

  $.ajax({
      type: 'POST',
      data: {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "state": state
       },
      url: '/findcity',
      success: function(data){
        $('#storecity').empty();
        $('#storecity').append('<option value="">Select City</option>');
        $.each(data, function (key, val) {
          console.log(val.city);
          $('#storecity').append("<option value='"+val.city+"'>"+val.city+"</option>");
      });
         
           }       
         }); 
}

// store search icon

$(".store-search-btn").click(function(){
  var brand_id = $("#storebrand").val();
  var state =  $('#storestate').val();
  var city  =  $('#storecity').val();

  if(brand_id==''){

    toastr.error("Please select a brand");
  } 
  else if(state==''){

    toastr.error("Please select a state");
  }
  else if(city==''){

    toastr.error("Please select a city");
  }
  else {

    findstores(brand_id,state,city)

    initgooglemap(brand_id,state,city)
  }

});

// find stores
function findstores(brand_id,state,city) {

  $.ajax({
    type: 'POST',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "brand_id": brand_id,
      "state": state,
      "city": city,
     },
    url: '/findstoresbycity',
    success: function(data){
   if(data.length==0) {
// changes 
    $('.store-main-box').empty();
    toastr.error("No Stores Found");
    $('.store-locater-map').hide();
    $('.store-main-box').html('<div class="near-store-box col-12"><div class="row"><div class="col-6"><div class="store-bx-hd">No Result Found</div></div><div class="col-3"></div><div class="col-3"></div></div></div>');

   }

  else {

    $('.store-main-box').empty();
    $.each(data, function (key, val) {
      $('.store-main-box').append('<div class="near-store-box col-12"><div class="row"><div class="col-6"><div class="store-bx-hd">'+val.store_name+'</div><div class="store-bx-locatn"><img src="/assets/image/location-blck-icon.png" class="pr">'+val.store_name+','+val.pincode+'</div></div><div class="col-3"><div class="store-bx-img"><img src="/assets/image/km-icon.png"></div><div class="store-bx-km">3.5 km</div></div><div class="col-3"><div class="store-bx-img"><img src="/assets/image/direction-icon.png"></div><div class="store-bx-dr">Directions</div></div></div></div>');
    });

    $('.store-locater-map').show();

  }

         }       
       }); 



}

 function initgooglemap(brand_id,state,city) {

  $.ajax({
    type: 'POST',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "brand_id": brand_id,
      "state": state,
      "city": city,
     },
    url: '/findstoresloc',
    success: function(data){

    console.log(data);

    initMapGoogle(data)

    }
  }); 

  /* locations=[["Taneira Commercial Street Bangalore",12.981933,77.608774]];
  initMapGoogle(locations) */

}

