$(".country_change").click(function(){
  var country =  $(this).attr("data-id");
  change_country(country)
  });

  function change_country(country) {
    $.ajax({
        type: 'POST',
        data: {
          "_token": $('meta[name="csrf-token"]').attr('content'),
          "country": country,
         },
        url: '/change_country',
        success: function(data){
          window.location.reload();
        }
    });
  }