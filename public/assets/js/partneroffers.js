$(".offer_category_tab").click(function(){
  var offers_cat = $(this).attr('data-id');
  loadCategory(offers_cat)
  $(".offer_category_tab").removeClass("active");
  $(this).addClass('active')
}); 

function loadCategory(offers_cat) {

  $.ajax({
    type: 'GET',
    data: {
      "_token": $('meta[name="csrf-token"]').attr('content')
     },
    url: '/find_offers/'+offers_cat,
    success: function(data){
      $('.offer-box').empty();
      $.each(data, function (key, val) {
        console.log();
         $('.offer-box').append('<div class="col-md-6 col-sm-12 d-flex align-items-center justify-content-center partner-offer-top"><div class=""><div class=""><img src="/img/usermedia/'+val.image_url+'" class="ti-offers-icon"></div><div class="titan-offer-bx text-center"><img src="img/usermedia/'+val.logo_url+'" class="img-fluid offericon-h"><div class="ti-offers-offer">'+val.offer_details+'</div><div class="float-start claim-txt">Claim now</div><div class="float-end"><a> <img src="/assets/image/next arrow.png" class="img-fluid"></a></div></div></div></div>');
      });

    }
});

}

$('.first_offer_category_tab').click();