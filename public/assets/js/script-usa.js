$("#loginSection").validate({
    rules: {
     
        mobile: {
        required: true,
        digits: true
      },
      country_code_input_field :{
        required: true
      },
      terms: {
        required: true,
        maxlength: 1
    }
    },
    messages: {
        mobile: {
  
        required: function() {
              toastr.error("Please enter mobile number")
          },
          digits: function() {
              toastr.error("Please enter a valid mobile number")
          }
      },
      country_code_input_field: {
  
        required: function() {
              toastr.error("Please select country code")
          }
      },
      terms: {
  
        required: function() {
              toastr.error("Please check terms and conditions")
          }
      }

    },
    submitHandler: function(form) {
        sendotp()
  }
  });

  $('.btn-resend-otp').click(function(){

    $(".confirm-otp").removeClass("d-flex");
    $(".confirm-otp").hide();
    $(".btn-resend-otp").removeClass("d-flex");
    $(".btn-resend-otp").hide();
    sendotp();

   });
   $('.confirm-otp').click(function(){
    $(".confirm-otp").removeClass("d-flex");
    $(".confirm-otp").hide();
    $(".btn-resend-otp").removeClass("d-flex");
    $(".btn-resend-otp").hide();
    $(".button__loader").show();
    validateOtp()
   });
  

  function sendotp() {

    var APP_URL = $("input[name=APP_URL]").val();


    var mobile = $("input[name=mobile]").val();
  
    var term =$("input[name=terms]").val();
    var country =$("input[name=country]").val();

    if(country=='india'){
      country_code='+91';
      
    } else {
      var country_code = $("#country_code_input_field option:selected" ).val();

    }
   
     var mobile = country_code+mobile;
   
  /*   $("#loginSection").hide(); */

  $(".send-otp").removeClass("d-flex");
  $(".send-otp").hide();
  $(".button__loader").show();
        $.ajax({
            type: 'POST',
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            data: {
              "_token": $('meta[name="csrf-token"]').attr('content'),
              "mobile": mobile,
              "terms":term,
              "country":country,
              "country_code":country_code,
             },
            url: APP_URL + '/sendloginotpusa',
            success: function(data){
              
               if(data.status =='success'){2
  
                 toastr.success('OTP has been sent to '+mobile);
  
                  $("#loginSection").show();
                  $(".button__loader").hide ();
                  $(".login-otp").show();
                  $(".form-check-privacy").hide();
                  $(".login-check").hide();
                  $(".mobile").hide();
                  $(".country_code_input_field").hide();
                  $(".send-otp").removeClass("d-flex");
                  $(".send-otp").hide();
                  $("#whatsapp-text").empty();
                  $(".send-alert-black").empty();
                  $(".send-alert-black").html('OTP has been sent to '+mobile);
                  $(".send-alert-black").show();
                  $(".btn-resend-otp").show();
                  $(".btn-resend-otp").addClass("d-flex");
                  $(".confirm-otp").show();
                  $(".confirm-otp").addClass("d-flex");
                  
            }else {
                toastr.error(data.msg);
                //$("#loginSection").show();
                $(".button__loader").hide ();
                $(".send-otp").addClass("d-flex");
                $(".send-otp").show();

            } 
  
            return false;
    
                 }       
               }); 
  }

  function validateOtp() {
    var APP_URL = $("input[name=APP_URL]").val();
    let otpdigits = $("#otpdigits").val();
    var mobile = $("input[name=mobile]","#loginSection").val();
    var country =$("input[name=country]").val();
    if(country=='india'){
      country_code='+91';
      
    } else {
      var country_code = $("#country_code_input_field option:selected" ).val();

    }
     var mobile = country_code+mobile;
    let back_link = $("input[name=back_link]","#loginSection").val();
    let term =   $("#terms:checked").length;
    var Whatsapp =  $("#check1:checked").length;
    if(Whatsapp==1) {
      var Whatsapp="Y";
    } else{
      var Whatsapp="N";
    }
    $.ajax({
        type: 'POST',
        data: {
          "_token": $('meta[name="csrf-token"]').attr('content'),
          "mobile": mobile,
          "otp": otpdigits,
          "terms":term,
          "Whatsapp":Whatsapp
          },
        url: APP_URL + '/validateotpusa',
        success: function(data){
          console.log(data);
          if(data.status =='success'){

            if(data.member =='YES'){
              
                 window.location.href = APP_URL +"/myprofile/personal-information/edit-usa";
            } else {

              window.location.href = APP_URL +"/enroll";

            }
          
        }else {
          $(".button__loader").hide();
          $(".confirm-otp").addClass("d-flex");
          $(".confirm-otp").show();
          $(".btn-resend-otp").addClass("d-flex");
          $(".btn-resend-otp").show();
            toastr.error(data.msg);
            
        }

             }       
           });
}