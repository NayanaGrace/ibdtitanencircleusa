<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_tables', function (Blueprint $table) {
            $table->id();
            $table->string('salutation');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('dob');
            $table->string('marital_status');
            $table->string('doa');
            $table->string('country');
            $table->string('State');
            $table->string('city');
            $table->string('city_others');
            $table->string('colony');
            $table->string('street_name');
            $table->string('house_no');
            $table->string('building_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_tables');
    }
};
