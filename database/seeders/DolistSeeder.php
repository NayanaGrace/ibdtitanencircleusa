<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DolistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // marital status

        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'marital_status',
            'title'     => 'Single',
            'value'     => '1',
            'country'   => 'uae'
        ],[
            'class'     => 'Single',
            'privilege' => '*',
        ]);


        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'marital_status',
            'title'     => 'Married',
            'value'     => '2',
            'country'   => 'uae'
        ],[
            'class'     => 'Married',
            'privilege' => '*',
        ]);

        // Salutation

        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'salutation',
            'title'     => 'Mr.',
            'value'     => '100',
            'country'   => 'uae'
        ],[
            'class'     => 'Mr.',
            'privilege' => '*',
        ]);


        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'salutation',
            'title'     => 'Ms.',
            'value'     => '101',
            'country'   => 'uae'
        ],[
            'class'     => 'Ms.',
            'privilege' => '*',
        ]);

        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'salutation',
            'title'     => 'Mrs.',
            'value'     => '102',
            'country'   => 'uae'
        ],[
            'class'     => 'Mrs.',
            'privilege' => '*',
        ]);

        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'salutation',
            'title'     => 'Dr.',
            'value'     => '103',
            'country'   => 'uae'
        ],[
            'class'     => 'Dr.',
            'privilege' => '*',
        ]);

        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'salutation',
            'title'     => 'Prof.',
            'value'     => '104',
            'country'   => 'uae'
        ],[
            'class'     => 'Prof.',
            'privilege' => '*',
        ]);

        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'salutation',
            'title'     => 'Mjr',
            'value'     => '105',
            'country'   => 'uae'
        ],[
            'class'     => 'Mjr.',
            'privilege' => '*',
        ]);

        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'salutation',
            'title'     => 'Capt.',
            'value'     => '106',
            'country'   => 'uae'
        ],[
            'class'     => 'Capt.',
            'privilege' => '*',
        ]);

        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'salutation',
            'title'     => 'Col.',
            'value'     => '107',
            'country'   => 'uae'
        ],[
            'class'     => 'Col.',
            'privilege' => '*',
        ]);



         // City
          DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'city',
            'title'     => 'Dubai',
            'value'     => '1',
            'country'   => 'uae'
        ],[
            'class'     => 'Single',
            'privilege' => '*',
        ]);

        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'city',
            'title'     => 'Abudhabi',
            'value'     => '6243',
            'country'   => 'uae'
        ],[
            'class'     => 'Single',
            'privilege' => '*',
        ]); 


         DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'city',
            'title'     => 'Sharjah',
            'value'     => '5928',
            'country'   => 'uae'
        ],[
            'class'     => 'Single',
            'privilege' => '*',
        ]); 
        

        // States

         DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'state',
            'title'     => 'Dubai',
            'value'     => '001',
            'country'   => 'uae'
        ],[
            'class'     => 'Single',
            'privilege' => '*',
        ]);

        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'state',
            'title'     => 'Abu Dhabi',
            'value'     => '002',
            'country'   => 'uae'
        ],[
            'class'     => 'Single',
            'privilege' => '*',
        ]);
        
        DB::table('do_lists')->updateOrInsert([
            'groupid'   => 'state',
            'title'     => 'Sharjah',
            'value'     => '0079',
            'country'   => 'uae'
        ],[
            'class'     => 'Single',
            'privilege' => '*',
        ]); 
        

        

    }
}
