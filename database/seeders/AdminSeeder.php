<?php

namespace Database\Seeders;

use App\Models\Admin\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->updateOrInsert([
            'email'     => 'admin@titanencircle.com',
        ],[
            'name'                  => 'Super Admin',
            'password'              => Hash::make('EncircleJan@20223'),
            'role'                  => 1,
            'password_expired_on'   => date('Y-m-d', strtotime('+2 months'))
        ]);

        DB::table('users')->updateOrInsert([
            'email'     => 'reshmabanu@titan.co.in',
        ],[
            'name'                  => 'Offer Manager Admin',
            'password'              => Hash::make('Encircle@Jan#2023'),
            'role'                  => 2,
            'password_expired_on'   => date('Y-m-d', strtotime('+2 months'))
        ]);

        DB::table('users')->updateOrInsert([
            'email'     => 'refer@titanencircle.com',
        ],[
            'name'                  => 'Refer Admin',
            'password'              => Hash::make('Encircle#2022'),
            'role'                  => 3,
            'password_expired_on'   => date('Y-m-d', strtotime('+2 months'))
        ]);
    }
}
